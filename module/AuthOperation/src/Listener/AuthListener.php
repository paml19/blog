<?php

namespace paml\Auth\Operation\Listener;

use paml\Auth\Listener\AuthListener as MainAuthListener;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\Mvc\MvcEvent;

class AuthListener extends MainAuthListener implements ListenerAggregateInterface
{
    protected function checkAuth(MvcEvent $mvcEvent): bool
    {
        $controllerName = $mvcEvent->getRouteMatch()->getParam('controller', null);

        if (in_array('ManageController', explode('\\', $controllerName))) {
            return true;
        }

        if (in_array('Operation', explode('\\', $controllerName))) {
            return false;
        }

        if (
            in_array('Auth', explode('\\', $controllerName))
            ||
            strstr($controllerName, 'Auth') !== false
            ||
            $mvcEvent->getTarget() instanceof AbstractRestfulController
        ) {
            return true;
        }

        return false;
    }
}
