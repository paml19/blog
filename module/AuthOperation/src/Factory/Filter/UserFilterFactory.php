<?php

namespace paml\Auth\Operation\Factory\Filter;

use Interop\Container\ContainerInterface;
use paml\Auth\Operation\Filter\UserFilter;
use paml\Auth\Operation\Repository\RoleRepository;
use Zend\ServiceManager\Factory\FactoryInterface;

class UserFilterFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): UserFilter
    {
        return new UserFilter(
            $container->get(RoleRepository::class)
        );
    }
}