<?php

namespace paml\Auth\Operation\Factory\Helper;

use Interop\Container\ContainerInterface;
use paml\Auth\Operation\Helper\ManageHelper;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\View\Helper\Url;

class ManageHelperFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): ManageHelper
    {
        return new ManageHelper(
            $container->get('ViewHelperManager')->get(Url::class)
        );
    }
}