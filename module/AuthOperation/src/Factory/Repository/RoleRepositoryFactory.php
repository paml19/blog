<?php

namespace paml\Auth\Operation\Factory\Repository;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use paml\Auth\Entity\Role;
use paml\Auth\Operation\Repository\RoleRepository;
use Zend\ServiceManager\Factory\FactoryInterface;

class RoleRepositoryFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): RoleRepository
    {
        return new RoleRepository(
            $container->get(EntityManager::class),
            $container->get(EntityManager::class)->getClassMetadata(Role::class)
        );
    }
}