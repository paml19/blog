<?php

namespace paml\Auth\Operation\Factory\Repository;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use paml\Auth\Entity\User;
use paml\Auth\Operation\Repository\UserRepository;
use Zend\ServiceManager\Factory\FactoryInterface;

class UserRepositoryFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): UserRepository
    {
        return new UserRepository(
            $container->get(EntityManager::class),
            $container->get(EntityManager::class)->getClassMetadata(User::class)
        );
    }
}