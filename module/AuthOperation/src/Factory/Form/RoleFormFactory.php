<?php

namespace paml\Auth\Operation\Factory\Form;

use Interop\Container\ContainerInterface;
use paml\Auth\Entity\Role;
use paml\Auth\Operation\Filter\RoleFilter;
use paml\Auth\Operation\Form\RoleForm;
use paml\Auth\Operation\Hydrator\RoleHydrator;
use Zend\ServiceManager\Factory\FactoryInterface;

class RoleFormFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): RoleForm
    {
        $form = new RoleForm('role-form');

        $form->setInputFilter($container->get('InputFilterManager')->get(RoleFilter::class));
        $form->setHydrator($container->get('HydratorManager')->get(RoleHydrator::class));
        $form->setObject(new Role);

        return  $form;
    }
}