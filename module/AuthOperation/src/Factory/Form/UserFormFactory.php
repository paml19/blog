<?php

namespace paml\Auth\Operation\Factory\Form;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use paml\Auth\Entity\User;
use paml\Auth\Operation\Filter\UserFilter;
use paml\Auth\Operation\Form\UserForm;
use paml\Auth\Operation\Hydrator\UserHydrator;
use Zend\ServiceManager\Factory\FactoryInterface;

class UserFormFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): UserForm
    {
        $form = new UserForm('user-form', [], $container->get(EntityManager::class));

        $form->setHydrator($container->get('HydratorManager')->get(UserHydrator::class));
        $form->setInputFilter($container->get('InputFilterManager')->get(UserFilter::class));
        $form->setObject(new User);

        return $form;
    }
}