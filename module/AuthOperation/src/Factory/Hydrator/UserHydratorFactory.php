<?php

namespace paml\Auth\Operation\Factory\Hydrator;

use Interop\Container\ContainerInterface;
use paml\Auth\Operation\Hydrator\UserHydrator;
use paml\Auth\Operation\Repository\RoleRepository;
use Zend\ServiceManager\Factory\FactoryInterface;

class UserHydratorFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): UserHydrator
    {
        return new UserHydrator(
            $container->get(RoleRepository::class)
        );
    }
}