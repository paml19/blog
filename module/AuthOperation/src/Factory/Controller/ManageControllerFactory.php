<?php

namespace paml\Auth\Operation\Factory\Controller;

use Interop\Container\ContainerInterface;
use paml\Auth\Operation\Controller\ManageController;
use paml\Auth\Operation\Form\UserForm;
use paml\Auth\Operation\Repository\UserRepository;
use paml\Auth\Service\AuthManager;
use Zend\ServiceManager\Factory\FactoryInterface;

class ManageControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): ManageController
    {
        return new ManageController(
            $container->get('FormElementManager')->get(UserForm::class),
            $container->get(UserRepository::class),
            $container->get(AuthManager::class)
        );
    }
}