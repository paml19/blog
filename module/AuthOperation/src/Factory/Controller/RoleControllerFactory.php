<?php

namespace paml\Auth\Operation\Factory\Controller;

use Interop\Container\ContainerInterface;
use paml\Auth\Operation\Controller\RoleController;
use paml\Auth\Operation\Form\RoleForm;
use paml\Auth\Operation\Repository\RoleRepository;
use Zend\ServiceManager\Factory\FactoryInterface;

class RoleControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): RoleController
    {
        if (isset($container->get('Config')['entity_view'])) {
            $defaults = $container->get('Config')['entity_view']['defaults'];
        } else {
            $defaults = [
                'page' => 1,
                'count' => 10,
                'sort' => 'ASC',
                'order' => 'id'
            ];
        }

        return new RoleController(
            $container->get(RoleRepository::class),
            $defaults,
            $container->get('FormElementManager')->get(RoleForm::class)
        );
    }
}