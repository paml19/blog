<?php

namespace paml\Auth\Operation\Factory\Controller;

use Interop\Container\ContainerInterface;
use paml\Auth\Operation\Controller\UserController;
use paml\Auth\Operation\Form\UserForm;
use paml\Auth\Operation\Repository\UserRepository;
use Zend\ServiceManager\Factory\FactoryInterface;

class UserControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): UserController
    {
        if (isset($container->get('Config')['entity_view'])) {
            $defaults = $container->get('Config')['entity_view']['defaults'];
        } else {
            $defaults = [
                'page' => 1,
                'count' => 10,
                'sort' => 'ASC',
                'order' => 'id'
            ];
        }

        return new UserController(
            $container->get(UserRepository::class),
            $defaults,
            $container->get('FormElementManager')->get(UserForm::class)
        );
    }
}