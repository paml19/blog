<?php

namespace paml\Auth\Operation\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use DoctrineModule\Paginator\Adapter\Collection as CollectionAdapter;
use paml\Auth\Repository\RoleRepository as MainRoleRepository;
use Zend\Paginator\Paginator;

class RoleRepository extends MainRoleRepository
{
    public function getList(int $page, int $limit, string $sort, string $order): Paginator
    {
        $collection = new ArrayCollection($this->findBy(
            [
                'systemAdmin' => false
            ],
            [
                $order => $sort
            ]
        ));
        $adapter = new CollectionAdapter($collection);

        $paginator = (new Paginator($adapter))
            ->setCurrentPageNumber($page)
            ->setItemCountPerPage($limit);

        return $paginator;
    }
}