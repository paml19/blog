<?php

namespace paml\Auth\Operation;

use paml\Auth\Operation\Listener\AuthListener;
use Zend\EventManager\EventManager;
use Zend\Mvc\MvcEvent;

class Module
{
    const USER_CREATED = 'userCreated';

    const USER_UPDATED = 'userUpdated';

    const USER_DELETED = 'userDeleted';

    const USER_RESTORED = 'userRestored';

    const ROLE_CREATED = 'roleCreated';

    const ROLE_UPDATED = 'roleUpdated';

    const ROLE_DELETED = 'roleDeleted';

    const ROLE_RESTORED = 'roleRestored';

    private $authConfig;

    public function getConfig()
    {
        if (! file_exists(__DIR__ . '/../../../config/autoload/authOperation.global.php')) {
            throw new \Exception('No file authOperation.global.php provided, copy dist from module config to ./config/autoload/');
        }

        $moduleConfig = include __DIR__ . '/../config/module.config.php';
        $this->authConfig = include __DIR__ . '/../../../config/autoload/authOperation.global.php';

        $navigationConfig = [
            'navigation' => [
                $this->authConfig['auth_operation']['navigation_name'] => [
                    [
                        'label' => 'Auth operations',
                        'route' => 'auth/operation',
                        'pages' => [
                            [
                                'label' => 'User',
                                'route' => 'auth/operation/user',
                                'action' => 'index',
                                'resource' => Controller\UserController::class,
                            ],
                            [
                                'label' => 'Role',
                                'route' => 'auth/operation/role',
                                'action' => 'index',
                                'resource' => Controller\RoleController::class,
                            ]
                        ],
                    ]
                ],
            ]
        ];

        $config = array_merge_recursive($moduleConfig, $navigationConfig);

        return $config;
    }

    public function onBootstrap(MvcEvent $event)
    {
        $serviceManager = $event->getApplication()->getServiceManager();
        /** @var EventManager $eventManager */
        $eventManager = $event->getTarget()->getEventManager();

        $eventManager->attach(
            MvcEvent::EVENT_DISPATCH,
            [
                $this,
                'onDispatch'
            ],
            1
        );

        /** @var AuthListener $authListener */
        $authListener = $serviceManager->get(AuthListener::class);
        $authListener->attach($eventManager, 99);
    }

    public function onDispatch(MvcEvent $event)
    {
        $controller = $event->getTarget();
        $controllerClass = get_class($controller);
        $controllerClassExploded = explode('\\', $controllerClass);
        unset($controllerClassExploded[count($controllerClassExploded) - 1]);
        unset($controllerClassExploded[count($controllerClassExploded) - 1]);
        $moduleNamespace = implode('\\', $controllerClassExploded);

        if ($moduleNamespace == __NAMESPACE__) {
            $viewModel = $event->getViewModel();
            $viewModel->setTemplate($this->authConfig['auth_operation']['layout_name']);
        }
    }
}
