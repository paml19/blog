<?php

namespace paml\Auth\Operation\Filter;

use DoctrineModule\Validator\ObjectExists;
use paml\Auth\Entity\Role;
use paml\Auth\Form\Validator\PasswordStrengthValidator;
use paml\Auth\Operation\Form\UserForm;
use paml\Auth\Operation\Repository\RoleRepository;
use paml\EntityOperation\Validator\MultipleObjectExists;
use Zend\Filter\StringTrim;
use Zend\Filter\StripNewlines;
use Zend\Filter\StripTags;
use Zend\InputFilter\InputFilter;
use Zend\Validator\EmailAddress;
use Zend\Validator\StringLength;

class UserFilter extends InputFilter
{
    private $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function init()
    {
        $this->add([
            'name' => UserForm::NAME,
            'required' => false,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => StripNewlines::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'max' => 50,
                    ],
                ],
            ],
        ]);

        $this->add([
            'name' => UserForm::SURNAME,
            'required' => false,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => StripNewlines::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'max' => 100,
                    ],
                ],
            ],
        ]);

        $this->add([
            'name' => UserForm::EMAIL,
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => StripNewlines::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'max' => 50,
                        'min' => 1,
                    ],
                ],
                ['name' => EmailAddress::class],
            ],
        ]);

        $this->add([
            'name' => UserForm::LOGIN,
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => StripNewlines::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'max' => 50,
                    ],
                ],
            ],
        ]);

        $this->add([
            'name' => UserForm::ROLES,
            'required' => false,
            'validators' => [
                [
                    'name' => MultipleObjectExists::class,
                    'options' => [
                        'object_repository' => $this->roleRepository,
                        'target_class' => Role::class,
                        'fields' => 'id',
                    ],
                ],
            ],
        ]);

        $this->add([
            'name' => UserForm::DEFAULT_ROLE,
            'required' => true,
            'validators' => [
                [
                    'name' => ObjectExists::class,
                    'options' => [
                        'object_repository' => $this->roleRepository,
                        'target_class' => Role::class,
                        'fields' => 'id',
                    ],
                ],
            ],
        ]);

        $this->add([
            'name' => UserForm::PASSWORD,
            'required' => false,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => StripNewlines::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'max' => 50,
                        'min' => 1,
                    ],
                ],
                [
                    'name' => PasswordStrengthValidator::class,
                ],
            ],
        ]);
    }
}
