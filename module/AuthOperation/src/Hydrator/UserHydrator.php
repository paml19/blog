<?php

namespace paml\Auth\Operation\Hydrator;

use Doctrine\Common\Collections\ArrayCollection;
use paml\Auth\Entity\Role;
use paml\Auth\Operation\Form\UserForm;
use paml\Auth\Operation\Repository\RoleRepository;
use Zend\Hydrator\AbstractHydrator;

class UserHydrator extends AbstractHydrator
{
    const ID = 'id';

    private $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function extract($object)
    {
        $roles = new ArrayCollection;

        /** @var Role $role */
        foreach ($object->getRoles() as $role) {
            if ($role->getActive() && ! $role->getDateDelete()) {
                $roles->add($role->getId());
            }
        }

        return [
            self::ID => $object->getId(),
            UserForm::NAME => $object->getName(),
            UserForm::SURNAME => $object->getSurname(),
            UserForm::EMAIL => $object->getEmail(),
            UserForm::LOGIN => $object->getLogin(),
            UserForm::ROLES => $roles->toArray(),
            UserForm::DEFAULT_ROLE => $object->getDefaultRole() ? $object->getDefaultRole()->getId() : null,
        ];
    }

    public function hydrate(array $data, $object)
    {
        if (isset($data[UserForm::NAME])) {
            $object->setName($data[UserForm::NAME]);
        }

        if (isset($data[UserForm::SURNAME])) {
            $object->setSurname($data[UserForm::SURNAME]);
        }

        if (isset($data[UserForm::EMAIL])) {
            $object->setEmail($data[UserForm::EMAIL]);
        }

        if (isset($data[UserForm::LOGIN])) {
            $object->setLogin($data[UserForm::LOGIN]);
        }

        if (isset($data[UserForm::ROLES])) {
            $object->setRoles(new ArrayCollection);
            foreach ($data[UserForm::ROLES] as $roleFromForm) {
                if ($roleFromForm != '') {
                    if (! is_array($roleFromForm)) {
                        $role = $this->roleRepository->find($roleFromForm);
                    } else {
                        $role = $this->roleRepository->find($roleFromForm[RoleHydrator::ID]);
                    }

                    $object->addRole($role);
                }
            }
        }

        if (isset($data[UserForm::DEFAULT_ROLE]) && $data[UserForm::DEFAULT_ROLE] != '') {
            $defaultRole = $this->roleRepository->find($data[UserForm::DEFAULT_ROLE]);

            if (! $object->getRoles()->contains($defaultRole)) {
                $object->addRole($defaultRole);
            }

            $object->setDefaultRole($defaultRole);
        }

        if (isset($data[UserForm::PASSWORD]) && $data[UserForm::PASSWORD] != '') {
            $object->setPassword($data[UserForm::PASSWORD]);
        }

        return $object;
    }
}