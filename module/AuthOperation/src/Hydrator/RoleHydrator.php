<?php

namespace paml\Auth\Operation\Hydrator;

use paml\Auth\Operation\Form\RoleForm;
use Zend\Hydrator\AbstractHydrator;

class RoleHydrator extends AbstractHydrator
{
    const ID = 'id';

    public function extract($object)
    {
        return [
            self::ID => $object->getId(),
            RoleForm::NAME => $object->getName(),
            RoleForm::ACTIVE => $object->getActive() ? 'yes' : 'no',
        ];
    }

    public function hydrate(array $data, $object)
    {
        if (isset($data[RoleForm::NAME])) {
            $object->setName($data[RoleForm::NAME]);
        }

        if (isset($data[RoleForm::ACTIVE])) {
            if ($data[RoleForm::ACTIVE] == 'yes') {
                $object->setActive(true);
            } else {
                $object->setActive(false);
            }
        }

        return $object;
    }
}