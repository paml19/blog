<?php

namespace paml\Auth\Operation\Form;

use Zend\Form\Element\Checkbox;
use Zend\Form\Element\Csrf;
use Zend\Form\Element\Submit;
use Zend\Form\Element\Text;
use Zend\Form\Form;

class RoleForm extends Form
{
    const CSRF = 'csrf';

    const NAME = 'name';

    const ACTIVE = 'active';

    const SUBMIT = 'submit';

    public function init()
    {
        $this->add([
            'name' => self::CSRF,
            'type' => Csrf::class
        ]);

        $this->add([
            'name' => self::NAME,
            'type' => Text::class,
            'options' => [
                'label' => 'Name',
                'label_attributes' => [
                    'class' => 'col-sm-2 col-form-label',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => 'Set role name',
                'required' => true,
            ],
        ]);

        $this->add([
            'name' => self::ACTIVE,
            'type' => Checkbox::class,
            'options' => [
                'label' => 'Active',
                'label_attributes' => [
                    'class' => 'col-sm-2 col-form-label',
                ],
                'use_hidden_value' => true,
                'checked_value' => 'yes',
                'unchecked_value' => 'no',
            ],
            'attributes' => [
                'class' => 'form-control checkbox',
            ]
        ]);

        $this->add([
            'name' => self::SUBMIT,
            'type' => Submit::class,
            'attributes' => [
                'value' => 'Save',
                'class' => 'btn btn-block btn-primary',
            ],
        ]);
    }
}