<?php

namespace paml\Auth\Operation\Form;

use Doctrine\ORM\EntityManager;
use DoctrineModule\Form\Element\ObjectSelect;
use paml\Auth\Entity\Role;
use Zend\Form\Element\Csrf;
use Zend\Form\Element\Email;
use Zend\Form\Element\Password;
use Zend\Form\Element\Submit;
use Zend\Form\Element\Text;
use Zend\Form\Form;

class UserForm extends Form
{
    const CSRF = 'csrf';

    const NAME = 'name';

    const SURNAME = 'surname';

    const EMAIL = 'email';

    const LOGIN = 'login';

    const ROLES = 'roles';

    const DEFAULT_ROLE = 'defaultRole';

    const PASSWORD = 'password';

    const SUBMIT = 'submit';

    private $entityManager;

    public function __construct($name = null, $options = [], EntityManager $entityManager)
    {
        parent::__construct($name, $options);
        $this->entityManager = $entityManager;
    }

    public function init()
    {
        $this->add([
            'name' => self::CSRF,
            'type' => Csrf::class
        ]);

        $this->add([
            'name' => self::NAME,
            'type' => Text::class,
            'options' => [
                'label' => 'Name',
                'label_attributes' => [
                    'class' => 'col-sm-2 col-form-label',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => 'Set user name',
                'required' => false,
            ],
        ]);

        $this->add([
            'name' => self::SURNAME,
            'type' => Text::class,
            'options' => [
                'label' => 'Surname',
                'label_attributes' => [
                    'class' => 'col-sm-2 col-form-label',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => 'Set user surname',
                'required' => false,
            ],
        ]);

        $this->add([
            'name' => self::EMAIL,
            'type' => Email::class,
            'options' => [
                'label' => 'Email',
                'label_attributes' => [
                    'class' => 'col-sm-2 col-form-label',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => 'Set user email',
                'required' => true,
            ],
        ]);

        $this->add([
            'name' => self::LOGIN,
            'type' => Text::class,
            'options' => [
                'label' => 'Login',
                'label_attributes' => [
                    'class' => 'col-sm-2 col-form-label',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => 'Set user login',
                'required' => true,
            ],
        ]);

        $this->add([
            'name' => self::ROLES,
            'type' => ObjectSelect::class,
            'options' => [
                'label' => 'Roles',
                'object_manager' => $this->entityManager,
                'target_class' => Role::class,
                'label_generator' => function ($role) {
                    return $role->getName();
                },
                'property' => 'id',
                'is_method' => true,
                'find_method' => [
                    'name' => 'findBy',
                    'params' => [
                        'criteria' => [
                            'dateDelete' => null,
                            'active' => true,
                            'systemAdmin' => false,
                        ],
                    ],
                ],
                'display_empty_item' => true,
                'empty_item_label' => 'Select user roles',
                'label_attributes' => [
                    'class' => 'col-sm-2 col-form-label',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
                'required' => false,
                'multiple' => true,
            ],
        ]);

        $this->add([
            'name' => self::DEFAULT_ROLE,
            'type' => ObjectSelect::class,
            'options' => [
                'label' => 'Default role',
                'object_manager' => $this->entityManager,
                'target_class' => Role::class,
                'label_generator' => function ($role) {
                    return $role->getName();
                },
                'property' => 'id',
                'is_method' => true,
                'find_method' => [
                    'name' => 'findBy',
                    'params' => [
                        'criteria' => [
                            'dateDelete' => null,
                            'active' => true,
                            'systemAdmin' => false,
                        ],
                    ],
                ],
                'display_empty_item' => true,
                'empty_item_label' => 'Select default user role',
                'label_attributes' => [
                    'class' => 'col-sm-2 col-form-label',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
                'required' => true,
                'multiple' => false,
            ],
        ]);

        $this->add([
            'name' => self::PASSWORD,
            'type' => Password::class,
            'options' => [
                'label' => 'Password',
                'label_attributes' => [
                    'class' => 'col-sm-2 col-form-label',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
                'required' => false,
                'data-toggle' => 'password',
                'data-placement' => 'after',
                'placeholder' => 'Set new password'
            ],
        ]);

        $this->add([
            'name' => self::SUBMIT,
            'type' => Submit::class,
            'attributes' => [
                'value' => 'Save',
                'class' => 'btn btn-block btn-primary'
            ],
        ]);
    }
}