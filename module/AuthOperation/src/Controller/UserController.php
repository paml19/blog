<?php

namespace paml\Auth\Operation\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use DoctrineModule\Validator\NoObjectExists;
use paml\Auth\Entity\User;
use paml\Auth\Operation\Form\UserForm;
use paml\Auth\Operation\Module;
use paml\Auth\Operation\Repository\UserRepository;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class UserController extends AbstractActionController
{
    private $userRepository;

    private $defaults;

    private $userForm;

    public function __construct(
        UserRepository $userRepository,
        array $defaults,
        UserForm $userForm
    ) {
        $this->userRepository = $userRepository;
        $this->defaults = $defaults;
        $this->userForm = $userForm;
    }

    public function indexAction()
    {
        $page = (int) $this->params()->fromQuery('page', $this->defaults['page']);
        $count = (int) $this->params()->fromQuery('count', $this->defaults['count']);
        $sort = (string) $this->params()->fromQuery('sort', $this->defaults['sort']);
        $order = (string) $this->params()->fromQuery('order', $this->defaults['order']);

        $list = $this->userRepository->getList($page, $count, $sort, $order);

        $users = new ArrayCollection();
        if ($list->count() >= $page) {
            foreach ($list as $item) {
                $users->add($item);
            }
        }

        return new ViewModel([
            'users' => $users
        ]);
    }

    public function addAction()
    {
        $userId = $this->params()->fromRoute('id', null);
        $request = $this->getRequest();
        $user = null;

        if ($userId) {
            /** @var User $user */
            $user = $this->userRepository->find($userId);

            if (! $user) {
                throw new \Exception('Not found object with ' . $userId);
            }

            $this->userForm->bind($user);
            $this->userForm->get(UserForm::EMAIL)->setAttribute('readonly', true);
            $this->userForm->get(UserForm::LOGIN)->setAttribute('readonly', true);
        } else {
            $noObjectExistsValidatorEmail = new NoObjectExists([
                'object_repository' => $this->userRepository,
                'fields' => 'email'
            ]);
            $this->userForm->getInputFilter()->get(UserForm::EMAIL)->getValidatorChain()->attach($noObjectExistsValidatorEmail);

            $noObjectExistsValidatorLogin = new NoObjectExists([
                'object_repository' => $this->userRepository,
                'fields' => 'login'
            ]);
            $this->userForm->getInputFilter()->get(UserForm::LOGIN)->getValidatorChain()->attach
            ($noObjectExistsValidatorLogin);
        }

        if ($request->isPost()) {
            $this->userForm->setData($request->getPost());
            if ($this->userForm->isValid()) {
                /** @var User $formUser */
                $formUser = $this->userForm->getData();

                if ($userId) {
                    $formUser->setEmail($user->getEmail());
                    $formUser->setLogin($user->getLogin());
                    $this->userRepository->merge($formUser);

                    $this->getEventManager()->trigger(
                        Module::USER_UPDATED,
                        $this,
                        [
                            'user' => $formUser
                        ]
                    );
                } else {
                    $this->userRepository->save($formUser);

                    $this->getEventManager()->trigger(
                        Module::USER_CREATED,
                        $this,
                        [
                            'user' => $formUser
                        ]
                    );
                }

                return $this->redirect()->toRoute('auth/operation/user');
            }
        }

        return new ViewModel([
            'form' => $this->userForm,
        ]);
    }

    public function deleteAction()
    {
        $userId = $this->params()->fromRoute('id', null);

        if ($userId) {
            $user = $this->userRepository->find($userId);

            if (! $user) {
                throw new \Exception('Not found object with ' . $userId);
            }

            $this->userRepository->delete($user);

            $this->getEventManager()->trigger(
                Module::USER_DELETED,
                $this,
                [
                    'user' => $user
                ]
            );
        }

        return $this->redirect()->toRoute('auth/operation/user');
    }

    public function restoreAction()
    {
        $userId = $this->params()->fromRoute('id', null);

        if ($userId) {
            $user = $this->userRepository->find($userId);

            if (! $user) {
                throw new \Exception('Not found object with ' . $userId);
            }

            $user->setDateDelete(null);

            $this->userRepository->merge($user);

            $this->getEventManager()->trigger(
                Module::USER_RESTORED,
                $this,
                [
                    'user' => $user
                ]
            );
        }

        return $this->redirect()->toRoute('auth/operation/user');
    }
}
