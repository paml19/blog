<?php

namespace paml\Auth\Operation\Controller;

use paml\Auth\Entity\User;
use paml\Auth\Operation\Form\UserForm;
use paml\Auth\Operation\Module;
use paml\Auth\Operation\Repository\UserRepository;
use paml\Auth\Service\AuthManager;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ManageController extends AbstractActionController
{
    private $userForm;

    private $userRepository;

    private $authManager;

    public function __construct(UserForm $userForm, UserRepository $userRepository, AuthManager $authManager)
    {
        $this->userForm = $userForm;
        $this->userRepository = $userRepository;
        $this->authManager = $authManager;
    }

    public function indexAction()
    {
        $userId = $this->identity()['id'];

        $user = null;

        /** @var User $user */
        $user = $this->userRepository->find($userId);

        if (! $user) {
            throw new \Exception('Not found object with ' . $userId);
        }

        return new ViewModel([
            'user' => $user,
        ]);
    }

    public function editAction()
    {
        $userId = $this->identity()['id'];
        $request = $this->getRequest();
        $user = null;

        /** @var User $user */
        $user = $this->userRepository->find($userId);

        if (! $user) {
            throw new \Exception('Not found object with ' . $userId);
        }

        $this->userForm->bind($user);

        if ($request->isPost()) {
            $this->userForm->getInputFilter()->get(UserForm::ROLES)->setRequired(false);
            $this->userForm->getInputFilter()->get(UserForm::DEFAULT_ROLE)->setRequired(false);
            $this->userForm->setData($request->getPost());
            if ($this->userForm->isValid()) {
                /** @var User $formUser */
                $formUser = $this->userForm->getData();

                $formUser->setEmail($user->getEmail());
                $formUser->setLogin($user->getLogin());
                $this->userRepository->merge($formUser);

                $this->getEventManager()->trigger(
                    Module::USER_UPDATED,
                    $this,
                    [
                        'user' => $formUser
                    ]
                );

                return $this->redirect()->toRoute('auth/operation/manage');
            }
        }

        return new ViewModel([
            'form' => $this->userForm,
        ]);
    }

    public function deleteAction()
    {
        $userId = $this->identity()['id'];
        $user = null;

        /** @var User $user */
        $user = $this->userRepository->find($userId);

        if (! $user) {
            throw new \Exception('Not found object with ' . $userId);
        }

        $this->userRepository->delete($user);
        $this->authManager->logout();

        return $this->redirect()->toRoute('auth/login');
    }
}