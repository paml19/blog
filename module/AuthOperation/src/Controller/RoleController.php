<?php

namespace paml\Auth\Operation\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use paml\Auth\Operation\Form\RoleForm;
use paml\Auth\Operation\Module;
use paml\Auth\Repository\RoleRepository;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class RoleController extends AbstractActionController
{
    private $roleRepository;

    private $defaults;

    private $roleForm;

    public function __construct(
        RoleRepository $roleRepository,
        array $defaults,
        RoleForm $roleForm
    ) {
        $this->roleRepository = $roleRepository;
        $this->defaults = $defaults;
        $this->roleForm = $roleForm;
    }

    public function indexAction()
    {
        $page = (int) $this->params()->fromQuery('page', $this->defaults['page']);
        $count = (int) $this->params()->fromQuery('count', $this->defaults['count']);
        $sort = (string) $this->params()->fromQuery('sort', $this->defaults['sort']);
        $order = (string) $this->params()->fromQuery('order', $this->defaults['order']);

        $list = $this->roleRepository->getList($page, $count, $sort, $order);

        $roles = new ArrayCollection();
        if ($list->count() >= $page) {
            foreach ($list as $item) {
                $roles->add($item);
            }
        }

        return new ViewModel([
            'roles' => $roles
        ]);
    }

    public function addAction()
    {
        $roleId = $this->params()->fromRoute('id', null);
        $request = $this->getRequest();

        if ($roleId) {
            $role = $this->roleRepository->find($roleId);

            if (! $role) {
                throw new \Exception('Not found object with ' . $roleId);
            }

            $this->roleForm->bind($role);
        }

        if ($request->isPost()) {
            $this->roleForm->setData($request->getPost());
            if ($this->roleForm->isValid()) {
                $role = $this->roleForm->getData();

                if ($roleId) {
                    $this->roleRepository->merge($role);

                    $this->getEventManager()->trigger(
                        Module::ROLE_UPDATED,
                        $this,
                        [
                            'role' => $role
                        ]
                    );
                } else {
                    $this->roleRepository->save($role);

                    $this->getEventManager()->trigger(
                        Module::ROLE_CREATED,
                        $this,
                        [
                            'role' => $role
                        ]
                    );
                }

                return $this->redirect()->toRoute('auth/operation/role');
            }
        }

        return new ViewModel([
            'form' => $this->roleForm
        ]);
    }

    public function deleteAction()
    {
        $roleId = $this->params()->fromRoute('id', null);

        if ($roleId) {
            $role = $this->roleRepository->find($roleId);

            if (! $role) {
                throw new \Exception('Not found object with ' . $roleId);
            }

            $this->roleRepository->delete($role);

            $this->getEventManager()->trigger(
                Module::ROLE_DELETED,
                $this,
                [
                    'role' => $role
                ]
            );
        }

        return $this->redirect()->toRoute('auth/operation/role');
    }

    public function restoreAction()
    {
        $roleId = $this->params()->fromRoute('id', null);

        if ($roleId) {
            $role = $this->roleRepository->find($roleId);

            if (! $role) {
                throw new \Exception('Not found object with ' . $roleId);
            }

            $role->setDateDelete(null);

            $this->roleRepository->merge($role);

            $this->getEventManager()->trigger(
                Module::ROLE_RESTORED,
                $this,
                [
                    'role' => $role
                ]
            );
        }

        return $this->redirect()->toRoute('auth/operation/role');
    }
}