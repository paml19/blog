<?php
namespace paml\Auth\Operation;

use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use paml\Auth\Factory\Listener\AuthListenerFactory;
use paml\Auth\Factory\Repository\ExtendedUserRepositoryFactory;
use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;

return [
    'acl' => [
        'privileges' => [
            \paml\Auth\Module::AUTH_ROLE_SUPER_ADMIN => [
                Controller\UserController::class => '*',
                Controller\RoleController::class => '*',
            ],
        ],
    ],
    'auth' => [
        'layouts' => [
            'excluded' => [
                Controller\UserController::class,
                Controller\RoleController::class,
                Controller\ManageController::class,
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\UserController::class => Factory\Controller\UserControllerFactory::class,
            Controller\RoleController::class => Factory\Controller\RoleControllerFactory::class,
            Controller\ManageController::class => Factory\Controller\ManageControllerFactory::class,
        ],
    ],
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Entity'],
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver',
                ],
            ],
        ],
    ],
    'form_elements' => [
        'factories' => [
            Form\UserForm::class => Factory\Form\UserFormFactory::class,
            Form\RoleForm::class => Factory\Form\RoleFormFactory::class,
        ],
    ],
    'hydrators' => [
        'factories' => [
            Hydrator\UserHydrator::class => Factory\Hydrator\UserHydratorFactory::class,
        ],
    ],
    'input_filters' => [
        'factories' => [
            Filter\UserFilter::class => Factory\Filter\UserFilterFactory::class,
        ],
    ],
    'router' => [
        'routes' => [
            'auth' => [
                'options' => [
                    'route'    => '/auth',
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'operation' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/operation',
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'user' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/user[/:action[/:id]]',
                                    'defaults' => [
                                        'controller' => Controller\UserController::class,
                                        'action' => 'index',
                                    ],
                                ],
                            ],
                            'role' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/role[/:action[/:id]]',
                                    'defaults' => [
                                        'controller' => Controller\RoleController::class,
                                        'action' => 'index',
                                    ],
                                ],
                            ],
                            'manage' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/manage[/:action]',
                                    'defaults' => [
                                        'controller' => Controller\ManageController::class,
                                        'action' => 'index',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            Listener\AuthListener::class => AuthListenerFactory::class,
            Repository\UserRepository::class => ExtendedUserRepositoryFactory::class,
            Repository\RoleRepository::class => Factory\Repository\RoleRepositoryFactory::class,
        ],
    ],
    'view_helpers' => [
        'factories' => [
            Helper\ManageHelper::class => Factory\Helper\ManageHelperFactory::class,
        ],
        'aliases' => [
            'manage' => Helper\ManageHelper::class,
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ]
    ],
];
