<?php
namespace paml\Notification;

use Doctrine\ORM\Mapping\Driver\AnnotationDriver;

return [
    'controller_plugins' => [
        'factories' => [
            Plugin\NotificationPlugin::class => Factory\NotificationPluginFactory::class,
        ],
        'aliases' => [
            'notification' => Plugin\NotificationPlugin::class,
        ],
    ],
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Entity'],
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver',
                ],
            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            Service\NotificationService::class => Factory\NotificationServiceFactory::class,
        ],
    ],
];
