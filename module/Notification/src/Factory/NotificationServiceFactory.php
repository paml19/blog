<?php

namespace paml\Notification\Factory;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use paml\Notification\Entity\Notification;
use paml\Notification\Service\NotificationService;
use Zend\ServiceManager\Factory\FactoryInterface;

class NotificationServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): NotificationService
    {
        return new NotificationService(
            $container->get(EntityManager::class)->getRepository(Notification::class)
        );
    }
}
