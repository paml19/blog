<?php

namespace paml\Notification\Factory;

use Interop\Container\ContainerInterface;
use paml\Notification\Plugin\NotificationPlugin;
use paml\Notification\Service\NotificationService;
use Zend\ServiceManager\Factory\FactoryInterface;

class NotificationPluginFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): NotificationPlugin
    {
        return new NotificationPlugin(
            $container->get(NotificationService::class)
        );
    }
}
