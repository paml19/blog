<?php

namespace paml\Notification\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityRepository;
use paml\Notification\Entity\Notification;

class NotificationRepository extends EntityRepository
{
    public function save(Notification $notification): void
    {
        $this->getEntityManager()->persist($notification);
        $this->getEntityManager()->flush();
    }

    public function findByPlatform(array $platforms): Collection
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder->select('n')
            ->from($this->getEntityName(), 'n')
            ->andWhere($queryBuilder->expr()->isNull('n.dateDelete'));

        foreach ($platforms as $platform) {
            $queryBuilder->orWhere(
                $queryBuilder->expr()
                ->like('n.patforms', '\'%' . $platform . '%\'')
            );
        }

        return new ArrayCollection($queryBuilder->getQuery()->getResult());
    }
}
