<?php

namespace paml\Notification\Service;

use paml\Notification\Entity\Notification;
use paml\Notification\Repository\NotificationRepository;

class NotificationService
{
    private $notificationRepository;

    public function __construct(NotificationRepository $notificationRepository)
    {
        $this->notificationRepository = $notificationRepository;
    }

    public function saveNotification($notification, ?array $platforms): void
    {
        if (is_object($notification) && $notification instanceof Notification) {
            $this->notificationRepository->save($notification);
        } else {
            $notification = (new Notification)
                ->setParams($notification)
                ->setPlatforms($platforms);

            $this->notificationRepository->save($notification);
        }
    }
}
