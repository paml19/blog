<?php

namespace paml\Notification\Plugin;

use paml\Notification\Service\NotificationService;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class NotificationPlugin extends AbstractPlugin
{
    private $notificationService;

    public function __construct(NotificationService $notificationService)
    {
        $this->notificationService = $notificationService;
    }

    public function __invoke(array $notificationParams, ?array $platforms)
    {
        $this->notificationService->saveNotification($notificationParams, $platforms);
    }
}
