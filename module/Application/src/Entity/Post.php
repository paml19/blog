<?php

declare(strict_types=1);

namespace Application\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use paml\EntityOperation\Entity\MainEntityTrait;
use paml\File\Entity\File;

/**
 * @ORM\Table(name="application_post")
 * @ORM\Entity(repositoryClass="Application\Repository\PostRepository")
 * @GEDMO\Loggable
 * @Gedmo\SoftDeleteable(fieldName="dateDelete", timeAware=true)
 */
class Post
{
    use MainEntityTrait;

    const ID = 'id';

    const DATE_ADD = 'dateAdd';

    const DATE_EDIT = 'dateEdit';

    const DATE_DELETE = 'dateDelete';

    /**
     * @ORM\Column(type="string", name="name", nullable=false)
     */
    private $name;
    const NAME = 'name';

    /**
     * @ORM\Column(type="string", name="slug", nullable=false)
     * @Gedmo\Slug(fields={"name"})
     */
    private $slug;
    const SLUG = 'slug';

    /**
     * @ORM\Column(type="text", name="cont", nullable=true)
     */
    private $cont;
    const CONTENT = 'cont';

    /**
     * @ORM\Column(type="boolean", name="visible", nullable=false)
     */
    private $visible = false;
    const VISIBLE = 'visible';

    /**
     * @ORM\ManyToOne(targetEntity="Application\Entity\Category", inversedBy="posts")
     * @ORM\JoinColumn(name="category")
     */
    private $category;
    const CATEGORY = 'category';

    /**
     * @ORM\ManyToMany(targetEntity="Application\Entity\Tag", inversedBy="posts")
     * @ORM\JoinTable(name="application_posts_tags")
     */
    private $tags;
    const TAGS = 'tags';
    const TAG = 'tag';

    /**
     * @ORM\OneToMany(targetEntity="Application\Entity\Comment", mappedBy="post")
     * @ORM\OrderBy({"dateAdd" = "DESC"})
     */
    private $comments;
    const COMMENTS = 'comments';
    const COMMENT = 'comment';

    /**
     * @ORM\OneToOne(targetEntity="paml\File\Entity\File")
     * @ORM\JoinColumn(name="file", referencedColumnName="id")
     */
    private $file;
    const FILE = 'file';

    public function __construct()
    {
        $this->comments = new ArrayCollection;
        $this->tags = new ArrayCollection;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(Category $category): self
    {
        $this->category = $category;
        return $this;
    }

    public function getCont(): ?string
    {
        return $this->cont;
    }

    public function setCont(string $cont): self
    {
        $this->cont = $cont;
        return $this;
    }

    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function setComments(Collection $comments): self
    {
        $this->comments = $comments;
        return $this;
    }

    public function addComment(Comment $comment): self
    {
        if (! $this->comments->contains($comment)) {
            $this->comments->add($comment);
        }

        return  $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
        }

        return  $this;
    }

    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function setTags(Collection $tags): self
    {
        $this->tags = $tags;
        return $this;
    }

    public function addTag(Tag $tag): self
    {
        if (! $this->tags->contains($tag)) {
            $this->tags->add($tag);
        }

        return $this;
    }

    public function removeTag(Tag $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
            $tag->removePost($this);
        }

        return $this;
    }

    public function isVisible(): bool
    {
        return $this->visible;
    }

    public function setVisible(bool $visible): Post
    {
        $this->visible = $visible;
        return $this;
    }

    public function getFile(): ?File
    {
        return $this->file;
    }

    public function setFile(File $file): self
    {
        $this->file = $file;
        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;
        return $this;
    }

    public function setDateAdd(\DateTime $dateAdd): self
    {
        $this->dateAdd = $dateAdd;
        return $this;
    }
}
