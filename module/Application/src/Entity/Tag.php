<?php

declare(strict_types=1);

namespace Application\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use paml\EntityOperation\Entity\MainEntityTrait;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="application_tag")
 * @ORM\Entity(repositoryClass="Application\Repository\TagRepository")
 * @Gedmo\SoftDeleteable(fieldName="dateDelete", timeAware=true)
 */
class Tag
{
    use MainEntityTrait;

    const ID = 'id';

    const DATE_ADD = 'dateAdd';

    const DATE_EDIT = 'dateEdit';

    const DATE_DELETE = 'dateDelete';

    /**
     * @ORM\Column(type="string", name="name", nullable=false)
     */
    private $name;
    const NAME = 'name';

    /**
     * @ORM\Column(type="string")
     * @Gedmo\Slug(fields={"name"})
     */
    private $slug;
    const SLUG = 'slug';

    /**
     * @ORM\Column(type="boolean", name="visible", nullable=false)
     */
    private $visible = false;
    const VISIBLE = 'visible';

    /**
     * @ORM\ManyToMany(targetEntity="Application\Entity\Post", mappedBy="tags")
     */
    private $posts;
    const POSTS = 'posts';
    const POST = 'post';

    public function __construct()
    {
        $this->posts = new ArrayCollection;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Tag
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     * @return Tag
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return bool
     */
    public function isVisible(): bool
    {
        return $this->visible;
    }

    /**
     * @param bool $visible
     * @return Tag
     */
    public function setVisible(bool $visible): Tag
    {
        $this->visible = $visible;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    /**
     * @param ArrayCollection $posts
     * @return Tag
     */
    public function setPosts(Collection $posts): Tag
    {
        $this->posts = $posts;
        return $this;
    }

    public function addPost(Post $post): self
    {
        if (! $this->posts->contains($post)) {
            $this->posts->add($post);
        }

        return $this;
    }

    public function removePost(Post $post): self
    {
        if ($this->posts->contains($post)) {
            $this->posts->removeElement($post);
        }

        return $this;
    }
}