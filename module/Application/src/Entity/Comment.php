<?php

namespace Application\Entity;

use paml\EntityOperation\Entity\MainEntityTrait;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="application_comment")
 * @ORM\Entity(repositoryClass="Application\Repository\CommentRepository")
 * @GEDMO\Loggable
 * @Gedmo\SoftDeleteable(fieldName="dateDelete", timeAware=true)
 */
class Comment
{
    use MainEntityTrait;

    const ID = 'id';

    const DATE_ADD = 'dateAdd';

    const DATE_EDIT = 'dateEdit';

    const DATE_DELETE = 'dateDelete';

    /**
     * @ORM\Column(type="string", name="name", nullable=false)
     */
    private $name;
    const NAME = 'name';

    /**
     * @ORM\Column(type="text", name="cont", nullable=true)
     */
    private $cont;
    const CONTENT = 'cont';

    /**
     * @ORM\ManyToOne(targetEntity="Application\Entity\Post", inversedBy="comments")
     * @ORM\JoinColumn(name="post", referencedColumnName="id")
     */
    private $post;
    const POST = 'post';

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getCont(): ?string
    {
        return $this->cont;
    }

    public function setCont(string $cont): self
    {
        $this->cont = $cont;
        return $this;
    }

    public function getPost(): ?Post
    {
        return $this->post;
    }

    public function setPost(Post $post): self
    {
        $this->post = $post;
        return $this;
    }

    public function setDateAdd(\DateTime $dateAdd): self
    {
        $this->dateAdd = $dateAdd;
        return $this;
    }
}
