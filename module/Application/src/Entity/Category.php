<?php

declare(strict_types=1);

namespace Application\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use paml\EntityOperation\Entity\MainEntityTrait;

/**
 * @ORM\Table(name="application_category")
 * @ORM\Entity(repositoryClass="Application\Repository\CategoryRepository")
 * @GEDMO\Loggable
 * @Gedmo\SoftDeleteable(fieldName="dateDelete", timeAware=false, hardDelete=false)
 */
class Category
{
    use MainEntityTrait;

    const ID = 'id';

    const DATE_ADD = 'dateAdd';

    const DATE_EDIT = 'dateEdit';

    const DATE_DELETE = 'dateDelete';

    /**
     * @ORM\Column(type="string", name="name", nullable=false)
     */
    private $name;
    const NAME = 'name';

    /**
     * @ORM\OneToMany(targetEntity="Application\Entity\Category", mappedBy="parent")
     */
    private $children;
    const CHILDREN = 'children';
    const CHILD = 'child';

    /**
     * @ORM\ManyToOne(targetEntity="Application\Entity\Category", inversedBy="children")
     * @ORM\JoinColumn(name="parent", referencedColumnName="id")
     */
    private $parent;
    const PARENT = 'parent';

    /**
     * @ORM\OneToMany(targetEntity="Application\Entity\Post", mappedBy="category")
     */
    private $posts;
    const POSTS = 'posts';
    const POST = 'post';

    /**
     * @ORM\Column(type="boolean", name="visible", nullable=false)
     */
    private $visible = false;
    const VISIBLE = 'visible';

    /**
     * @ORM\Column(type="string", name="slug", nullable=false)
     * @Gedmo\Slug(fields={"name"})
     */
    private $slug;
    const SLUG = 'slug';

    public function __construct()
    {
        $this->children = new ArrayCollection;
        $this->posts = new ArrayCollection;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function setChildren(Collection $children): self
    {
        $this->children = $children;
        return $this;
    }

    public function addChild(Category $child): self
    {
        if (! $this->children->contains($child)) {
            $this->children->add($child);
        }

        return $this;
    }

    public function removeChild(Category $child): self
    {
        if ($this->children->contains($child)) {
            $this->children->removeElement($child);
        }

        return $this;
    }

    public function getParent(): ?Category
    {
        return $this->parent;
    }

    public function setParent(?Category $parent): self
    {
        $this->parent = $parent;
        if ($parent) {
            $parent->addChild($this);
        }
        return $this;
    }

    public function getPosts(): ?Collection
    {
        return $this->posts;
    }

    public function setPosts(Collection $posts): self
    {
        $this->posts = $posts;
        return $this;
    }

    public function addPost(Post $post): self
    {
        if (! $this->posts->contains($post)) {
            $this->posts->add($post);
        }

        return $this;
    }

    public function removePost(Post $post): self
    {
        if ($this->posts->contains($post)) {
            $this->posts->removeElement($post);
        }

        return $this;
    }

    public function isVisible(): bool
    {
        return $this->visible;
    }

    public function setVisible(bool $visible): self
    {
        $this->visible = $visible;
        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;
        return $this;
    }

    public function setDateAdd(\DateTime $dateAdd): self
    {
        $this->dateAdd = $dateAdd;
        return $this;
    }
}
