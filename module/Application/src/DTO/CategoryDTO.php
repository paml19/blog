<?php

declare(strict_types=1);

namespace Application\DTO;

use paml\Layouts\ColorAdmin\Plugin\DataTableButtons;

/**
 * Class CategoryDTO
 * @package Application\DTO
 */
class CategoryDTO
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var bool
     */
    private $active;

    /**
     * @var \DateTime
     */
    private $dateAdd;

    /**
     * @var int
     */
    private $postsCount;

    /**
     * CategoryDTO constructor.
     * @param string $id
     * @param string $name
     * @param bool $active
     * @param \DateTime $dateAdd
     * @param int $postsCount
     */
    public function __construct(string $id, string $name, bool $active, \DateTime $dateAdd, int $postsCount)
    {
        $this->id = $id;
        $this->name = $name;
        $this->active = $active;
        $this->dateAdd = $dateAdd;
        $this->postsCount = $postsCount;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @return \DateTime
     */
    public function getDateAdd(): \DateTime
    {
        return $this->dateAdd;
    }

    /**
     * @return int
     */
    public function getPostsCount(): int
    {
        return $this->postsCount;
    }

    /**
     * @param DataTableButtons $dataTableButtons
     * @return array
     */
    public function toArray(DataTableButtons $dataTableButtons): array
    {
        return [
            'name' => $this->name,
            'visible' => $this->active ? 'Yes' : 'No',
            'dateAdd' => $this->dateAdd ? $this->dateAdd->format('d.m.Y H:i:s') : '',
            'id' => $dataTableButtons->prepareButtons($this->id),
            'postsCount' => $this->postsCount,
        ];
    }
}