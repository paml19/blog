<?php

namespace Application\DTO;

use paml\Layouts\ColorAdmin\Plugin\DataTableButtons;

/**
 * Class CommentDTO
 * @package Application\DTO
 */
class CommentDTO
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $cont;

    /**
     * @var \DateTime
     */
    private $dateAdd;

    /**
     * CommentDTO constructor.
     * @param string $name
     * @param \DateTime $dateAdd
     * @param string $cont
     */
    public function __construct(string $name, \DateTime $dateAdd, string $cont)
    {
        $this->name = $name;
        $this->dateAdd = $dateAdd;
        $this->cont = $cont;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return \DateTime
     */
    public function getDateAdd(): \DateTime
    {
        return $this->dateAdd;
    }

    /**
     * @return string
     */
    public function getCont(): string
    {
        return $this->cont;
    }

    /**
     * @param DataTableButtons $dataTableButtons
     * @return array
     */
    public function toArray(DataTableButtons $dataTableButtons): array
    {
        return [
            'name' => $this->name,
            'dateAdd' => $this->dateAdd ? $this->dateAdd->format('d.m.Y H:i:s') : '',
            'cont' => $this->cont,
        ];
    }
}
