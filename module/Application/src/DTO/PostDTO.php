<?php

namespace Application\DTO;

use paml\Layouts\ColorAdmin\Plugin\DataTableButtons;

/**
 * Class PostDTO
 * @package Application\DTO
 */
class PostDTO
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var bool
     */
    private $active;

    /**
     * @var \DateTime
     */
    private $dateAdd;

    /**
     * @var int
     */
    private $commentsCount;

    /**
     * PostDTO constructor.
     * @param string $id
     * @param string $name
     * @param bool $active
     * @param \DateTime $dateAdd
     * @param int $commentsCount
     */
    public function __construct(string $id, string $name, bool $active, \DateTime $dateAdd, int $commentsCount)
    {
        $this->id = $id;
        $this->name = $name;
        $this->active = $active;
        $this->dateAdd = $dateAdd;
        $this->commentsCount = $commentsCount;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @return \DateTime
     */
    public function getDateAdd(): \DateTime
    {
        return $this->dateAdd;
    }

    /**
     * @return int
     */
    public function getCommentsCount(): int
    {
        return $this->commentsCount;
    }

    /**
     * @param DataTableButtons $dataTableButtons
     * @return array
     */
    public function toArray(DataTableButtons $dataTableButtons): array
    {
        return [
            'name' => $this->name,
            'visible' => $this->active ? 'Yes' : 'No',
            'dateAdd' => $this->dateAdd ? $this->dateAdd->format('d.m.Y H:i:s') : '',
            'id' => $dataTableButtons->prepareButtons($this->id),
            'commentsCount' => $this->commentsCount,
        ];
    }
}
