<?php

declare(strict_types=1);

namespace Application\Hydrator\View;

use Application\Entity\Category;
use Application\Repository\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use paml\EntityOperation\Hydrator\HydratorInterface;
use Zend\Hydrator\AbstractHydrator;

class CategoryHydrator extends AbstractHydrator implements HydratorInterface
{
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function hydrate(array $data, $object)
    {
        if (isset($data[Category::NAME])) {
            $object->setName($data[Category::NAME]);
            $slug = str_replace(' ', '-', strtolower($data[Category::NAME]));
            $slugsCount = $this->categoryRepository->count(['slug' => $slug]);

            if ($object->getSlug() == '') {
                $object->setSlug($slug . ($slugsCount ? ($slugsCount + 1) : null));
            }
        }

        if (isset($data[Category::PARENT]) && $data[Category::PARENT] != '') {
            $parent = $this->categoryRepository->find($data[Category::PARENT]);
            $object->setParent($parent);
        }

        if (isset($data[Category::CHILDREN])) {
            foreach ($data[Category::CHILDREN] as $childId) {
                if (! is_array($childId)) {
                    $child = $this->categoryRepository->find($childId);
                } else {
                    $child = $this->categoryRepository->find($childId[Category::ID]);
                }
                $object->addChild($child);
            }
        }

        if (isset($data[Category::VISIBLE])) {
            if ($data[Category::VISIBLE] == 'yes') {
                $object->setVisible(true);
            } else {
                $object->setVisible(false);
            }
        }

        return $object;
    }

    public function extract($object)
    {
        $posts = new ArrayCollection;
        $children = new ArrayCollection;

        foreach ($object->getPosts() as $post) {
            if (! $post->getDateDelete()) {
                $posts->add($post);
            }
        }

        foreach ($object->getChildren() as $child) {
            if (! $child->getDateDelete()) {
                $children->add($child);
            }
        }

        return [
            Category::ID => $object->getId(),
            Category::NAME => $object->getName(),
            Category::PARENT => $object->getParent() ? $object->getParent()->getId() : null,
            Category::DATE_ADD => $object->getDateAdd() ? $object->getDateAdd()->format('d.m.Y H:i:s') : null,
            Category::DATE_EDIT => $object->getDateEdit() ? $object->getDateEdit()->format('d.m.Y H:i:s') : null,
            Category::POSTS => $posts->toArray(),
            Category::CHILDREN => $children->toArray(),
            Category::VISIBLE => $object->isVisible() ? 'yes' : 'no',
        ];
    }
}
