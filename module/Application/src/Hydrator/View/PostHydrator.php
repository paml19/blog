<?php

declare(strict_types=1);

namespace Application\Hydrator\View;

use Application\Entity\Post;
use Application\Entity\Tag;
use Application\Repository\CategoryRepository;
use Application\Repository\PostRepository;
use Application\Repository\TagRepository;
use Doctrine\Common\Collections\ArrayCollection;
use paml\EntityOperation\Hydrator\HydratorInterface;
use paml\File\Entity\File;
use paml\File\Library\Hydrator\FileHydrator;
use Zend\Hydrator\AbstractHydrator;

class PostHydrator extends AbstractHydrator implements HydratorInterface
{
    private $categoryRepository;

    private $fileHydrator;

    private $postRepository;

    private $tagRepository;

    private $categoryHydrator;

    public function __construct(
        CategoryRepository $categoryRepository,
        FileHydrator $fileHydrator,
        PostRepository $postRepository,
        TagRepository $tagRepository,
        CategoryHydrator $categoryHydrator
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->fileHydrator = $fileHydrator;
        $this->postRepository = $postRepository;
        $this->tagRepository = $tagRepository;
        $this->categoryHydrator = $categoryHydrator;
    }

    public function extract($object)
    {
        $comments = new ArrayCollection;
        $tags = new ArrayCollection;

        foreach ($object->getComments() as $comment) {
            if (! $comment->getDateDelete()) {
                $comments->add($comment->getId());
            }
        }

        foreach ($object->getTags() as $tag) {
            if (! $tag->getDateDelete()) {
                $tags->add($tag->getId());
            }
        }

        return [
            Post::ID => $object->getId(),
            Post::NAME => $object->getName(),
            Post::CONTENT => $object->getCont(),
            Post::DATE_ADD => $object->getDateAdd() ? $object->getDateAdd()->format('d.m.Y H:i:s') : null,
            Post::DATE_EDIT => $object->getDateEdit() ? $object->getDateEdit()->format('d.m.Y H:i:s') : null,
            Post::DATE_DELETE => $object->getDateDelete() ? $object->getDateDelete()->format('d.m.Y H:i:s') : null,
            Post::CATEGORY => $object->getCategory() ? $object->getCategory()->getId() : null,
            Post::COMMENTS => $comments->toArray(),
            Post::VISIBLE => $object->isVisible() ? 'yes' : 'no',
            Post::FILE => $object->getFile() ? $this->fileHydrator->extract($object->getFile()) : null,
            Post::SLUG => $object->getSlug(),
            Post::TAGS => $tags->toArray(),
        ];
    }

    public function hydrate(array $data, $object)
    {
        if (isset($data[Post::NAME])) {
            $object->setName($data[Post::NAME]);
            $slug = str_replace(' ', '-', strtolower($data[Post::NAME]));
            $slugsCount = $this->postRepository->count(['slug' => $slug]);

            if ($object->getSlug() == '') {
                $object->setSlug($slug . ($slugsCount ? ($slugsCount + 1) : null));
            }
        }

        if (isset($data[Post::CATEGORY]) && ! empty($data[Post::CATEGORY])) {
            $category = $this->categoryRepository->find($data[Post::CATEGORY]);
            $object->setCategory($category);
        }

        if (isset($data[Post::TAGS])) {
            $object->setTags(new ArrayCollection);
            foreach ($data[Post::TAGS] as $tagId) {
                if ($tagId != '') {
                    if (! is_array($tagId)) {
                        $tag = $this->tagRepository->find($tagId);
                    } else {
                        $tag = $this->tagRepository->find($tagId[Tag::ID]);
                    }

                    $object->addTag($tag);
                }
            }
        }

        if (isset($data[Post::CONTENT])) {
            $object->setCont($data[Post::CONTENT]);
        }

        if (isset($data[Post::VISIBLE])) {
            if ($data[Post::VISIBLE] == 'yes') {
                $object->setVisible(true);
            } else {
                $object->setVisible(false);
            }
        }

        if (isset($data[Post::FILE]) && $data[Post::FILE]['name'] != '') {
            $data['catalog_name'] = $data[Post::NAME];
            $file = $this->fileHydrator->hydrate($data, (new File));
            $object->setFile($file);
        }

        return $object;
    }
}
