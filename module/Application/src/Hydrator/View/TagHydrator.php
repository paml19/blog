<?php

declare(strict_types=1);

namespace Application\Hydrator\View;

use Application\Entity\Post;
use Application\Entity\Tag;
use Doctrine\Common\Collections\ArrayCollection;
use paml\EntityOperation\Hydrator\HydratorInterface;
use Zend\Hydrator\AbstractHydrator;

class TagHydrator extends AbstractHydrator implements HydratorInterface
{
    public function extract($object)
    {
        $posts = new ArrayCollection;

        /** @var Post $post */
        foreach ($object->getPosts() as $post) {
            if (! $post->getDateDelete()) {
                $posts->add($post->getId());
            }
        }

        return [
            Tag::ID => $object->getId(),
            Tag::NAME => $object->getName(),
            Tag::SLUG => $object->getSlug(),
            Tag::DATE_ADD => $object->getDateAdd() ? $object->getDateAdd()->format('d.m.Y H:i:s') : null,
            Tag::DATE_EDIT => $object->getDateEdit() ? $object->getDateEdit()->format('d.m.Y H:i:s') : null,
            Tag::DATE_DELETE => $object->getDateDelete() ? $object->getDateDelete()->format('d.m.Y H:i:s') : null,
            Tag::VISIBLE => $object->isVisible(),
            Tag::POSTS => $posts->toArray()
        ];
    }

    /**
     * Hydrate $object with the provided $data.
     *
     * @param array $data
     * @param Tag $object
     * @return object
     */
    public function hydrate(array $data, $object)
    {
        if (isset($data[Tag::NAME])) {
            $object->setName($data[Tag::NAME]);
        }

        if (isset($data[Tag::VISIBLE])) {
            if ($data[Tag::VISIBLE] == 'yes') {
                $object->setVisible(true);
            } else {
                $object->setVisible(false);
            }
        }

        return $object;
    }
}
