<?php

declare(strict_types=1);

namespace Application\Hydrator\Api;

use Application\Entity\Category;
use Application\Entity\Task;
use paml\EntityOperation\Hydrator\HydratorInterface;
use paml\EntityOperation\Hydrator\ListHydratorTrait;
use Zend\Hydrator\AbstractHydrator;

class CategoryListHydrator extends AbstractHydrator implements HydratorInterface
{
    use ListHydratorTrait;

    /**
     * @param Task $object
     * @return array
     */
    public function extract($object)
    {
        return [
            Category::ID => $object->getId(),
            Category::NAME => $object->getName(),
            Category::DATE_ADD => $object->getDateAdd() ? $object->getDateAdd()->format('d.m.Y H:i:s') : '',
        ];
    }
}
