<?php

declare(strict_types=1);

namespace Application\Hydrator\Api;

use Application\Entity\Comment;
use Application\Repository\PostRepository;
use paml\EntityOperation\Hydrator\HydratorInterface;
use Zend\Hydrator\AbstractHydrator;
use Zend\Hydrator\Strategy\StrategyChain;

class CommentHydrator extends AbstractHydrator implements HydratorInterface
{
    private $postListHydrator;

    private $postRepository;

    public function __construct(PostListHydrator $postHydrator, PostRepository $postRepository)
    {
        $this->postListHydrator = $postHydrator;
        $this->postRepository = $postRepository;
    }

    public function extract($object)
    {
        return [
            Comment::ID => $object->getId(),
            Comment::DATE_ADD => $object->getDateAdd() ? $object->getDateAdd()->format('d.m.Y H:i:s') : null,
            Comment::DATE_EDIT => $object->getDateEdit() ? $object->getDateEdit()->format('d.m.Y H:i:s') : null,
            Comment::DATE_DELETE => $object->getDateDelete() ? $object->getDateDelete()->format('d.m.Y H:i:s') : null,
            Comment::NAME => $object->getName(),
            Comment::CONTENT => $object->getCont(),
            Comment::POST => $object->getPost() ? $this->postListHydrator->extract($object->getPost()) : null,
        ];
    }

    public function hydrate(array $data, $object)
    {
        if (isset($data[Comment::NAME])) {
            $object->setName($data[Comment::NAME]);
        }

        if (isset($data[Comment::CONTENT])) {
            $object->setCont($data[Comment::CONTENT]);
        }

        if (isset($data[Comment::POST])) {
            $object->setPost($this->postRepository->find($data[Comment::POST]));
        }

        return  $object;
    }
}
