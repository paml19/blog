<?php

declare(strict_types=1);

namespace Application\Hydrator\Api;

use Application\Entity\Comment;
use paml\EntityOperation\Hydrator\HydratorInterface;
use paml\EntityOperation\Hydrator\ListHydratorTrait;
use Zend\Hydrator\AbstractHydrator;

class CommentListHydrator extends AbstractHydrator implements HydratorInterface
{
    use ListHydratorTrait;

    public function extract($object)
    {
        return [
            Comment::ID => $object->getId(),
            Comment::DATE_ADD => $object->getDateAdd() ? $object->getDateAdd()->format('d.m.Y H:i:s') : '',
        ];
    }
}
