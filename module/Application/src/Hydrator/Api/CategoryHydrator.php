<?php

declare(strict_types=1);

namespace Application\Hydrator\Api;

use Application\Entity\Category;
use Application\Entity\Post;
use Application\Repository\CategoryRepository;
use Application\Repository\PostRepository;
use Doctrine\Common\Collections\ArrayCollection;
use paml\EntityOperation\Hydrator\HydratorInterface;
use Zend\Hydrator\AbstractHydrator;

/**
 * Class CategoryHydrator
 * @package Application\Hydrator\Api
 */
class CategoryHydrator extends AbstractHydrator implements HydratorInterface
{
    /**
     * @var PostListHydrator
     */
    private $postListHydrator;

    /**
     * @var CategoryListHydrator
     */
    private $categoryListHydrator;

    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * @var PostRepository
     */
    private $postRepository;

    /**
     * CategoryHydrator constructor.
     * @param PostListHydrator $postHydrator
     * @param CategoryListHydrator $categoryListHydrator
     * @param CategoryRepository $categoryRepository
     * @param PostRepository $postRepository
     */
    public function __construct(
        PostListHydrator $postHydrator,
        CategoryListHydrator $categoryListHydrator,
        CategoryRepository $categoryRepository,
        PostRepository $postRepository
    ) {
        $this->postListHydrator = $postHydrator;
        $this->categoryListHydrator = $categoryListHydrator;
        $this->categoryRepository = $categoryRepository;
        $this->postRepository = $postRepository;
    }

    /**
     * @param Category $object
     * @return array
     */
    public function extract($object)
    {
        $posts = new ArrayCollection;
        $children = new ArrayCollection;

        foreach ($object->getPosts() as $post) {
            if (! $post->getDateDelete()) {
                $posts->add($this->postListHydrator->extract($post));
            }
        }

        foreach ($object->getChildren() as $child) {
            if (! $child->getDateDelete()) {
                $children->add($this->categoryListHydrator->extract($child));
            }
        }

        return [
            Category::ID => $object->getId(),
            Category::NAME => $object->getName(),
            Category::PARENT => $object->getParent() ? $object->getParent()->getId() : null,
            Category::DATE_ADD => $object->getDateAdd() ? $object->getDateAdd()->format('d.m.Y H:i:s') : null,
            Category::DATE_EDIT => $object->getDateEdit() ? $object->getDateEdit()->format('d.m.Y H:i:s') : null,
            Category::POSTS => $posts->toArray(),
            Category::CHILDREN => $children->toArray(),
            Category::VISIBLE => $object->isVisible(),
        ];
    }

    /**
     * Hydrate $object with the provided $data.
     *
     * @param array $data
     * @param Category $object
     * @return object
     */
    public function hydrate(array $data, $object)
    {
        if (isset($data[Category::NAME])) {
            $object->setName($data[Category::NAME]);
            $slug = str_replace(' ', '-', strtolower($data[Category::NAME]));
            $slugsCount = $this->categoryRepository->count(['slug' => $slug]);

            if ($object->getSlug() == '') {
                $object->setSlug($slug . ($slugsCount ? ($slugsCount + 1) : null));
            }
        }

        if (isset($data[Category::PARENT])) {
            $parent = $this->categoryRepository->find($data[Category::PARENT]);
            $object->setParent($parent);
        }

        if (isset($data[Category::CHILD])) {
            $child = $this->categoryRepository->find($data[Category::CHILD]);
            $object->addChild($child);
        }

        if (isset($data[Category::CHILDREN])) {
            foreach ($data[Category::CHILDREN] as $childId) {
                if (! is_array($childId)) {
                    $child = $this->categoryRepository->find($childId);
                } else {
                    $child = $this->categoryRepository->find($childId[Category::ID]);
                }
                $object->addChild($child);
            }
        }

        if (isset($data[Category::POST])) {
            $task = $this->postRepository->find($data[Category::POST]);
            $object->addPost($task);
        }

        if (isset($data[Category::POSTS])) {
            foreach ($data[Category::POSTS] as $taskId) {
                if (! is_array($taskId)) {
                    $task = $this->postRepository->find($taskId);
                } else {
                    $task = $this->postRepository->find($taskId[Post::ID]);
                }
                $object->addPost($task);
            }
        }

        if (isset($data[Category::VISIBLE])) {
            if ($data[Category::VISIBLE] == 'yes') {
                $object->setVisible(true);
            } else {
                $object->setVisible(false);
            }
        }

        return $object;
    }
}
