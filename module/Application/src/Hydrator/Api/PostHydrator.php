<?php

declare(strict_types=1);

namespace Application\Hydrator\Api;

use Application\Entity\Category;
use Application\Entity\Post;
use Application\Repository\CategoryRepository;
use Application\Repository\PostRepository;
use Doctrine\Common\Collections\ArrayCollection;
use paml\EntityOperation\Hydrator\HydratorInterface;
use paml\File\Entity\File;
use paml\File\Library\Hydrator\FileHydrator;
use Zend\Hydrator\AbstractHydrator;

class PostHydrator extends AbstractHydrator implements HydratorInterface
{
    private $categoryHydrator;

    private $categoryRepository;

    private $commentListHydrator;

    private $fileHydrator;

    private $postRepository;

    public function __construct(
        CategoryHydrator $categoryHydrator,
        CategoryRepository $categoryRepository,
        CommentListHydrator $commentListHydrator,
        PostRepository $postRepository,
        FileHydrator $fileHydrator
    ) {
        $this->categoryHydrator = $categoryHydrator;
        $this->categoryRepository = $categoryRepository;
        $this->commentListHydrator = $commentListHydrator;
        $this->fileHydrator = $fileHydrator;
        $this->postRepository = $postRepository;
    }

    /**
     * @param Task $object
     * @return array
     */
    public function extract($object)
    {
        $comments = new ArrayCollection;

        foreach ($object->getComments() as $comment) {
            if (! $comment->getDateDelete()) {
                $comments->add($this->commentListHydrator->extract($comment));
            }
        }

        return [
            Post::ID => $object->getId(),
            Post::NAME => $object->getName(),
            Post::CONTENT => $object->getCont(),
            Post::DATE_ADD => $object->getDateAdd() ? $object->getDateAdd()->format('d.m.Y H:i:s') : null,
            Post::DATE_EDIT => $object->getDateEdit() ? $object->getDateEdit()->format('d.m.Y H:i:s') : null,
            Post::DATE_DELETE => $object->getDateDelete() ? $object->getDateDelete()->format('d.m.Y H:i:s') : null,
            Post::CATEGORY => $object->getCategory() ? $this->categoryHydrator->extract($object->getCategory()) : null,
            Post::COMMENTS => $comments->toArray(),
            Post::VISIBLE => $object->isVisible(),
            Post::FILE => $object->getFile() ? $this->fileHydrator->extract($object->getFile()) : null,
            Post::SLUG => $object->getSlug()
        ];
    }

    /**
     * Hydrate $object with the provided $data.
     *
     * @param array $data
     * @param Post $object
     * @return object
     */
    public function hydrate(array $data, $object)
    {
        if (isset($data[Post::NAME])) {
            $object->setName($data[Post::NAME]);
            $slug = str_replace(' ', '-', strtolower($data[Post::NAME]));
            $slugsCount = $this->postRepository->count(['slug' => $slug]);

            if ($object->getSlug() == '') {
                $object->setSlug($slug . ($slugsCount ? ($slugsCount + 1) : null));
            }
        }

        if (isset($data[Post::CATEGORY])) {
            $category = $this->categoryRepository->find($data[Post::CATEGORY]);
            $object->setCategory($category);
        }

        if (isset($data[Post::CONTENT])) {
            $object->setCont($data[Post::CONTENT]);
        }

        if (isset($data[Post::VISIBLE])) {
            if ($data[Post::VISIBLE] == 'yes') {
                $object->setVisible(true);
            } else {
                $object->setVisible(false);
            }
        }

        if (isset($data[Post::FILE]) && $data[Post::FILE]['name'] != '') {
            $data['catalog_name'] = $data[Post::NAME];
            $file = $this->fileHydrator->hydrate($data, (new File));
            $object->setFile($file);
        }

        return $object;
    }
}
