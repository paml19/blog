<?php

declare(strict_types=1);

namespace Application\Hydrator\Api;

use Application\Entity\Post;
use paml\EntityOperation\Hydrator\HydratorInterface;
use paml\EntityOperation\Hydrator\ListHydratorTrait;
use Zend\Hydrator\AbstractHydrator;

/**
 * Class PostListHydrator
 * @package Application\Hydrator\Api
 */
class PostListHydrator extends AbstractHydrator implements HydratorInterface
{
    use ListHydratorTrait;

    /**
     * @param Post $object
     * @return array
     */
    public function extract($object)
    {
        return [
            Post::ID => $object->getId(),
            Post::NAME => $object->getName(),
            Post::DATE_ADD => $object->getDateAdd() ? $object->getDateAdd()->format('d.m.Y H:i:s') : ''
        ];
    }
}
