<?php

namespace Application\Controller\Admin;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;

class DashboardController extends AbstractActionController
{
    public function onDispatch(MvcEvent $e)
    {
        $this->layout()->setTemplate('layout/color-admin');
        return parent::onDispatch($e);
    }
}
