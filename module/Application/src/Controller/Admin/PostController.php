<?php

namespace Application\Controller\Admin;

use Application\DTO\PostDTO;
use paml\EntityView\Controller\AbstractViewController;
use paml\Layouts\ColorAdmin\Service\Query\DataTableService;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class PostController extends AbstractViewController
{
    const HEADERS = [
        [
            'name' => 'Name',
            'data' => 'name',
        ],
        [
            'name' => 'Date add',
            'data' => 'dateAdd',
        ],
        [
            'name' => 'Comments',
            'data' => 'commentsCount',
        ],
        [
            'name' => 'Active',
            'data' => 'visible',
        ],
        [
            'name' => '',
            'data' => 'id',
            'orderable' => false,
            'width' => '85px',
        ],
    ];

    const COMMENT_HEADER = [
        [
            'name' => 'Author',
            'data' => 'name',
        ],
        [
            'name' => 'Content',
            'data' => 'cont',
        ],
        [
            'name' => 'Date add',
            'data' => 'dateAdd',
        ],
    ];

    /** @var DataTableService */
    private $dataTableService;

    public function onDispatch(MvcEvent $e)
    {
        $this->layout()->setTemplate('layout/color-admin');
        return parent::onDispatch($e);
    }

    public function indexAction()
    {
        return new ViewModel([
            'headers' => self::HEADERS,
            'url' => '/admin/post/asyncList',
        ]);
    }

    public function addAction()
    {
        $viewModel = parent::addAction();

        if ($viewModel instanceof ViewModel) {

            $commentsUrl = null;
            if (isset($viewModel->getVariables()['object'])) {
                $commentsUrl = '/admin/comment/asyncList?post=' . $viewModel->getVariable('object')->getId();
            }

            $params = [
                'headers' => self::COMMENT_HEADER,
                'url' => $commentsUrl,
            ];

            return $viewModel->setVariables(array_merge($params, $viewModel->getVariables()));
        }

        return $viewModel;
    }

    public function asyncListAction()
    {
        $dataTableParameters = $this->dataTableService->parseFromQuery($this->params()->fromQuery());

        $posts = $this->entityRepository->filter($dataTableParameters);
        $postsCount = $this->entityRepository->filterCount($dataTableParameters);

        return new JsonModel([
            'data' => array_map(function (PostDTO $postDTO) {
                return $postDTO->toArray($this->buttons('admin/front', ['controller' => 'post']));
            }, $posts),
            'draw' => $dataTableParameters->getDraw(),
            'recordsTotal' => count($postsCount),
            'recordsFiltered' => count($postsCount),
        ]);
    }

    public function setDataTableService(DataTableService $dataTableService): void
    {
        $this->dataTableService = $dataTableService;
    }
}
