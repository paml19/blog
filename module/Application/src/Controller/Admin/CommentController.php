<?php

namespace Application\Controller\Admin;

use Application\DTO\CommentDTO;
use Application\Repository\CommentRepository;
use paml\Layouts\ColorAdmin\Service\Query\DataTableService;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

class CommentController extends AbstractActionController
{
    private $dataTableService;

    private $commentRepository;

    public function __construct(DataTableService $dataTableService, CommentRepository $commentRepository)
    {
        $this->dataTableService = $dataTableService;
        $this->commentRepository = $commentRepository;
    }

    public function asyncListAction()
    {
        $postId = $this->params()->fromQuery('post');
        $dataTableParameters = $this->dataTableService->parseFromQuery($this->params()->fromQuery());

        $comments = $this->commentRepository->filter($dataTableParameters, $postId);
        $commentsCount = $this->commentRepository->filterCount($dataTableParameters, $postId);

        return new JsonModel([
            'data' => array_map(function (CommentDTO $commentDTO) {
                return $commentDTO->toArray($this->buttons('admin/front', ['controller' => 'post']));
            }, $comments),
            'draw' => $dataTableParameters->getDraw(),
            'recordsTotal' => count($commentsCount),
            'recordsFiltered' => count($commentsCount),
        ]);
    }
}