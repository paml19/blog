<?php

namespace Application\Controller\Admin;

use Application\DTO\CategoryDTO;
use paml\EntityOperation\Repository\AbstractEntityRepository;
use paml\EntityView\Controller\AbstractViewController;
use paml\EntityView\Controller\EntityViewControllerInterface;
use paml\Layouts\ColorAdmin\Service\Query\DataTableService;
use Zend\Form\FormInterface;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class CategoryController extends AbstractViewController implements EntityViewControllerInterface
{
    const HEADERS = [
        [
            'name' => 'Name',
            'data' => 'name',
        ],
        [
            'name' => 'Date add',
            'data' => 'dateAdd',
        ],
        [
            'name' => 'Posts',
            'data' => 'postsCount',
        ],
        [
            'name' => 'Active',
            'data' => 'visible',
        ],
        [
            'name' => '',
            'data' => 'id',
            'orderable' => false,
            'width' => '85px',
        ],
    ];

    /** @var DataTableService */
    private $dataTableService;

    public function onDispatch(MvcEvent $e)
    {
        $this->layout()->setTemplate('layout/color-admin');
        return parent::onDispatch($e);
    }

    public function indexAction()
    {
        return new ViewModel([
            'headers' => self::HEADERS,
            'url' => '/admin/category/asyncList',
        ]);
    }

    public function asyncListAction()
    {
        $dataTableParameters = $this->dataTableService->parseFromQuery($this->params()->fromQuery());

        $categories = $this->entityRepository->filter($dataTableParameters);
        $categoriesCount = $this->entityRepository->filterCount($dataTableParameters);

        return new JsonModel([
            'data' => array_map(function (CategoryDTO $categoryDTO) {
                return $categoryDTO->toArray($this->buttons('admin/front', ['controller' => 'category']));
            }, $categories),
            'draw' => $dataTableParameters->getDraw(),
            'recordsTotal' => count($categoriesCount),
            'recordsFiltered' => count($categoriesCount),
        ]);
    }

    public function setDataTableService(DataTableService $dataTableService): void
    {
        $this->dataTableService = $dataTableService;
    }
}
