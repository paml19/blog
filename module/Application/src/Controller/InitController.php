<?php

namespace Application\Controller;

use Zend\Mvc\Console\Controller\AbstractConsoleController;

class InitController extends AbstractConsoleController
{
    public function indexAction()
    {
        exec('php ./public/index.php init roles');
        exec('php ./public/index.php init admin');
        die('OK');
    }
}
