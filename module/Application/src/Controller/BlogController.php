<?php

namespace Application\Controller;

use Application\Entity\Post;
use Application\Form\CommentForm;
use Application\Repository\CategoryRepository;
use Application\Repository\PostRepository;
use Doctrine\Common\Collections\ArrayCollection;
use paml\EntityView\Module;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class BlogController extends AbstractActionController
{
    private $postRepository;

    private $commentForm;

    private $categoryRepository;

    public function __construct(
        PostRepository $postRepository,
        CommentForm $commentForm,
        CategoryRepository $categoryRepository
    ) {
        $this->postRepository = $postRepository;
        $this->commentForm = $commentForm;
        $this->categoryRepository = $categoryRepository;
    }

    public function indexAction()
    {
        $page = (int) $this->params()->fromQuery('page', 1);
        $count = (int) $this->params()->fromQuery('count', 10);
        $sort = (string) $this->params()->fromQuery('sort', 'desc');
        $order = (string) $this->params()->fromQuery('order', 'dateAdd');

        $list = $this->postRepository->getActiveList($page, $count, $sort, $order);

        $posts = new ArrayCollection();
        if ($list->count() >= $page) {
            foreach ($list as $item) {
                $posts->add($item);
            }
        }

        $this->getEventManager()->trigger(Module::EVENT_GET_LIST, $this, ['list' => $posts]);

        return new ViewModel([
            'posts' => $posts
        ]);
    }

    public function categoryAction()
    {
        $categorySlug = $this->params()->fromRoute('slug');
        $page = (int) $this->params()->fromQuery('page', 1);
        $count = (int) $this->params()->fromQuery('count', 10);
        $sort = (string) $this->params()->fromQuery('sort', 'desc');
        $order = (string) $this->params()->fromQuery('order', 'dateAdd');

        $list = $this->postRepository->getCategoryPosts($page, $count, $sort, $order, $categorySlug);

        $posts = new ArrayCollection();
        if ($list->count() >= $page) {
            foreach ($list as $item) {
                $posts->add($item);
            }
        }

        $this->getEventManager()->trigger(Module::EVENT_GET_LIST, $this, ['list' => $posts]);

        return new ViewModel([
            'posts' => $posts
        ]);
    }

    public function postAction()
    {
        $slug = (string) $this->params()->fromRoute('slug');
        $post = $this->postRepository->findOneBy([Post::SLUG => $slug]);

        if (! $post) {
            $post = $this->postRepository->find($slug);

            if (! $post) {
                throw new \Exception('Post not exists');
            }
        }

        $this->getEventManager()->trigger(Module::EVENT_GET, $this, ['object' => $post]);

        return new ViewModel([
            'post' => $post,
            'form' => $this->commentForm
        ]);
    }
}
