<?php

namespace Application\Listener;

use Application\Entity\Comment;
use paml\Notification\Service\NotificationService;
use Zend\EventManager\EventInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\ListenerAggregateTrait;
use Zend\Mvc\Controller\AbstractController;

class CommentListener implements ListenerAggregateInterface
{
    use ListenerAggregateTrait;

    private $notificationService;

    public function __construct(NotificationService $notificationService)
    {
        $this->notificationService = $notificationService;
    }

    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $sharedEventManager = $events->getSharedManager();

        $sharedEventManager->attach(
            AbstractController::class,
            \paml\EntityRest\Module::EVENT_CREATED,
            [
                $this,
                'eventCreated'
            ],
            $priority
        );
    }

    public function eventCreated(EventInterface $event): void
    {
        /** @var Comment $object */
        $object = $event->getParam('object');

        if ($object instanceof Comment) {
            $params = [
                'title' => 'Comment created',
                'id' => $object->getId(),
                'name' => $object->getName(),
                'postId' => $object->getPost()->getId(),
            ];

            $platforms = [
                'mail'
            ];

            $this->notificationService->saveNotification($params, $platforms);
        }
    }
}
