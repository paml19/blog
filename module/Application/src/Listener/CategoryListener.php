<?php

namespace Application\Listener;

use Application\Entity\Category;
use paml\Notification\Service\NotificationService;
use Zend\EventManager\EventInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\ListenerAggregateTrait;
use Application\Service\CategoryService;
use Zend\Mvc\Controller\AbstractController;

class CategoryListener implements ListenerAggregateInterface
{
    use ListenerAggregateTrait;

    private $notificationService;

    public function __construct(NotificationService $notificationService)
    {
        $this->notificationService = $notificationService;
    }

    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $sharedEventManager = $events->getSharedManager();

        $sharedEventManager->attach(
            AbstractController::class,
            \paml\EntityRest\Module::EVENT_CREATED,
            [
                $this,
                'eventCreated'
            ],
            $priority
        );
    }

    public function eventCreated(EventInterface $event)
    {
        /** @var Category $object */
        $object = $event->getParam('object');

        if ($object instanceof Category) {
            $params = [
                'title' => 'Category created',
                'id' => $object->getId(),
                'name' => $object->getName(),
            ];

            $platforms = [
                'mail'
            ];

            $this->notificationService->saveNotification($params, $platforms);
        }
    }
}
