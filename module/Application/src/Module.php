<?php

namespace Application;

use Application\Listener\CategoryListener;
use Application\Listener\CommentListener;
use Application\Listener\RouteListener;
use Application\Listener\TaskListener;
use Zend\Mvc\MvcEvent;
use Zend\ServiceManager\ServiceManager;
use Zend\Session\SessionManager;

class Module
{
    const VERSION = '0.1';

    const APP_NAME = 'BLOG';

    const ROLE_GUEST = 'guest';

    const ROLE_ADMIN = 'admin';

    const ROLE_CATEGORY_MODERATOR = 'category_moderator';

    const ROLE_POST_MODERATOR = 'post_moderator';

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function onBootstrap(MvcEvent $event)
    {
        $serviceManager = $event->getApplication()->getServiceManager();
        $eventManager = $event->getTarget()->getEventManager();

        /** @var CategoryListener $categoryListener */
        $categoryListener = $serviceManager->get(CategoryListener::class);
        $categoryListener->attach($eventManager);

        /** @var CommentListener $commentListener */
        $commentListener = $serviceManager->get(CommentListener::class);
        $commentListener->attach($eventManager);
    }
}
