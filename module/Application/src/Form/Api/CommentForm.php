<?php

namespace Application\Form\Api;

use Application\Entity\Comment;
use Zend\Form\Element\Hidden;
use Zend\Form\Form;
use Zend\Form\FormInterface;

class CommentForm extends Form implements FormInterface
{
    public function init()
    {
        $this->add([
            'name' => Comment::NAME,
            'type' => Hidden::class,
        ]);

        $this->add([
            'name' => Comment::CONTENT,
            'type' => Hidden::class,
        ]);

        $this->add([
            'name' => Comment::POST,
            'type' => Hidden::class,
        ]);
    }
}
