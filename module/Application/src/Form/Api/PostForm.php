<?php

namespace Application\Form\Api;

use Application\Entity\Post;
use Zend\Form\Element\File;
use Zend\Form\Element\Hidden;
use Zend\Form\Form;
use Zend\Form\FormInterface;

class PostForm extends Form implements FormInterface
{
    public function init()
    {
        $this->add([
            'name' => Post::NAME,
            'type' => Hidden::class,
        ]);

        $this->add([
            'name' => Post::CATEGORIES,
            'type' => Hidden::class,
        ]);

        $this->add([
            'name' => Post::CATEGORY,
            'type' => Hidden::class,
        ]);

        $this->add([
            'name' => Post::CONTENT,
            'type' => Hidden::class,
        ]);

        $this->add([
            'name' => Post::VISIBLE,
            'type' => Hidden::class,
        ]);

        $this->add([
            'name' => Post::FILE,
            'type' => File::class,
        ]);
    }
}
