<?php


namespace Application\Form\Api;

use Application\Entity\Category;
use Zend\Form\Element\Hidden;
use Zend\Form\Form;
use Zend\Form\FormInterface;

class CategoryForm extends Form implements FormInterface
{
    public function init()
    {
        $this->add([
            'name' => Category::NAME,
            'type' => Hidden::class,
        ]);

        $this->add([
            'name' => Category::PARENT,
            'type' => Hidden::class,
        ]);

        $this->add([
            'name' => Category::CHILD,
            'type' => Hidden::class,
        ]);

        $this->add([
            'name' => Category::CHILDREN,
            'type' => Hidden::class,
        ]);

        $this->add([
            'name' => Category::POST,
            'type' => Hidden::class,
        ]);

        $this->add([
            'name' => Category::VISIBLE,
            'type' => Hidden::class,
        ]);

        $this->add([
            'name' => Category::POSTS,
            'type' => Hidden::class,
        ]);
    }
}
