<?php

namespace Application\Form;

use Application\Entity\Comment;
use Zend\Form\Element\Hidden;
use Zend\Form\Element\Submit;
use Zend\Form\Element\Text;
use Zend\Form\Element\Textarea;
use Zend\Form\Form;

class CommentForm extends Form
{
    public function __construct($name = null, $options = [])
    {
        parent::__construct($name, $options);
        $this->setAttribute('id', 'js-comment-form');
        $this->setAttribute('action', '/api/comments');
    }

    public function init()
    {
        $this->add([
            'name' => Comment::NAME,
            'type' => Text::class,
            'required' => true,
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => 'Name',
            ],
        ]);

        $this->add([
            'name' => Comment::CONTENT,
            'type' => Textarea::class,
            'required' => true,
            'attributes' => [
                'class' => 'form-control',
            ]
        ]);

        $this->add([
            'name' => Comment::POST,
            'type' => Hidden::class,
            'attributes' => [
                'id' => 'js-post-id',
            ],
        ]);

        $this->add([
            'name' => 'submit',
            'type' => Submit::class,
            'attributes' => [
                'class' => 'btn btn-block btn-primary',
                'value' => 'Send comment',
            ],
        ]);
    }
}
