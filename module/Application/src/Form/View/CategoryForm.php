<?php

namespace Application\Form\View;

use Application\Entity\Category;
use Doctrine\ORM\EntityManager;
use DoctrineModule\Form\Element\ObjectSelect;
use Zend\Form\Element\Checkbox;
use Zend\Form\Element\Csrf;
use Zend\Form\Element\Submit;
use Zend\Form\Element\Text;
use Zend\Form\Form;
use Zend\Form\FormInterface;

class CategoryForm extends Form implements FormInterface
{
    private $entityManager;

    public function __construct($name = null, $options = [], EntityManager $entityManager)
    {
        parent::__construct($name, $options);
        $this->entityManager = $entityManager;
    }

    public function init()
    {
        $this->add([
            'name' => 'csrf',
            'type' => Csrf::class,
        ]);

        $this->add([
            'name' => Category::NAME,
            'type' => Text::class,
            'options' => [
                'label' => 'Name',
                'label_attributes' => [
                    'class' => 'col-sm-2 col-form-label control-label'
                ]
            ],
            'attributes' => [
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => Category::PARENT,
            'type' => ObjectSelect::class,
            'options' => [
                'label' => 'Parent',
                'object_manager' => $this->entityManager,
                'target_class' => Category::class,
                'label_generator' => function ($category) {
                    return $category->getName();
                },
                'property' => 'id',
                'is_method' => true,
                'find_method' => [
                    'name' => 'findBy',
                    'params' => [
                        'criteria' => [
                            'dateDelete' => null,
                        ],
                    ],
                ],
                'display_empty_item' => true,
                'empty_item_label' => 'Select category',
                'label_attributes' => [
                    'class' => 'col-sm-2 col-form-label control-label',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
                'id' => 'js-' . Category::PARENT,
            ],
        ]);

        $this->add([
            'name' => Category::VISIBLE,
            'type' => Checkbox::class,
            'options' => [
                'label' => 'Visible',
                'label_attributes' => [
                    'class' => 'col-sm-2 col-form-label control-label',
                ],
                'use_hidden_value' => true,
                'checked_value' => 'yes',
                'unchecked_value' => 'no',
            ],
            'attributes' => [
                'class' => 'form-control checkbox',
            ],
        ]);

        $this->add([
            'name' => 'submit',
            'type' => Submit::class,
            'attributes' => [
                'value' => 'Save',
                'class' => 'btn btn-block btn-primary',
            ],
        ]);
    }
}
