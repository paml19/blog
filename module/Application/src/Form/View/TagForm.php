<?php

namespace Application\Form\View;

use Application\Entity\Tag;
use Zend\Form\Element\Checkbox;
use Zend\Form\Element\Csrf;
use Zend\Form\Element\Submit;
use Zend\Form\Element\Text;
use Zend\Form\Form;
use Zend\Form\FormInterface;

class TagForm extends Form implements FormInterface
{
    public function init()
    {
        $this->add([
            'name' => 'csrf',
            'type' => Csrf::class,
        ]);

        $this->add([
            'name' => Tag::NAME,
            'type' => Text::class,
            'options' => [
                'label' => 'Name',
                'label_attributes' => [
                    'class' => 'col-sm-2 col-form-label control-label'
                ]
            ],
            'attributes' => [
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => Tag::VISIBLE,
            'type' => Checkbox::class,
            'options' => [
                'label' => 'Visible',
                'label_attributes' => [
                    'class' => 'col-sm-2 col-form-label control-label',
                ],
                'use_hidden_value' => true,
                'checked_value' => 'yes',
                'unchecked_value' => 'no',
            ],
            'attributes' => [
                'class' => 'form-control checkbox',
            ],
        ]);

        $this->add([
            'name' => 'submit',
            'type' => Submit::class,
            'attributes' => [
                'value' => 'Save',
                'class' => 'btn btn-block btn-primary',
            ],
        ]);
    }
}
