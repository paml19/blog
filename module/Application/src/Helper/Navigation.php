<?php

namespace Application\Helper;

use Application\Entity\Category;
use Application\Repository\CategoryRepository;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Navigation\Service\DefaultNavigationFactory;

class Navigation extends DefaultNavigationFactory
{
    protected function getPages(ContainerInterface $container)
    {
        if ($this->pages === null) {
            $configuration = $container->get('Config');
            /** @var CategoryRepository $categoryRepository */
            $categoryRepository = $container->get(EntityManager::class)->getRepository(Category::class);
            $categories = $categoryRepository->findBy([Category::VISIBLE => true]);

            /** @var Category $category */
            foreach ($categories as $category) {
                $configuration['navigation'][$this->getName()][$category->getSlug()] = [
                    'label' => $category->getName(),
                    'route' => 'home/category',
                    'action' => 'category',
                    'params' => [
                        'slug' => $category->getSlug() != '' ? $category->getSlug() : $category->getId(),
                    ],
                ];
            }

            if (! isset($configuration['navigation'][$this->getName()])) {
                throw new \InvalidArgumentException(sprintf(
                    'Failed to find a navigation container by the name "%s"',
                    $this->getName()
                ));
            }

            $application = $container->get('Application');
            $routeMatch = $application->getMvcEvent()->getRouteMatch();
            $router = $application->getMvcEvent()->getRouter();
            $pages = $this->getPagesFromConfig($configuration['navigation'][$this->getName()]);

            $this->pages = $this->injectComponents($pages, $routeMatch, $router);
        }

        return $this->pages;
    }
}
