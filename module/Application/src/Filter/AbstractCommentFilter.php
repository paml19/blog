<?php

namespace Application\Filter;

use Application\Entity\Comment;
use Application\Entity\Post;
use Application\Repository\PostRepository;
use DoctrineModule\Validator\ObjectExists;
use Zend\InputFilter\InputFilter;
use Zend\Validator\EmailAddress;
use Zend\Validator\StringLength;

abstract class AbstractCommentFilter extends InputFilter
{
    private $postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function init()
    {
        $this->add([
            'name' => Comment::NAME,
            'required' => true,
            'validators' => [
                ['name' => EmailAddress::class],
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 2,
                    ],
                ],
            ],
        ]);

        $this->add([
            'name' => Comment::CONTENT,
            'required' => true,
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 6,
                        'max' => 255,
                    ],
                ],
            ],
        ]);

        $this->add([
            'name' => Comment::POST,
            'required' => true,
            'validators' => [
                [
                    'name' => ObjectExists::class,
                    'options' => [
                        'object_repository' => $this->postRepository,
                        'target_class' => Post::class,
                        'fields' => 'id',
                    ],
                ],
            ]
        ]);
    }
}
