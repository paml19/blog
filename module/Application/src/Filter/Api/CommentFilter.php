<?php

namespace Application\Filter\Api;

use Application\Filter\AbstractCommentFilter;
use paml\EntityOperation\Filter\FilterInterface;

class CommentFilter extends AbstractCommentFilter implements FilterInterface
{
}
