<?php

namespace Application\Filter\View;

use Application\Entity\Category;
use paml\EntityOperation\Filter\FilterInterface;
use Zend\InputFilter\InputFilter;

class CategoryFilter extends InputFilter implements FilterInterface
{
    public function init()
    {
        $this->add([
            'name' => Category::PARENT,
            'required' => false,
        ]);
    }
}
