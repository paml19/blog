<?php

declare(strict_types=1);

namespace Application\Filter;

use Application\Entity\Category;
use Application\Entity\Post;
use Application\Entity\Tag;
use Application\Repository\CategoryRepository;
use Application\Repository\TagRepository;
use DoctrineModule\Validator\ObjectExists;
use paml\EntityOperation\Filter\FilterInterface;
use paml\EntityOperation\Validator\MultipleObjectExists;
use paml\File\Validator\InjectionValidator;
use Zend\InputFilter\InputFilter;
use Zend\Validator\File\MimeType;
use Zend\Validator\StringLength;

abstract class AbstractPostFilter extends InputFilter implements FilterInterface
{
    private $categoryRepository;

    private $tagRepository;

    public function __construct(CategoryRepository $categoryRepository, TagRepository $tagRepository)
    {
        $this->categoryRepository = $categoryRepository;
        $this->tagRepository = $tagRepository;
    }

    public function init()
    {
        $this->add([
            'name' => Post::NAME,
            'required' => true,
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 5,
                    ],
                ],
            ],
        ]);

        $this->add([
            'name' => Post::CONTENT,
            'required' => true,
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 6,
                        'max' => 4000,
                    ],
                ],
            ],
        ]);

        $this->add([
            'name' => Post::TAGS,
            'required' => false,
            'validators' => [
                [
                    'name' => MultipleObjectExists::class,
                    'options' => [
                        'object_repository' => $this->tagRepository,
                        'target_class' => Tag::class,
                        'fields' => 'id',
                    ],
                ],
            ],
        ]);

        $this->add([
            'name' => Post::FILE,
            'required' => false,
            'validators' => [
                [
                    'name' => InjectionValidator::class,
                ],
                [
                    'name' => MimeType::class,
                    'options' => [
                        'mimeType' => ['image/png', 'image/jpeg'],
                    ],
                ]
            ],
        ]);
    }
}
