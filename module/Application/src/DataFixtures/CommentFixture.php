<?php

namespace Application\DataFixtures;

use Application\Entity\Comment;
use Application\Entity\Post;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class CommentFixture extends BaseFixture implements FixtureInterface, DependentFixtureInterface
{
    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(Comment::class, 1000, function (Comment $comment) use ($manager) {
            $comment->setName($this->faker->email)
                ->setCont($this->faker->text(255))
                ->setPost($this->getRandomReference(Post::class))
                ->setDateAdd($this->faker->dateTimeBetween('-1 months', '-1 days'));

            $manager->persist($comment);
        });

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            PostFixture::class
        ];
    }
}