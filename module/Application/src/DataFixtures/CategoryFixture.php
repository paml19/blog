<?php

namespace Application\DataFixtures;

use Application\Entity\Category;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class CategoryFixture extends BaseFixture implements FixtureInterface
{
    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(Category::class, 6, function (Category $category) use ($manager) {
            $category->setName($this->faker->text(255))
                ->setVisible($this->faker->boolean)
                ->setDateAdd($this->faker->dateTimeBetween('-1 months', '-1 days'));

            $parent = $this->getRandomReference(Category::class);

            $category->setParent($parent);

            $manager->persist($category);
        });

        $manager->flush();
    }
}