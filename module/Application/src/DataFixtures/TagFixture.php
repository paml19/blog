<?php

namespace Application\DataFixtures;

use Application\Entity\Tag;
use Doctrine\Common\Persistence\ObjectManager;

class TagFixture extends BaseFixture
{
    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(Tag::class, 1000, function (Tag $tag) use ($manager) {
            $tag->setName($this->faker->name)
                ->setVisible($this->faker->boolean);

            $manager->persist($tag);
        });

        $manager->flush();
    }
}