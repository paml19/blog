<?php

namespace Application\DataFixtures;

use Application\Entity\Category;
use Application\Entity\Post;
use Application\Entity\Tag;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use paml\File\Entity\File;
use paml\File\Library\Entity\Catalog;

class PostFixture extends BaseFixture implements DependentFixtureInterface
{
    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(Post::class, 100, function (Post $post) use ($manager) {
            $post->setName($this->faker->text)
                ->setCont($this->faker->realText(2000))
                ->setVisible($this->faker->boolean)
                ->setDateAdd($this->faker->dateTimeBetween('-1 months', '-1 days'))
                ->setCategory($this->getRandomReference(Category::class));

            $tags = $this->getRandomReferences(Tag::class, $this->faker->numberBetween(0, 15));

            foreach ($tags as $tag) {
                $post->addTag($tag);
            }

            $manager->persist($post);

            $image = $this->faker->image();
            $fileName = explode('/', $image)[2];

            $file = (new File)
                ->setSize(filesize($image))
                ->setName($fileName)
                ->setType(mime_content_type($image));

            $manager->persist($file);

            $catalog = (new Catalog)
                ->setName($this->faker->name)
                ->setSlug($this->faker->slug)
                ->setSize($file->getSize())
                ->addFile($file);

            $manager->persist($catalog);

            $catalogTargetDir = './data' . $catalog->getPath();

            if (! file_exists($catalogTargetDir)) {
                mkdir($catalogTargetDir, 0777, true);
            }

            $targetDir = './data' . $catalog->getPath() . $file->getNoUploadPathPath();
            $targetPath = $targetDir . $fileName;

            if (! file_exists($targetDir)) {
                mkdir($targetDir, 0777, true);
            }

            rename($image, $targetPath);

            $post->setFile($file);
        });

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            CategoryFixture::class,
            TagFixture::class,
        ];
    }
}