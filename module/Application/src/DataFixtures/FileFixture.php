<?php

namespace Application\DataFixtures;

use Doctrine\Common\Persistence\ObjectManager;
use paml\File\Entity\File;
use paml\File\Library\Entity\Catalog;

class FileFixture extends BaseFixture
{
    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(File::class, 5, function (File $file) use ($manager) {
            $image = $this->faker->image();
            $fileName = explode('/', $image)[2];

            $file->setSize(filesize($image))
                ->setName($fileName)
                ->setType(mime_content_type($image));

            $manager->persist($file);

            $catalog = (new Catalog)
                ->setName($this->faker->name)
                ->setSlug($this->faker->slug)
                ->setSize($file->getSize())
                ->addFile($file);

            $manager->persist($catalog);

            $catalogTargetDir = './data' . $catalog->getPath();

            if (! file_exists($catalogTargetDir)) {
                mkdir($catalogTargetDir, 0777, true);
            }

            $targetDir = './data' . $catalog->getPath() . $file->getNoUploadPathPath();
            $targetPath = $targetDir . $fileName;

            if (! file_exists($targetDir)) {
                mkdir($targetDir, 0777, true);
            }

            rename($image, $targetPath);
        });

        $manager->flush();
    }
}