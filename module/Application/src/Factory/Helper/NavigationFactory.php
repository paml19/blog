<?php

namespace Application\Factory\Helper;

use Application\Helper\Navigation;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class NavigationFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return (new Navigation())->createService($container);
    }
}
