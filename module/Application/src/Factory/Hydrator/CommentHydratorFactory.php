<?php

namespace Application\Factory\Hydrator;

use Application\Entity\Post;
use Application\Hydrator\Api\CommentHydrator;
use Application\Hydrator\Api\PostListHydrator;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class CommentHydratorFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): CommentHydrator
    {
        return new CommentHydrator(
            $container->get('HydratorManager')->get(PostListHydrator::class),
            $container->get(EntityManager::class)->getRepository(Post::class)
        );
    }
}
