<?php

namespace Application\Factory\Hydrator;

use Application\Entity\Category;
use Application\Entity\Post;
use Application\Entity\Tag;
use Application\Hydrator\View\CategoryHydrator;
use Application\Hydrator\View\PostHydrator;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use paml\File\Library\Hydrator\FileHydrator;
use Zend\ServiceManager\Factory\FactoryInterface;

class ViewPostHydratorFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): PostHydrator
    {
        return new PostHydrator(
            $container->get(EntityManager::class)->getRepository(Category::class),
            $container->get('HydratorManager')->get(FileHydrator::class),
            $container->get(EntityManager::class)->getRepository(Post::class),
            $container->get(EntityManager::class)->getRepository(Tag::class),
            $container->get('HydratorManager')->get(CategoryHydrator::class)
        );
    }
}
