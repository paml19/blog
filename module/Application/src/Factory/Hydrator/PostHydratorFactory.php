<?php

namespace Application\Factory\Hydrator;

use Application\Entity\Category;
use Application\Entity\Post;
use Application\Hydrator\Api\CategoryListHydrator;
use Application\Hydrator\Api\CommentListHydrator;
use Application\Hydrator\Api\PostHydrator;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use paml\File\Library\Hydrator\FileHydrator;
use Zend\ServiceManager\Factory\FactoryInterface;

class PostHydratorFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): PostHydrator
    {
        return new PostHydrator(
            $container->get('HydratorManager')->get(CategoryListHydrator::class),
            $container->get(EntityManager::class)->getRepository(Category::class),
            $container->get('HydratorManager')->get(CommentListHydrator::class),
            $container->get(EntityManager::class)->getRepository(Post::class),
            $container->get('HydratorManager')->get(FileHydrator::class)
        );
    }
}
