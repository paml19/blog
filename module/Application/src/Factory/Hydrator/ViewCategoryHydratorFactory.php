<?php

namespace Application\Factory\Hydrator;

use Application\Entity\Category;
use Application\Hydrator\View\CategoryHydrator;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class ViewCategoryHydratorFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): CategoryHydrator
    {
        return new CategoryHydrator(
            $container->get(EntityManager::class)->getRepository(Category::class)
        );
    }
}
