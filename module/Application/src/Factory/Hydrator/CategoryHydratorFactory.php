<?php

namespace Application\Factory\Hydrator;

use Application\Entity\Category;
use Application\Entity\Post;
use Application\Hydrator\Api\CategoryHydrator;
use Application\Hydrator\Api\CategoryListHydrator;
use Application\Hydrator\Api\PostListHydrator;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class CategoryHydratorFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): CategoryHydrator
    {
        return new CategoryHydrator(
            $container->get('HydratorManager')->get(PostListHydrator::class),
            $container->get('HydratorManager')->get(CategoryListHydrator::class),
            $container->get(EntityManager::class)->getRepository(Category::class),
            $container->get(EntityManager::class)->getRepository(Post::class)
        );
    }
}
