<?php


namespace Application\Factory\Listener;

use Application\Listener\CategoryListener;
use Application\Service\CategoryService;
use Interop\Container\ContainerInterface;
use paml\Notification\Service\NotificationService;
use Zend\ServiceManager\Factory\FactoryInterface;

class CategoryListenerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): CategoryListener
    {
        return new CategoryListener(
            $container->get(NotificationService::class)
        );
    }
}
