<?php

namespace Application\Factory\Listener;

use Application\Listener\CommentListener;
use Interop\Container\ContainerInterface;
use paml\Notification\Service\NotificationService;
use Zend\ServiceManager\Factory\FactoryInterface;

class CommentListenerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): CommentListener
    {
        return new CommentListener(
            $container->get(NotificationService::class)
        );
    }
}
