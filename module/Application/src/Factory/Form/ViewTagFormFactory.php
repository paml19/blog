<?php

namespace Application\Factory\Form;

use Application\Entity\Tag;
use Application\Filter\View\TagFilter;
use Application\Form\View\TagForm;
use Application\Hydrator\View\TagHydrator;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class ViewTagFormFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): TagForm
    {
        $form = new TagForm('tag-form');

        $form->setInputFilter($container->get('InputFilterManager')->get(TagFilter::class));
        $form->setHydrator($container->get('HydratorManager')->get(TagHydrator::class));
        $form->setObject(new Tag);

        return $form;
    }
}