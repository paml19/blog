<?php

namespace Application\Factory\Form;

use Application\Entity\Category;
use Application\Filter\View\CategoryFilter;
use Application\Form\View\CategoryForm;
use Application\Hydrator\View\CategoryHydrator;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class ViewCategoryFormFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): CategoryForm
    {
        $form = new CategoryForm('category-form', [], $container->get(EntityManager::class));

        $form->setInputFilter($container->get('InputFilterManager')->get(CategoryFilter::class));
        $form->setHydrator($container->get('HydratorManager')->get(CategoryHydrator::class));
        $form->setObject(new Category);

        return $form;
    }
}
