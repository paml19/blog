<?php

namespace Application\Factory\Form;

use Application\Entity\Comment;
use Application\Filter\CommentFilter;
use Application\Form\CommentForm;
use Application\Hydrator\Api\CommentHydrator;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class CommentFormFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): CommentForm
    {
        $form = new CommentForm('comment-form');

        $form->setInputFilter($container->get('InputFilterManager')->get(CommentFilter::class));
        $form->setHydrator($container->get('HydratorManager')->get(CommentHydrator::class));
        $form->setObject(new Comment);

        return $form;
    }
}
