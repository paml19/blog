<?php

namespace Application\Factory\Form;

use Application\Entity\Post;
use Application\Filter\View\PostFilter;
use Application\Form\View\PostForm;
use Application\Hydrator\View\PostHydrator;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\I18n\Translator\Translator;
use Zend\ServiceManager\Factory\FactoryInterface;

class VewPostFormFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): PostForm
    {
        $form = new PostForm(
            'post-form',
            [],
            $container->get(EntityManager::class)
        );

        $form->setHydrator($container->get('HydratorManager')->get(PostHydrator::class));
        $form->setInputFilter($container->get('InputFilterManager')->get(PostFilter::class));
        $form->setObject(new Post);

        return $form;
    }
}
