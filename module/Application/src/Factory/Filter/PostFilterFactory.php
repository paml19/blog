<?php

namespace Application\Factory\Filter;

use Application\Entity\Category;
use Application\Entity\Tag;
use Application\Filter\Api\PostFilter;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class PostFilterFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): PostFilter
    {
        return new PostFilter(
            $container->get(EntityManager::class)->getRepository(Category::class),
            $container->get(EntityManager::class)->getRepository(Tag::class)
        );
    }
}
