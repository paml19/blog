<?php

namespace Application\Factory;

use Doctrine\Common\Cache\RedisCache;
use Interop\Container\ContainerInterface;
use Redis;
use Zend\ServiceManager\Factory\FactoryInterface;

class CacheFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): RedisCache
    {
        $cache = new RedisCache;
        $redis = new Redis;
        $redis->connect('10.1.0.4');
        $cache->setRedis($redis);

        return $cache;
    }
}