<?php

namespace Application\Factory\Controller\Admin;

use Application\Controller\Admin\CategoryController;
use Interop\Container\ContainerInterface;
use paml\EntityView\Factory\AbstractViewControllerFactory;
use paml\Layouts\ColorAdmin\Service\Query\DataTableService;

class CategoryControllerFactory extends AbstractViewControllerFactory
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): CategoryController
    {
        /** @var CategoryController $categoryController */
        $categoryController = parent::__invoke($container, $requestedName, $options);

        $categoryController->setDataTableService($container->get(DataTableService::class));

        return $categoryController;
    }

}