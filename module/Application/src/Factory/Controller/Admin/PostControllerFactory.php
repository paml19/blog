<?php

namespace Application\Factory\Controller\Admin;

use Application\Controller\Admin\PostController;
use Interop\Container\ContainerInterface;
use paml\EntityView\Factory\AbstractViewControllerFactory;
use paml\Layouts\ColorAdmin\Service\Query\DataTableService;

class PostControllerFactory extends AbstractViewControllerFactory
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): PostController
    {
        /** @var PostController $postController */
        $postController = parent::__invoke($container, $requestedName, $options);

        $postController->setDataTableService($container->get(DataTableService::class));

        return $postController;
    }

}