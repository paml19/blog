<?php

namespace Application\Factory\Controller\Admin;

use Application\Controller\Admin\TagController;
use Interop\Container\ContainerInterface;
use paml\EntityView\Factory\AbstractViewControllerFactory;

class TagControllerFactory extends AbstractViewControllerFactory
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): TagController
    {
        /** @var TagController $tagController */
        $tagController = parent::__invoke($container, $requestedName, $options);

        $tagController->setCache($container->get('RedisCache'));

        return $tagController;
    }

}