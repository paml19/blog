<?php

namespace Application\Factory\Controller\Admin;

use Application\Controller\Admin\CommentController;
use Application\Entity\Comment;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use paml\Layouts\ColorAdmin\Service\Query\DataTableService;
use Zend\ServiceManager\Factory\FactoryInterface;

class CommentControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): CommentController
    {
        return new CommentController(
            $container->get(DataTableService::class),
            $container->get(EntityManager::class)->getRepository(Comment::class)
        );
    }
}