<?php

declare(strict_types=1);

namespace Application\Factory\Controller;

use Application\Controller\BlogController;
use Application\Entity\Category;
use Application\Entity\Post;
use Application\Form\CommentForm;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class BlogControllerFactory
 * @package Application\Factory\Controller
 */
class BlogControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return BlogController
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): BlogController
    {
        return new BlogController(
            $container->get(EntityManager::class)->getRepository(Post::class),
            $container->get('FormElementManager')->get(CommentForm::class),
            $container->get(EntityManager::class)->getRepository(Category::class)
        );
    }
}
