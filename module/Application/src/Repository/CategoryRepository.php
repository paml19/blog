<?php

namespace Application\Repository;

use Application\DTO\CategoryDTO;
use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\Query\Expression\ExpressionBuilder;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;
use paml\EntityOperation\Repository\AbstractEntityRepository;
use paml\Layouts\ColorAdmin\Repository\Traits\ServerSideRepositoryTrait;
use paml\Layouts\ColorAdmin\Service\Query\Parameters;

class CategoryRepository extends AbstractEntityRepository
{
    private $columns = [
        0 => 'c.name',
        1 => 'c.dateAdd',
        2 => 'c.visible',
        3 => 'p.id',
        4 => 'c.id',
    ];

    private $searchParams = [
        'c.name',
        'p.name'
    ];

    private $groupParams = [
        'c.name',
        'c.dateAdd',
        'c.visible',
        'c.id',
    ];

    private $selectParams = [
        'c.id',
        'c.name',
        'c.visible',
        'c.dateAdd',
        'COUNT(p.id)'
    ];

    use ServerSideRepositoryTrait;

    public function find($id, $lockMode = null, $lockVersion = null)
    {
        $queryBuilder = $this->createQueryBuilder('c');

        return $queryBuilder->join('c.posts', 'p')
            ->leftJoin('c.parent', 'par')
            ->addSelect('p, par')
            ->where($queryBuilder->expr()->eq('c.id', '\'' . $id . '\''))
            ->getQuery()
            ->setCacheable(true)
            ->useQueryCache(true)
            ->enableResultCache(3600)
            ->getOneOrNullResult();
    }

    public function filter(Parameters $parameters): array
    {
        return $this->prepareFilter($parameters)
            ->setFirstResult($parameters->getOffset())
            ->setMaxResults($parameters->getLimit())
            ->orderBy(
                $this->columns[$parameters->getOrder()['column']],
                $parameters->getOrder()['dir']
            )
            ->getQuery()
            ->setCacheable(true)
            ->useQueryCache(true)
            ->enableResultCache(3600)
            ->getResult();
    }

    public function filterCount(Parameters $parameters): array
    {
        return $this->prepareFilter($parameters)
            ->getQuery()
            ->setCacheable(true)
            ->useQueryCache(true)
            ->enableResultCache(3600)
            ->getResult();
    }

    private function prepareFilter(Parameters $parameters): QueryBuilder
    {
        $selectParams = '(' . implode(',', $this->selectParams) .')';
        $dtoSelect = 'new ' . CategoryDTO::class . $selectParams;

        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $queryBuilder->select($dtoSelect)
            ->from($this->getEntityName(), 'c')
            ->join('c.posts', 'p')
            ->groupBy(implode(',', $this->groupParams));

        return $this->prepareSearchAndFilter(
            $queryBuilder,
            $parameters->getForm(),
            $this->searchParams
        );
    }
}
