<?php

namespace Application\Repository;

use Application\DTO\CommentDTO;
use Doctrine\ORM\QueryBuilder;
use paml\EntityOperation\Repository\AbstractEntityRepository;
use paml\Layouts\ColorAdmin\Repository\Traits\ServerSideRepositoryTrait;
use paml\Layouts\ColorAdmin\Service\Query\Parameters;

class CommentRepository extends AbstractEntityRepository
{
    private $columns = [
        0 => 'c.name',
        1 => 'c.cont',
        2 => 'c.dateAdd',
    ];

    private $searchParams = [
        'c.name'
    ];

    private $groupParams = [
        'c.name',
        'c.dateAdd',
        'c.cont',
    ];

    private $selectParams = [
        'c.name',
        'c.dateAdd',
        'c.cont',
    ];

    use ServerSideRepositoryTrait;

    public function filter(Parameters $parameters, string $postId): array
    {
        return $this->prepareFilter($parameters, $postId)
            ->setFirstResult($parameters->getOffset())
            ->setMaxResults($parameters->getLimit())
            ->orderBy(
                $this->columns[$parameters->getOrder()['column']],
                $parameters->getOrder()['dir']
            )
            ->getQuery()
            ->setCacheable(true)
            ->useQueryCache(true)
            ->enableResultCache(3600)
            ->getResult();
    }

    public function filterCount(Parameters $parameters, string $postId): array
    {
        return $this->prepareFilter($parameters, $postId)
            ->getQuery()
            ->setCacheable(true)
            ->useQueryCache(true)
            ->enableResultCache(3600)
            ->getResult();
    }

    private function prepareFilter(Parameters $parameters, string $postId): QueryBuilder
    {
        $selectParams = '(' . implode(',', $this->selectParams) .')';
        $dtoSelect = 'new ' . CommentDTO::class . $selectParams;

        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $queryBuilder->select($dtoSelect)
            ->from($this->getEntityName(), 'c')
            ->join('c.post', 'p')
            ->groupBy(implode(',', $this->groupParams))
            ->andWhere($queryBuilder->expr()->eq('p.id', ':post'))
            ->setParameter(':post', $postId);

        return $this->prepareSearchAndFilter(
            $queryBuilder,
            $parameters->getForm(),
            $this->searchParams
        );
    }
}
