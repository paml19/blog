<?php

namespace Application\Repository;

use Application\DTO\PostDTO;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Cache;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Paginator\Adapter\Collection as CollectionAdapter;
use paml\EntityOperation\Repository\AbstractEntityRepository;
use paml\Layouts\ColorAdmin\Repository\Traits\ServerSideRepositoryTrait;
use paml\Layouts\ColorAdmin\Service\Query\Parameters;
use Zend\Paginator\Paginator;

class PostRepository extends AbstractEntityRepository
{
    private $columns = [
        0 => 'p.name',
        1 => 'p.dateAdd',
        2 => 'p.visible',
        3 => 'COUNT(c.id)',
        4 => 'p.id',
    ];

    private $searchParams = [
        'p.name',
        'c.name'
    ];

    private $groupParams = [
        'p.name',
        'p.dateAdd',
        'p.visible',
        'p.id'
    ];

    private $selectParams = [
        'p.id',
        'p.name',
        'p.visible',
        'p.dateAdd',
        'COUNT(c.id)'
    ];

    use ServerSideRepositoryTrait;

    public function find($id, $lockMode = null, $lockVersion = null)
    {
        $queryBuilder = $this->createQueryBuilder('p');

        return $queryBuilder->leftJoin('p.category', 'cat')
            ->leftJoin('p.comments', 'com')
            ->leftJoin('p.file', 'f')
            ->addSelect('cat, com, f')
            ->andWhere($queryBuilder->expr()->eq('p.id', '\'' . $id . '\''))
            ->getQuery()
            ->setCacheable(true)
            ->enableResultCache(3600)
            ->useQueryCache(true)
            ->getOneOrNullResult();
    }

    public function getActiveList(int $page, int $limit, string $sort, string $order): Paginator
    {
        $collection = new ArrayCollection($this->findBy(['dateDelete' => null, 'visible' => true], [$order => $sort]));
        $adapter = new CollectionAdapter($collection);

        $paginator = (new Paginator($adapter))
            ->setCurrentPageNumber($page)
            ->setItemCountPerPage($limit);

        return $paginator;
    }

    public function getCategoryPosts(int $page, int $limit, string $sort, string $order, string $categoryId): Paginator
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder->select('p')
            ->from($this->getEntityName(), 'p')
            ->addSelect('c')
            ->join('p.categories', 'c')
            ->orderBy('p.' . $order, $sort)
            ->andWhere($queryBuilder->expr()->eq('c.id', ':categoryId'))
            ->orWhere($queryBuilder->expr()->eq('c.slug', ':categoryId'))
            ->andWhere($queryBuilder->expr()->eq('p.visible', true))
            ->setParameter(':categoryId', $categoryId)
            ->getQuery()
            ->setCacheable(true)
            ->setCacheMode(Cache::MODE_NORMAL)
            ->enableResultCache(3600);

        $result = new ArrayCollection($queryBuilder->getResult());

        $adapter = new CollectionAdapter($result);

        $paginator = (new Paginator($adapter))
            ->setCurrentPageNumber($page)
            ->setItemCountPerPage($limit);

        return $paginator;
    }

    public function filter(Parameters $parameters): array
    {
        return $this->prepareFilter($parameters)
            ->setFirstResult($parameters->getOffset())
            ->setMaxResults($parameters->getLimit())
            ->orderBy(
                $this->columns[$parameters->getOrder()['column']],
                $parameters->getOrder()['dir']
            )
            ->getQuery()
            ->setCacheable(true)
            ->useQueryCache(true)
            ->enableResultCache(3600)
            ->getResult();
    }

    public function filterCount(Parameters $parameters): array
    {
        return $this->prepareFilter($parameters)
            ->getQuery()
            ->setCacheable(true)
            ->useQueryCache(true)
            ->enableResultCache(3600)
            ->getResult();
    }

    private function prepareFilter(Parameters $parameters): QueryBuilder
    {
        $selectParams = '(' . implode(',', $this->selectParams) .')';
        $dtoSelect = 'new ' . PostDTO::class . $selectParams;

        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $queryBuilder->select($dtoSelect)
            ->from($this->getEntityName(), 'p')
            ->join('p.comments', 'c')
            ->groupBy(implode(',', $this->groupParams));

        return $this->prepareSearchAndFilter(
            $queryBuilder,
            $parameters->getForm(),
            $this->searchParams
        );
    }
}
