<?php

namespace ApplicationTest\Entity;

use Application\Entity\Tag;
use Application\Entity\Post;
use ApplicationTest\BadDataTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Faker\Factory;
use PHPUnit\Framework\TestCase;

/**
 * Class TagTest
 * @package ApplicationTest\Entity
 *
 * @covers \Application\Entity\Tag
 */
class TagTest extends TestCase
{
    use BadDataTrait;

    /**
     * @var Tag
     */
    private $tag;

    /**
     *
     */
    protected function setUp(): void
    {
        $this->tag = new Tag;
    }

    /**
     * @param $id
     * @param $email
     * @param $text
     * @param $posts
     * @param $visible
     * @param $post
     */
    private function populateData($id, $email, $text, $posts, $visible, $post): void
    {
        $this->tag
            ->setId($id)
            ->setName($email)
            ->setSlug($text)
            ->setPosts($posts)
            ->setVisible($visible)
            ->addPost($post)
            ->removePost($post);
    }

    /**
     *
     */
    public function testHasAttributes(): void
    {
        $this->assertClassHasAttribute('id', Tag::class);
        $this->assertClassHasAttribute('name', Tag::class);
        $this->assertClassHasAttribute('slug', Tag::class);
        $this->assertClassHasAttribute('dateAdd', Tag::class);
        $this->assertClassHasAttribute('dateEdit', Tag::class);
        $this->assertClassHasAttribute('dateDelete', Tag::class);
        $this->assertClassHasAttribute('posts', Tag::class);
        $this->assertClassHasAttribute('visible', Tag::class);
    }

    /**
     *
     */
    public function testInitialCategoryValuesAreNull(): void
    {
        $this->assertNull($this->tag->getId());
        $this->assertNull($this->tag->getName());
        $this->assertNull($this->tag->getSlug());
        $this->assertNull($this->tag->getDateAdd());
        $this->assertNull($this->tag->getDateEdit());
        $this->assertNull($this->tag->getDateDelete());
        $this->assertFalse($this->tag->isVisible());
        $this->assertInstanceOf(Collection::class, $this->tag->getPosts());
    }

    /**
     * @dataProvider providerReturnGoodValues
     * @param $id
     * @param $email
     * @param $slug
     * @param $posts
     * @param $visible
     * @param $post
     */
    public function testReturnedValues($id, $email, $slug, $posts, $visible, $post): void
    {
        $this->populateData($id, $email, $slug, $posts, $visible, $post);

        $this->assertIsString($this->tag->getId());
        $this->assertIsString($this->tag->getName());
        $this->assertIsString($this->tag->getSlug());
        $this->assertIsBool($this->tag->isVisible());
        $this->assertInstanceOf(Collection::class, $this->tag->getPosts());
    }

    /**
     * @dataProvider providerReturnBadData
     *
     * @param $id
     * @param $name
     * @param $slug
     * @param $posts
     * @param $visible
     * @param $post
     * @param $exception
     */
    public function testExceptions($id, $name, $slug, $posts, $visible, $post, $exception): void
    {
        $this->expectException($exception);

        $this->populateData($id, $name, $slug, $posts, $visible, $post);
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function providerReturnGoodValues()
    {
        return [
            'good values visible true' => [
                'string-value',
                'string-value',
                'string-value',
                new ArrayCollection,
                true,
                new Post
            ],
            'good values visible false' => [
                'string-value',
                'string-value',
                'string-value',
                new ArrayCollection,
                false,
                new Post
            ]
        ];
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function providerReturnBadData()
    {
        return $this->getBadDataWithTypeErrorException(100, 6);
    }
}
