<?php

declare(strict_types=1);

namespace ApplicationTest\Entity;

use Application\Entity\Category;
use Application\Entity\Comment;
use Application\Entity\Post;
use Application\Entity\Tag;
use ApplicationTest\BadDataTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Faker\Factory;
use paml\File\Entity\File;
use PHPUnit\Framework\TestCase;

/**
 * Class PostTest
 * @package ApplicationTest\Entity
 *
 * @covers \Application\Entity\Post
 */
class PostTest extends TestCase
{
    use BadDataTrait;

    private $post;

    public function setUp(): void
    {
        $this->post = new Post;
    }

    /**
     * @param $id
     * @param $slug
     * @param $name
     * @param $cont
     * @param $visible
     * @param $dateAdd
     * @param $comments
     * @param $category
     * @param $file
     * @param $comment
     * @param $tags
     * @param $tag
     */
    private function populateData(
        $id,
        $slug,
        $name,
        $cont,
        $visible,
        $dateAdd,
        $comments,
        $category,
        $file,
        $comment,
        $tags,
        $tag
    ): void {
        $this->post
            ->setId($id)
            ->setSlug($slug)
            ->setName($name)
            ->setCont($cont)
            ->setVisible($visible)
            ->setDateAdd($dateAdd)
            ->setCategory($category)
            ->setComments($comments)
            ->setFile($file)
            ->addComment($comment)
            ->removeComment($comment)
            ->setTags($tags)
            ->addTag($tag)
            ->removeTag($tag);
    }

    public function testHasAttributes(): void
    {
        $this->assertClassHasAttribute('id', Post::class);
        $this->assertClassHasAttribute('slug', Post::class);
        $this->assertClassHasAttribute('name', Post::class);
        $this->assertClassHasAttribute('cont', Post::class);
        $this->assertClassHasAttribute('file', Post::class);
        $this->assertClassHasAttribute('visible', Post::class);
        $this->assertClassHasAttribute('dateAdd', Post::class);
        $this->assertClassHasAttribute('dateEdit', Post::class);
        $this->assertClassHasAttribute('dateDelete', Post::class);
        $this->assertClassHasAttribute('comments', Post::class);
        $this->assertClassHasAttribute('category', Post::class);
        $this->assertClassHasAttribute('tags', Post::class);
    }

    public function testInitialCategoryValuesAreNull(): void
    {
        $this->assertNull($this->post->getId());
        $this->assertNull($this->post->getSlug());
        $this->assertNull($this->post->getName());
        $this->assertNull($this->post->getCont());
        $this->assertNull($this->post->getFile());
        $this->assertFalse($this->post->isVisible());
        $this->assertNull($this->post->getDateAdd());
        $this->assertNull($this->post->getDateEdit());
        $this->assertNull($this->post->getDateDelete());
        $this->assertNull($this->post->getCategory());
        $this->assertInstanceOf(Collection::class, $this->post->getComments());
        $this->assertInstanceOf(Collection::class, $this->post->getTags());
    }

    /**
     * @dataProvider providerReturnedGoodValues
     * @param $id
     * @param $slug
     * @param $name
     * @param $cont
     * @param $visible
     * @param $dateAdd
     * @param $comments
     * @param $category
     * @param $file
     * @param $comment
     * @param $tags
     * @param $tag
     */
    public function testReturnedValues(
        $id,
        $slug,
        $name,
        $cont,
        $visible,
        $dateAdd,
        $comments,
        $category,
        $file,
        $comment,
        $tags,
        $tag
    ): void {
        $this->populateData(
            $id,
            $slug,
            $name,
            $cont,
            $visible,
            $dateAdd,
            $comments,
            $category,
            $file,
            $comment,
            $tags,
            $tag
        );
        $this->assertIsString($this->post->getId());
        $this->assertIsString($this->post->getSlug());
        $this->assertIsString($this->post->getName());
        $this->assertIsString($this->post->getCont());
        $this->assertIsBool($this->post->isVisible());
        $this->assertInstanceOf(File::class, $this->post->getFile());
        $this->assertInstanceOf(\DateTime::class, $this->post->getDateAdd());
        $this->assertInstanceOf(Collection::class, $this->post->getComments());
        $this->assertInstanceOf(Category::class, $this->post->getCategory());
        $this->assertInstanceOf(Collection::class, $this->post->getTags());
    }

    /**
     * @dataProvider providerReturnedBadValues
     *
     * @param $id
     * @param $slug
     * @param $name
     * @param $cont
     * @param $visible
     * @param $dateAdd
     * @param $comments
     * @param $category
     * @param $file
     * @param $comment
     * @param $tags
     * @param $tag
     * @param $exception
     */
    public function testReturnedBadValues(
        $id,
        $slug,
        $name,
        $cont,
        $visible,
        $dateAdd,
        $comments,
        $category,
        $file,
        $comment,
        $tags,
        $tag,
        $exception
    ): void {
        $this->expectException($exception);

        $this->populateData(
            $id,
            $slug,
            $name,
            $cont,
            $visible,
            $dateAdd,
            $comments,
            $category,
            $file,
            $comment,
            $tags,
            $tag
        );
    }

    public function providerReturnedGoodValues(): array
    {
        return [
            'god data' => [
                'string-value',
                'string-value',
                'string-value',
                'string-value',
                true,
                new \DateTime,
                new ArrayCollection,
                new Category,
                new File,
                new Comment,
                new ArrayCollection,
                new Tag
            ]
        ];
    }

    public function providerReturnedBadValues(): array
    {
        return $this->getBadDataWithTypeErrorException(100, 12);
    }
}