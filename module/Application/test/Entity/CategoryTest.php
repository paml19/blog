<?php

declare(strict_types=1);

namespace ApplicationTest\Entity;

use Application\Entity\Category;
use Application\Entity\Post;
use ApplicationTest\BadDataTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Faker\Factory;
use PHPUnit\Framework\TestCase;

/**
 * Class CategoryTest
 * @package ApplicationTest\Entity
 *
 * @covers \Application\Entity\Category
 */
class CategoryTest extends TestCase
{
    use BadDataTrait;

    /**
     * @var Category
     */
    private $category;

    /**
     *
     */
    protected function setUp(): void
    {
        $this->category = new Category;
    }

    /**
     * @param $id
     * @param $slug
     * @param $name
     * @param $visible
     * @param $dateAdd
     * @param $posts
     * @param $parent
     * @param $children
     * @param $child
     * @param $post
     */
    private function populateData($id, $slug, $name, $visible, $dateAdd, $posts, $parent, $children, $child, $post): void
    {
        $this->category = new Category;
        $this->category
            ->setId($id)
            ->setSlug($slug)
            ->setName($name)
            ->setVisible($visible)
            ->setDateAdd($dateAdd)
            ->setPosts($posts)
            ->setParent($parent)
            ->setChildren($children)
            ->addChild($child)
            ->removeChild($child)
            ->addPost($post)
            ->removePost($post);
    }

    /**
     *
     */
    public function testHasAttributes(): void
    {
        $this->assertClassHasAttribute('id', Category::class);
        $this->assertClassHasAttribute('slug', Category::class);
        $this->assertClassHasAttribute('name', Category::class);
        $this->assertClassHasAttribute('parent', Category::class);
        $this->assertClassHasAttribute('visible', Category::class);
        $this->assertClassHasAttribute('dateAdd', Category::class);
        $this->assertClassHasAttribute('dateEdit', Category::class);
        $this->assertClassHasAttribute('dateDelete', Category::class);
        $this->assertClassHasAttribute('posts', Category::class);
    }

    /**
     *
     */
    public function testInitialCategoryValuesAreNull(): void
    {
        $this->assertNull($this->category->getId());
        $this->assertNull($this->category->getSlug());
        $this->assertNull($this->category->getName());
        $this->assertNull($this->category->getParent());
        $this->assertNull($this->category->getParent());
        $this->assertFalse($this->category->isVisible());
        $this->assertNull($this->category->getDateAdd());
        $this->assertNull($this->category->getDateEdit());
        $this->assertNull($this->category->getDateDelete());
        $this->assertInstanceOf(Collection::class, $this->category->getPosts());
        $this->assertInstanceOf(Collection::class, $this->category->getChildren());
    }

    /**
     * @dataProvider providerGoodDataWithParent
     *
     * @param $id
     * @param $slug
     * @param $name
     * @param $visible
     * @param $dateAdd
     * @param $posts
     * @param $parent
     * @param $children
     * @param $child
     * @param $post
     */
    public function testReturnedValuesWithParent(
        $id,
        $slug,
        $name,
        $visible,
        $dateAdd,
        $posts,
        $parent,
        $children,
        $child,
        $post
    ): void {
        $this->populateData($id, $slug, $name, $visible, $dateAdd, $posts, $parent, $children, $child, $post);

        $this->assertIsString($this->category->getId());
        $this->assertIsString($this->category->getSlug());
        $this->assertIsString($this->category->getName());
        $this->assertIsBool($this->category->isVisible());
        $this->assertInstanceOf(\DateTime::class, $this->category->getDateAdd());
        $this->assertInstanceOf(Collection::class, $this->category->getPosts());
        $this->assertInstanceOf(Category::class, $this->category->getParent());
        $this->assertInstanceOf(Collection::class, $this->category->getChildren());
    }

    /**
     * @dataProvider providerGoodDataWithoutParent
     *
     * @param $id
     * @param $slug
     * @param $name
     * @param $visible
     * @param $dateAdd
     * @param $posts
     * @param $parent
     * @param $children
     * @param $child
     * @param $post
     */
    public function testReturnedValuesWithoutParent(
        $id,
        $slug,
        $name,
        $visible,
        $dateAdd,
        $posts,
        $parent,
        $children,
        $child,
        $post
    ): void {
        $this->populateData($id, $slug, $name, $visible, $dateAdd, $posts, $parent, $children, $child, $post);

        $this->assertIsString($this->category->getId());
        $this->assertIsString($this->category->getSlug());
        $this->assertIsString($this->category->getName());
        $this->assertIsBool($this->category->isVisible());
        $this->assertInstanceOf(\DateTime::class, $this->category->getDateAdd());
        $this->assertInstanceOf(Collection::class, $this->category->getPosts());
        $this->assertInstanceOf(Collection::class, $this->category->getChildren());
        $this->assertNull($this->category->getParent());
    }

    /**
     * @dataProvider providerBadData
     *
     * @param $id
     * @param $slug
     * @param $name
     * @param $visible
     * @param $dateAdd
     * @param $posts
     * @param $parent
     * @param $children
     * @param $child
     * @param $post
     * @param $exception
     */
    public function testReturnedBadValues(
        $id,
        $slug,
        $name,
        $visible,
        $dateAdd,
        $posts,
        $parent,
        $children,
        $child,
        $post,
        $exception
    ): void {
        $this->expectException($exception);

        $this->populateData($id, $slug, $name, $visible, $dateAdd, $posts, $parent, $children, $child, $post);
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function providerGoodDataWithParent(): array
    {
        return [
            'good visible true' => [
                'string-uuid-value',
                'string-slug-value',
                'string-name-value',
                true,
                new \DateTime,
                new ArrayCollection,
                new Category,
                new ArrayCollection,
                new Category,
                new Post
            ],
            'good visible false' => [
                'string-uuid-value',
                'string-slug-value',
                'string-name-value',
                false,
                new \DateTime,
                new ArrayCollection,
                new Category,
                new ArrayCollection,
                new Category,
                new Post
            ]
        ];
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function providerGoodDataWithoutParent(): array
    {
        return [
            'good visible true' => [
                'string-uuid-value',
                'string-slug-value',
                'string-name-value',
                true,
                new \DateTime,
                new ArrayCollection,
                null,
                new ArrayCollection,
                new Category,
                new Post
            ],
            'good visible false' => [
                'string-uuid-value',
                'string-slug-value',
                'string-name-value',
                false,
                new \DateTime,
                new ArrayCollection,
                null,
                new ArrayCollection,
                new Category,
                new Post
            ]
        ];
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function providerBadData(): array
    {
        return $this->getBadDataWithTypeErrorException(100, 10);
    }
}