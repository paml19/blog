<?php

namespace ApplicationTest\Entity;

use Application\Entity\Comment;
use Application\Entity\Post;
use ApplicationTest\BadDataTrait;
use Faker\Factory;
use PHPUnit\Framework\TestCase;

/**
 * Class CommentTest
 * @package ApplicationTest\Entity
 *
 * @covers \Application\Entity\Comment
 */
class CommentTest extends TestCase
{
    use BadDataTrait;

    /**
     * @var Comment
     */
    private $comment;

    /**
     *
     */
    protected function setUp(): void
    {
        $this->comment = new Comment;
    }

    /**
     * @param $id
     * @param $email
     * @param $text
     * @param $dateAdd
     * @param $post
     */
    private function populateData($id, $email, $text, $dateAdd, $post): void
    {
        $this->comment
            ->setId($id)
            ->setName($email)
            ->setCont($text)
            ->setDateAdd($dateAdd)
            ->setPost($post);
    }

    /**
     *
     */
    public function testHasAttributes(): void
    {
        self::assertClassHasAttribute('id', Comment::class);
        self::assertClassHasAttribute('name', Comment::class);
        self::assertClassHasAttribute('cont', Comment::class);
        self::assertClassHasAttribute('dateAdd', Comment::class);
        self::assertClassHasAttribute('dateEdit', Comment::class);
        self::assertClassHasAttribute('dateDelete', Comment::class);
        self::assertClassHasAttribute('post', Comment::class);
    }

    /**
     *
     */
    public function testInitialCategoryValuesAreNull(): void
    {
        $this->assertNull($this->comment->getId());
        self::assertNull($this->comment->getPost());
        $this->assertNull($this->comment->getName());
        $this->assertNull($this->comment->getCont());
        $this->assertNull($this->comment->getDateAdd());
        $this->assertNull($this->comment->getDateEdit());
        $this->assertNull($this->comment->getDateDelete());
    }

    /**
     * @dataProvider providerReturnGoodValues
     * @param $id
     * @param $email
     * @param $text
     * @param $dateAdd
     * @param $post
     */
    public function testReturnedValues($id, $email, $text, $dateAdd, $post): void
    {
        $this->populateData($id, $email, $text, $dateAdd, $post);

        self::assertIsString($this->comment->getId());
        self::assertIsString($this->comment->getName());
        self::assertIsString($this->comment->getCont());
        self::assertInstanceOf(\DateTime::class, $this->comment->getDateAdd());
        self::assertInstanceOf(Post::class, $this->comment->getPost());
    }

    /**
     * @dataProvider providerReturnBadData
     *
     * @param $id
     * @param $name
     * @param $content
     * @param $dateAdd
     * @param $post
     * @param $exception
     */
    public function testExceptions($id, $name, $content, $dateAdd, $post, $exception): void
    {
        $this->expectException($exception);

        $this->populateData($id, $name, $content, $dateAdd, $post);
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function providerReturnGoodValues()
    {
        return [
            'good values' => [
                'string-value',
                'string-value',
                'string-value',
                new \DateTime,
                new Post
            ]
        ];
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function providerReturnBadData()
    {
        return $this->getBadDataWithTypeErrorException(100, 5);
    }
}
