<?php

declare(strict_types=1);

namespace ApplicationTest;

use paml\Layouts\ColorAdmin\Plugin\DataTableButtons;

trait DataTableButtonsTrait
{
    private $dataTableButtons;

    protected function setUp(): void
    {
        parent::setUp();
        $this->dataTableButtons = $this->prophesize(DataTableButtons::class);
    }
}