<?php

declare(strict_types=1);

namespace ApplicationTest;

/**
 * Trait BadDataTrait
 * @package ApplicationTest
 */
trait BadDataTrait
{
    /**
     * @param int $limit
     * @param int $parameters
     * @param array $without
     * @return array
     * @throws \Exception
     */
    public function getBadDataWithTypeErrorException(int $limit, int $parameters, ?array $without = []): array
    {
        $badData = [
            123,
            123,
            'string-value',
            'string-value',
            true,
            false,
            new \DateTime,
            123,
            123,
            'string-value',
            'string-value',
            true,
            false,
            new \DateTime
        ];

        $returnData = [];
        for ($i = 0; $i < $limit; $i++) {
            shuffle($badData);
            for ($j = 0; $j < $parameters; $j++) {
                if (! in_array(gettype($badData[$j]), $without)) {
                    $returnData[$i][$j] = $badData[$j];
                } else {
                    shuffle($badData);
                    $j--;
                }
            }
            $returnData[$i][$j] = \TypeError::class;
        }

        return  $returnData;
    }
}