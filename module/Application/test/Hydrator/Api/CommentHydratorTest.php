<?php

namespace ApplicationTest\Hydrator\Api;

use Application\Entity\Comment;
use Application\Entity\Post;
use Application\Hydrator\Api\CommentHydrator;
use Application\Hydrator\Api\PostListHydrator;
use Application\Repository\PostRepository;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

/**
 * Class CommentHydratorTest
 * @package ApplicationTest\Hydrator\Api
 */
class CommentHydratorTest extends TestCase
{
    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $postListHydrator;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $postRepository;

    /**
     *
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->postListHydrator = $this->prophesize(PostListHydrator::class);
        $this->postRepository = $this->prophesize(PostRepository::class);
    }

    /**
     * @dataProvider providerForTestExtractWithHydrator
     *
     * @param $object
     */
    public function testExtractWithHydrator($object): void
    {
        $this->postListHydrator->extract(Argument::type(Post::class))
            ->willReturn([])
            ->shouldBeCalled();

        $commentHydrator = new CommentHydrator(
            $this->postListHydrator->reveal(),
            $this->postRepository->reveal()
        );

        $this->assertIsArray($commentHydrator->extract($object));
    }

    /**
     * @dataProvider providerForTestExtractWithoutHydrator
     *
     * @param $object
     */
    public function testExtractWithoutHydrator($object): void
    {
        $commentHydrator = new CommentHydrator(
            $this->postListHydrator->reveal(),
            $this->postRepository->reveal()
        );

        $this->assertIsArray($commentHydrator->extract($object));
    }

    /**
     * @dataProvider providerForTestHydrateWithRepository
     *
     * @param $data
     * @param $object
     */
    public function testHydrateWithRepository($data, $object): void
    {
        $this->postRepository->find(Argument::type('string'))
            ->willReturn(new Post)
            ->shouldBeCalled();

        $commentHydrator = new CommentHydrator(
            $this->postListHydrator->reveal(),
            $this->postRepository->reveal()
        );

        $this->assertInstanceOf(Comment::class, $commentHydrator->hydrate($data, $object));
    }

    /**
     * @dataProvider providerForTestHydrateWithoutRepository
     *
     * @param $data
     * @param $object
     */
    public function testHydrateWithoutRepository($data, $object): void
    {
        $commentHydrator = new CommentHydrator(
            $this->postListHydrator->reveal(),
            $this->postRepository->reveal()
        );

        $this->assertInstanceOf(Comment::class, $commentHydrator->hydrate($data, $object));
    }

    /**
     * @return array
     */
    public function providerForTestExtractWithHydrator(): array
    {
        $comment = (new Comment)
            ->setPost(new Post);

        return [
            [$comment]
        ];
    }

    /**
     * @return array
     */
    public function providerForTestExtractWithoutHydrator(): array
    {
        return [
            [new Comment]
        ];
    }

    /**
     * @return array
     */
    public function providerForTestHydrateWithRepository(): array
    {
        return [
            'data' => [
                [
                    Comment::NAME => 'sample-string',
                    Comment::CONTENT => 'sample-string',
                    Comment::POST => 'sample-string'
                ],
                new Comment
            ],
        ];
    }

    /**
     * @return array
     */
    public function providerForTestHydrateWithoutRepository(): array
    {
        return [
            [[], new Comment],
        ];
    }
}
