<?php

namespace ApplicationTest\Hydrator\Api;

use Application\Entity\Category;
use Application\Entity\Post;
use Application\Hydrator\Api\CategoryHydrator;
use Application\Hydrator\Api\CategoryListHydrator;
use Application\Hydrator\Api\PostListHydrator;
use Application\Repository\CategoryRepository;
use Application\Repository\PostRepository;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

/**
 * Class CategoryHydratorTest
 * @package ApplicationTest\Hydrator\Api
 */
class CategoryHydratorTest extends TestCase
{
    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $postListHydrator;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $categoryListHydrator;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $categoryRepository;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $postRepository;

    /**
     *
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->postListHydrator = $this->prophesize(PostListHydrator::class);
        $this->categoryListHydrator = $this->prophesize(CategoryListHydrator::class);
        $this->categoryRepository = $this->prophesize(CategoryRepository::class);
        $this->postRepository = $this->prophesize(PostRepository::class);
    }

    /**
     * @dataProvider providerForTestExtractWithHydrators
     *
     * @param $object
     */
    public function testExtractWithHydrators($object): void
    {
        $this->postListHydrator->extract(Argument::type('object'))
            ->willReturn([])
            ->shouldBeCalled();

        $this->categoryListHydrator->extract(Argument::type('object'))
            ->willReturn([])
            ->shouldBeCalled();

        $categoryHydrator = new CategoryHydrator(
            $this->postListHydrator->reveal(),
            $this->categoryListHydrator->reveal(),
            $this->categoryRepository->reveal(),
            $this->postRepository->reveal()
        );

        $this->assertIsArray($categoryHydrator->extract($object));
    }

    /**
     * @dataProvider providerForTestHydratorWithoutHydrators
     *
     * @param $object
     */
    public function testExtractWithoutOtherHydrators($object): void
    {
        $categoryHydrator = new CategoryHydrator(
            $this->postListHydrator->reveal(),
            $this->categoryListHydrator->reveal(),
            $this->categoryRepository->reveal(),
            $this->postRepository->reveal()
        );

        $this->assertIsArray($categoryHydrator->extract($object));
    }

    /**
     * @dataProvider providerForTestHydrateWithRepositories
     *
     * @param $data
     * @param $object
     */
    public function testHydrateWithOtherRepositories($data, $object): void
    {
        $this->categoryRepository->count(Argument::type('array'))
            ->willReturn(123)
            ->shouldBeCalled();

        $this->categoryRepository->find(Argument::type('string'))
            ->willReturn(new Category)
            ->shouldBeCalledTimes(4);

        $this->postRepository->find(Argument::type('string'))
            ->willReturn(new Post)
            ->shouldBeCalledTimes(3);

        $categoryHydrator = new CategoryHydrator(
            $this->postListHydrator->reveal(),
            $this->categoryListHydrator->reveal(),
            $this->categoryRepository->reveal(),
            $this->postRepository->reveal()
        );

        $this->assertInstanceOf(Category::class, $categoryHydrator->hydrate($data, $object));
    }

    /**
     * @dataProvider providerForTestHydrateWithoutRepositories
     *
     * @param $data
     * @param $object
     */
    public function testHydrateWithoutRepositories($data, $object): void
    {
        $categoryHydrator = new CategoryHydrator(
            $this->postListHydrator->reveal(),
            $this->categoryListHydrator->reveal(),
            $this->categoryRepository->reveal(),
            $this->postRepository->reveal()
        );

        $this->assertInstanceOf(Category::class, $categoryHydrator->hydrate($data, $object));
    }

    /**
     * @return array
     */
    public function providerForTestExtractWithHydrators(): array
    {
        $posts = new ArrayCollection;
        $children = new ArrayCollection;
        for ($i = 0; $i < 10; $i++) {
            $posts->add(new Post);
            $children->add(new Category);
        }

        $category = (new Category)
            ->setPosts($posts)
            ->setChildren($children)
            ->setParent(new Category);

        return [
            [$category]
        ];
    }

    /**
     * @return array
     */
    public function providerForTestHydratorWithoutHydrators(): array
    {
        return [
            [new Category]
        ];
    }

    /**
     * @return array
     */
    public function providerForTestHydrateWithRepositories(): array
    {
        return [
            'data with visible true' => [
                [
                    Category::NAME => 'sample-name',
                    Category::PARENT => 'sample-name',
                    Category::CHILD => 'sample-name',
                    Category::CHILDREN => [
                        'sample-string',
                        [Category::ID => 'sample-string'],
                    ],
                    Category::POST => 'sample-name',
                    Category::POSTS => [
                        'sample-string',
                        [Post::ID => 'sample-string'],
                    ],
                    Category::VISIBLE => 'yes',
                ],
                new Category
            ],
            'data with visible false' => [
                [
                    Category::NAME => 'sample-name',
                    Category::PARENT => 'sample-name',
                    Category::CHILD => 'sample-name',
                    Category::CHILDREN => [
                        'sample-string',
                        [Category::ID => 'sample-string'],
                    ],
                    Category::POST => 'sample-name',
                    Category::POSTS => [
                        'sample-string',
                        [Post::ID => 'sample-string'],
                    ],
                    Category::VISIBLE => 'no',
                ],
                new Category
            ]
        ];
    }

    /**
     * @return array
     */
    public function providerForTestHydrateWithoutRepositories(): array
    {
        return [
            [[], new Category],
        ];
    }
}
