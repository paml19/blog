<?php

declare(strict_types=1);

namespace ApplicationTest\Hydrator\Api;

use Application\Entity\Comment;
use Application\Hydrator\Api\CommentListHydrator;
use PHPUnit\Framework\TestCase;

/**
 * Class CommentListHydratorTest
 * @package ApplicationTest\Hydrator\Api
 */
class CommentListHydratorTest extends TestCase
{
    /**
     * @dataProvider providerForTestExtract
     *
     * @param $object
     */
    public function testExtract($object): void
    {
        $commentListHydrator = new CommentListHydrator;

        $this->assertIsArray($commentListHydrator->extract($object));
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function providerForTestExtract(): array
    {
        $objectWithData = (new Comment)
            ->setId('sample-string')
            ->setDateAdd(new \DateTime);

        return [
            [$objectWithData],
            [new Comment]
        ];
    }
}
