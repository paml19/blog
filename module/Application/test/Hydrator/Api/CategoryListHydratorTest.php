<?php

namespace ApplicationTest\Hydrator\Api;

use Application\Entity\Category;
use Application\Hydrator\Api\CategoryListHydrator;
use PHPUnit\Framework\TestCase;

/**
 * Class CategoryListHydratorTest
 * @package ApplicationTest\Hydrator\Api
 */
class CategoryListHydratorTest extends TestCase
{
    /**
     * @dataProvider providerForTestExtract
     *
     * @param $object
     */
    public function testExtract($object): void
    {
        $categoryListHydrator = new CategoryListHydrator;

        $this->assertIsArray($categoryListHydrator->extract($object));
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function providerForTestExtract(): array
    {
        $objectWithValues = (new Category)
            ->setId('sample-string')
            ->setName('sample-string')
            ->setDateAdd(new \DateTime);

        return [
            [$objectWithValues],
            [new Category]
        ];
    }
}
