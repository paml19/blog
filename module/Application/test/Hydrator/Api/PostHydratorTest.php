<?php

namespace ApplicationTest\Hydrator\Api;

use Application\Entity\Category;
use Application\Entity\Comment;
use Application\Entity\Post;
use Application\Hydrator\Api\CategoryHydrator;
use Application\Hydrator\Api\CommentListHydrator;
use Application\Hydrator\Api\PostHydrator;
use Application\Repository\CategoryRepository;
use Application\Repository\PostRepository;
use Doctrine\Common\Collections\ArrayCollection;
use paml\File\Entity\File;
use paml\File\Library\Hydrator\FileHydrator;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

/**
 * Class PostHydratorTest
 * @package ApplicationTest\Hydrator\Api
 */
class PostHydratorTest extends TestCase
{
    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $categoryHydrator;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $categoryRepository;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $commentListHydrator;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $fileHydrator;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $postRepository;

    /**
     *
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->categoryHydrator = $this->prophesize(CategoryHydrator::class);
        $this->categoryRepository = $this->prophesize(CategoryRepository::class);
        $this->commentListHydrator = $this->prophesize(CommentListHydrator::class);
        $this->fileHydrator = $this->prophesize(FileHydrator::class);
        $this->postRepository = $this->prophesize(PostRepository::class);
    }

    /**
     * @dataProvider providerForTestExtractWithHydrators
     *
     * @param $object
     */
    public function testExtractWithHydrators($object): void
    {
        $this->categoryHydrator->extract(Argument::type(Category::class))
            ->willReturn([])
            ->shouldBeCalledTimes(1);

        $this->commentListHydrator->extract(Argument::type(Comment::class))
            ->willReturn([])
            ->shouldBeCalled();

        $this->fileHydrator->extract(Argument::type(File::class))
            ->willReturn([])
            ->shouldBeCalledTimes(1);

        $this->extractAndAssert($object);
    }

    /**
     * @dataProvider providerForTestExtractWithoutHydrators
     *
     * @param $object
     */
    public function testExtractWithoutHydrators($object): void
    {
        $this->extractAndAssert($object);
    }

    /**
     * @dataProvider providerForTestHydrateWithRepositories
     *
     * @param $data
     * @param $object
     */
    public function testHydrateWithRepositories($data, $object): void
    {
        $this->postRepository->count(Argument::type('array'))
            ->willReturn(123)
            ->shouldBeCalledTimes(1);

        $this->categoryRepository->find(Argument::type('string'))
            ->willReturn(new Category)
            ->shouldBeCalledTimes(1);

        $this->fileHydrator->hydrate(Argument::type('array'), Argument::type(File::class))
            ->willReturn(new File)
            ->shouldBeCalledTimes(1);

        $this->hydrateAndAssert($data, $object);
    }

    /**
     * @dataProvider providerForTestHydrateWithoutRepositories
     *
     * @param $data
     * @param $object
     */
    public function testHydrateWithoutRepositories($data, $object): void
    {
        $this->hydrateAndAssert($data, $object);
    }

    /**
     * @return array
     */
    public function providerForTestExtractWithHydrators(): array
    {
        $comments = new ArrayCollection;

        for ($i = 0; $i < 10; $i++) {
            $comments->add(new Comment);
        }

        $post = (new Post)
            ->setComments($comments)
            ->setCategory(new Category)
            ->setFile(new File);

        return [[$post]];
    }

    /**
     * @return array
     */
    public function providerForTestExtractWithoutHydrators(): array
    {
        return [[new Post]];
    }

    /**
     * @return array
     */
    public function providerForTestHydrateWithRepositories(): array
    {
        return [
            'data post visible' => [
                [
                    Post::NAME => 'sample-name',
                    Post::CATEGORY => 'sample-id',
                    Post::CONTENT => 'sample-content',
                    Post::VISIBLE => 'yes',
                    Post::FILE => [
                        'name' => 'sample-file-name'
                    ]
                ],
                new Post,
            ],
            'data post not visible' => [
                [
                    Post::NAME => 'sample-name',
                    Post::CATEGORY => 'sample-id',
                    Post::CONTENT => 'sample-content',
                    Post::VISIBLE => 'no',
                    Post::FILE => [
                        'name' => 'sample-file-name'
                    ]
                ],
                new Post,
            ]
        ];
    }

    /**
     * @return array
     */
    public function providerForTestHydrateWithoutRepositories(): array
    {
        return [
            [[], new Post]
        ];
    }

    /**
     * @param $object
     */
    private function extractAndAssert($object): void
    {
        $postHydrator = $this->createPostHydrator();

        $this->assertIsArray($postHydrator->extract($object));
    }

    /**
     * @param $data
     * @param $object
     */
    private function hydrateAndAssert($data, $object): void
    {
        $postHydrator = $this->createPostHydrator();

        $this->assertInstanceOf(Post::class, $postHydrator->hydrate($data, $object));
    }

    /**
     * @return PostHydrator
     */
    private function createPostHydrator(): PostHydrator
    {
        return new PostHydrator(
            $this->categoryHydrator->reveal(),
            $this->categoryRepository->reveal(),
            $this->commentListHydrator->reveal(),
            $this->postRepository->reveal(),
            $this->fileHydrator->reveal()
        );
    }
}
