<?php

declare(strict_types=1);

namespace ApplicationTest\Hydrator\Api;

use Application\Entity\Post;
use Application\Hydrator\Api\PostListHydrator;
use PHPUnit\Framework\TestCase;

/**
 * Class PostListHydratorTest
 * @package ApplicationTest\Hydrator\Api
 */
class PostListHydratorTest extends TestCase
{
    /**
     * @dataProvider providerForTestExtract
     *
     * @param $object
     */
    public function testExtract($object): void
    {
        $postListHydrator = new PostListHydrator;

        $this->assertIsArray($postListHydrator->extract($object));
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function providerForTestExtract(): array
    {
        $objectWithData = (new Post)
            ->setId('sample-string')
            ->setName('sample-string')
            ->setDateAdd(new \DateTime);

        return [
            [$objectWithData],
            [new Post],
        ];
    }
}
