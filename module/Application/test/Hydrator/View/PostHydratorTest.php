<?php

namespace ApplicationTest\Hydrator\View;

use Application\Entity\Category;
use Application\Entity\Comment;
use Application\Entity\Post;
use Application\Entity\Tag;
use Application\Hydrator\View\CategoryHydrator;
use Application\Hydrator\View\PostHydrator;
use Application\Repository\CategoryRepository;
use Application\Repository\PostRepository;
use Application\Repository\TagRepository;
use Doctrine\Common\Collections\ArrayCollection;
use paml\File\Entity\File;
use paml\File\Library\Hydrator\FileHydrator;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class PostHydratorTest extends TestCase
{
    private $categoryRepository;

    private $fileHydrator;

    private $postRepository;

    private $tagRepository;

    private $categoryHydrator;

    protected function setUp(): void
    {
        parent::setUp();

        $this->categoryRepository = $this->prophesize(CategoryRepository::class);
        $this->fileHydrator = $this->prophesize(FileHydrator::class);
        $this->postRepository = $this->prophesize(PostRepository::class);
        $this->tagRepository = $this->prophesize(TagRepository::class);
        $this->categoryHydrator = $this->prophesize(CategoryHydrator::class);
    }

    /**
     * @dataProvider providerForTestExtract
     *
     * @param $object
     */
    public function testExtract($object): void
    {
        $this->fileHydrator->extract(Argument::type(File::class))
            ->willReturn([])
            ->shouldBeCalledTimes(1);

        $postHydrator = $this->createPostHydrator();
        $this->assertIsArray($postHydrator->extract($object));
    }

    /**
     * @dataProvider providerForTestHydrate
     *
     * @param $data
     * @param $object
     */
    public function testHydrate($data, $object): void
    {
        $this->postRepository->count(Argument::type('array'))
            ->willReturn(123)
            ->shouldBeCalledTimes(1);

        $this->categoryRepository->find(Argument::type('string'))
            ->willReturn(new Category)
            ->shouldBeCalledTimes(1);

        $this->tagRepository->find(Argument::type('string'))
            ->willReturn(new Tag)
            ->shouldBeCalledTimes(2);

        $this->fileHydrator->hydrate(Argument::type('array'), Argument::type(File::class))
            ->willReturn(new File)
            ->shouldBeCalledTimes(1);

        $postHydrator = $this->createPostHydrator();
        $this->assertInstanceOf(Post::class, $postHydrator->hydrate($data, $object));
    }

    public function providerForTestExtract(): array
    {
        $comments = new ArrayCollection;
        $tags = new ArrayCollection;

        for ($i = 0; $i < 10; $i++) {
            $comments->add(new Comment);
            $tags->add(new Tag);
        }

        $post = (new Post)
            ->setComments($comments)
            ->setTags($tags)
            ->setCategory(new Category)
            ->setFile(new File);

        return  [
            [$post]
        ];
    }

    public function providerForTestHydrate(): array
    {
        return [
            'data post visible' => [
                [
                    Post::NAME => 'sample-name',
                    Post::CATEGORY => 'sample-category-id',
                    Post::TAGS => [
                        'sample-tag-id',
                        [
                            Tag::ID => 'sample-tag-id',
                        ],
                    ],
                    Post::CONTENT => 'sample-content',
                    Post::VISIBLE => 'yes',
                    Post::FILE => [
                        File::NAME => 'sample-file-name',
                    ],
                ],
                new Post
            ],
            'data post not visible' => [
                [
                    Post::NAME => 'sample-name',
                    Post::CATEGORY => 'sample-category-id',
                    Post::TAGS => [
                        'sample-tag-id',
                        [
                            Tag::ID => 'sample-tag-id',
                        ],
                    ],
                    Post::CONTENT => 'sample-content',
                    Post::VISIBLE => 'no',
                    Post::FILE => [
                        File::NAME => 'sample-file-name',
                    ],
                ],
                new Post
            ],
        ];
    }

    private function createPostHydrator(): PostHydrator
    {
        return new PostHydrator(
            $this->categoryRepository->reveal(),
            $this->fileHydrator->reveal(),
            $this->postRepository->reveal(),
            $this->tagRepository->reveal(),
            $this->categoryHydrator->reveal()
        );
    }
}
