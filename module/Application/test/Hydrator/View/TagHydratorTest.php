<?php

namespace ApplicationTest\Hydrator\View;

use Application\Entity\Post;
use Application\Entity\Tag;
use Application\Hydrator\View\TagHydrator;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;

/**
 * Class TagHydratorTest
 * @package ApplicationTest\Hydrator\View
 */
class TagHydratorTest extends TestCase
{
    /**
     * @dataProvider providerForTestExtract
     *
     * @param $object
     */
    public function testExtract($object): void
    {
        $tagHydrator = new TagHydrator;

        $this->assertIsArray($tagHydrator->extract($object));
    }

    /**
     * @dataProvider providerForTestHydrate
     *
     * @param $data
     * @param $object
     */
    public function testHydrate($data, $object): void
    {
        $tagHydrator = new TagHydrator;

        $this->assertInstanceOf(Tag::class, $tagHydrator->hydrate($data, $object));
    }

    /**
     * @return array
     */
    public function providerForTestExtract(): array
    {
        $posts = new ArrayCollection;

        for ($i = 0; $i < 10; $i++) {
            $posts->add(new Post);
        }

        $tag = (new Tag)
            ->setPosts($posts);

        return [
            [$tag]
        ];
    }

    /**
     * @return array
     */
    public function providerForTestHydrate(): array
    {
        return [
            'data tag visible' => [
                [
                    Tag::NAME => 'sample-name',
                    Tag::VISIBLE => 'yes',
                ],
                new Tag
            ],
            'data tag not visible' => [
                [
                    Tag::NAME => 'sample-name',
                    Tag::VISIBLE => 'no',
                ],
                new Tag
            ]
        ];
    }
}
