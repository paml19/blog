<?php

namespace ApplicationTest\Hydrator\View;

use Application\Entity\Category;
use Application\Entity\Post;
use Application\Hydrator\View\CategoryHydrator;
use Application\Repository\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

/**
 * Class CategoryHydratorTest
 * @package ApplicationTest\Hydrator\View
 */
class CategoryHydratorTest extends TestCase
{
    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $categoryRepository;

    /**
     *
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->categoryRepository = $this->prophesize(CategoryRepository::class);
    }

    /**
     * @dataProvider providerForTestHydrate
     *
     * @param $data
     * @param $object
     */
    public function testHydrate($data, $object): void
    {
        $this->categoryRepository->count(Argument::type('array'))
            ->willReturn(123)
            ->shouldBeCalledTimes(1);

        $this->categoryRepository->find(Argument::type('string'))
            ->willReturn(new Category)
            ->shouldBeCalled();

        $categoryHydrator = $this->createCategoryHydrator();

        $this->assertInstanceOf(Category::class, $categoryHydrator->hydrate($data, $object));
    }

    /**
     * @dataProvider providerForTestExtract
     *
     * @param $object
     */
    public function testExtract($object): void
    {
        $categoryHydrator = $this->createCategoryHydrator();

        $this->assertIsArray($categoryHydrator->extract($object));
    }

    /**
     * @return array
     */
    public function providerForTestHydrate(): array
    {
        return [
            [
                [
                    Category::NAME => 'sample-name',
                    Category::PARENT => 'sample-parent-id',
                    Category::CHILDREN => [
                        'sample-child-id',
                        [
                            Category::ID => 'sample-child-id'
                        ],
                    ],
                    Category::VISIBLE => 'yes'
                ],
                new Category
            ],
            [
                [
                    Category::NAME => 'sample-name',
                    Category::PARENT => 'sample-parent-id',
                    Category::CHILDREN => [
                        'sample-child-id',
                        [
                            Category::ID => 'sample-child-id'
                        ],
                    ],
                    Category::VISIBLE => 'no'
                ],
                new Category
            ]
        ];
    }

    /**
     * @return array
     */
    public function providerForTestExtract(): array
    {
        $posts = new ArrayCollection;
        $children = new ArrayCollection;

        for ($i = 0; $i < 10; $i++) {
            $posts->add(new Post);
            $children->add(new Category);
        }

        $category = (new Category)
            ->setPosts($posts)
            ->setChildren($children);

        return [
            [$category]
        ];
    }

    /**
     * @return CategoryHydrator
     */
    private function createCategoryHydrator(): CategoryHydrator
    {
        return new CategoryHydrator(
            $this->categoryRepository->reveal()
        );
    }
}
