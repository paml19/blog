<?php

namespace ApplicationTest\DTO;

use Application\DTO\PostDTO;
use ApplicationTest\BadDataTrait;
use ApplicationTest\DataTableButtonsTrait;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class PostDTOTest extends TestCase
{
    use BadDataTrait;
    use DataTableButtonsTrait;

    public function testHasAttributes(): void
    {
        $this->assertClassHasAttribute('id', PostDTO::class);
        $this->assertClassHasAttribute('name', PostDTO::class);
        $this->assertClassHasAttribute('active', PostDTO::class);
        $this->assertClassHasAttribute('dateAdd', PostDTO::class);
        $this->assertClassHasAttribute('commentsCount', PostDTO::class);
    }

    /**
     * @dataProvider providerForReturnValues
     *
     * @param $id
     * @param $name
     * @param $active
     * @param $dateAdd
     * @param $commentsCount
     */
    public function testReturnValues($id, $name, $active, $dateAdd, $commentsCount): void
    {
        $postDTO = new PostDTO($id, $name, $active, $dateAdd, $commentsCount);

        $this->assertNotNull($postDTO->getId());
        $this->assertNotNull($postDTO->getName());
        $this->assertNotNull($postDTO->isActive());
        $this->assertNotNull($postDTO->getDateAdd());
        $this->assertNotNull($postDTO->getCommentsCount());

        $this->assertIsString($postDTO->getId());
        $this->assertIsString($postDTO->getName());
        $this->assertIsBool($postDTO->isActive());
        $this->assertInstanceOf(\DateTime::class, $postDTO->getDateAdd());
        $this->assertIsInt($postDTO->getCommentsCount());

        $this->dataTableButtons->prepareButtons(Argument::type('string'))
            ->willReturn('string')
            ->shouldBeCalled();

        $this->assertIsArray($postDTO->toArray($this->dataTableButtons->reveal()));
    }

    /**
     * @dataProvider providerForTestExceptions
     *
     * @param $id
     * @param $name
     * @param $active
     * @param $dateAdd
     * @param $commentsCount
     * @param $exception
     */
    public function testExceptions($id, $name, $active, $dateAdd, $commentsCount, $exception): void
    {
        $this->expectException($exception);

        new PostDTO($id, $name, $active, $dateAdd, $commentsCount);
    }

    public function providerForReturnValues(): array
    {
        return [
            ['sample-string', 'sample-string', true, new \DateTime, 100],
            ['sample-string', 'sample-string', false, new \DateTime, 100],
        ];
    }

    public function providerForTestExceptions(): array
    {
        return $this->getBadDataWithTypeErrorException(100, 5, ['integer', 'string', 'boolean']);
    }
}
