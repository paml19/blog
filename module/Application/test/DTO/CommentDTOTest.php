<?php

declare(strict_types=1);

namespace ApplicationTest\DTO;

use Application\DTO\CommentDTO;
use ApplicationTest\BadDataTrait;
use ApplicationTest\DataTableButtonsTrait;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

/**
 * Class CommentDTOTest
 * @package ApplicationTest\DTO
 */
class CommentDTOTest extends TestCase
{
    use BadDataTrait;
    use DataTableButtonsTrait;

    /**
     *
     */
    public function testHasAttributes(): void
    {
        $this->assertClassHasAttribute('name', CommentDTO::class);
        $this->assertClassHasAttribute('cont', CommentDTO::class);
        $this->assertClassHasAttribute('dateAdd', CommentDTO::class);
    }

    /**
     * @dataProvider providerForTestReturnedValues
     *
     * @param $name
     * @param $cont
     * @param $dateAdd
     */
    public function testReturnedValues($name, $cont, $dateAdd): void
    {
        $commentDTO = new CommentDTO($name, $dateAdd, $cont);

        $this->assertNotNull($commentDTO->getName());
        $this->assertNotNull($commentDTO->getCont());
        $this->assertNotNull($commentDTO->getDateAdd());

        $this->assertIsString($commentDTO->getName());
        $this->assertIsString($commentDTO->getCont());
        $this->assertInstanceOf(\DateTime::class, $commentDTO->getDateAdd());

        $this->assertIsArray($commentDTO->toArray($this->dataTableButtons->reveal()));
    }

    /**
     * @dataProvider providerForTestExceptions
     *
     * @param $name
     * @param $cont
     * @param $dateAdd
     * @param $exception
     */
    public function testExceptions($name, $cont, $dateAdd, $exception): void
    {
        $this->expectException($exception);

        new CommentDTO($name, $dateAdd, $cont);
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function providerForTestReturnedValues(): array
    {
        return [
            ['sample-name', 'sample-content', new \DateTime]
        ];
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function providerForTestExceptions(): array
    {
        return $this->getBadDataWithTypeErrorException(100, 3, ['string']);
    }
}
