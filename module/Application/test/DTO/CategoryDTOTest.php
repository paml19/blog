<?php

declare(strict_types=1);

namespace ApplicationTest\DTO;

use Application\DTO\CategoryDTO;
use ApplicationTest\BadDataTrait;
use ApplicationTest\DataTableButtonsTrait;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

/**
 * Class CategoryDTOTest
 * @package ApplicationTest\DTO
 */
class CategoryDTOTest extends TestCase
{
    use BadDataTrait;
    use DataTableButtonsTrait;

    /**
     *
     */
    public function testHasAttributes(): void
    {
        $this->assertClassHasAttribute('id', CategoryDTO::class);
        $this->assertClassHasAttribute('name', CategoryDTO::class);
        $this->assertClassHasAttribute('active', CategoryDTO::class);
        $this->assertClassHasAttribute('dateAdd', CategoryDTO::class);
        $this->assertClassHasAttribute('postsCount', CategoryDTO::class);
    }

    /**
     * @dataProvider providerForTestReturnValues
     *
     * @param $id
     * @param $name
     * @param $active
     * @param $dateAdd
     * @param $postsCount
     */
    public function testReturnValues($id, $name, $active, $dateAdd, $postsCount): void
    {
        $categoryDTO = new CategoryDTO($id, $name, $active, $dateAdd, $postsCount);

        $this->assertNotNull($categoryDTO->getId());
        $this->assertNotNull($categoryDTO->getDateAdd());
        $this->assertNotNull($categoryDTO->getName());
        $this->assertNotNull($categoryDTO->getPostsCount());
        $this->assertNotNull($categoryDTO->isActive());

        $this->assertIsString($categoryDTO->getId());
        $this->assertIsString($categoryDTO->getName());
        $this->assertIsBool($categoryDTO->isActive());
        $this->assertIsInt($categoryDTO->getPostsCount());
        $this->assertInstanceOf(\DateTime::class, $categoryDTO->getDateAdd());

        $this->dataTableButtons->prepareButtons(Argument::type('string'))
            ->willReturn('return string')
            ->shouldBeCalled();

        $this->assertIsArray($categoryDTO->toArray($this->dataTableButtons->reveal()));
    }

    /**
     * @dataProvider providerForTestExceptions
     *
     * @param $id
     * @param $name
     * @param $active
     * @param $dateAdd
     * @param $postsCount
     * @param $exception
     */
    public function testExceptions($id, $name, $active, $dateAdd, $postsCount, $exception): void
    {
        $this->expectException($exception);

        new CategoryDTO($id, $name, $active, $dateAdd, $postsCount);
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function providerForTestReturnValues(): array
    {
        return [
            ['sample-string', 'sample-string', true, new \DateTime, 123],
            ['sample-string', 'sample-string', false, new \DateTime, 123],
        ];
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function providerForTestExceptions(): array
    {
        return $this->getBadDataWithTypeErrorException(25, 5);
    }
}