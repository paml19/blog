<?php

namespace Application;

use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use paml\EntityRest\Factory\AbstractRestControllerFactory;
use paml\EntityRest\Factory\AbstractRestFormFactory;
use Zend\Mvc\I18n\Router\TranslatorAwareTreeRouteStack;
use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'controllers' => [
        'factories' => [
            Controller\Api\PostController::class => AbstractRestControllerFactory::class,
            Controller\Api\CategoryController::class => AbstractRestControllerFactory::class,
            Controller\Api\CommentController::class => AbstractRestControllerFactory::class,
            Controller\Admin\PostController::class => Factory\Controller\Admin\PostControllerFactory::class,
            Controller\Admin\CategoryController::class => Factory\Controller\Admin\CategoryControllerFactory::class,
            Controller\Admin\CommentController::class => Factory\Controller\Admin\CommentControllerFactory::class,
            Controller\Admin\DashboardController::class => InvokableFactory::class,
            Controller\Admin\TagController::class => Factory\Controller\Admin\TagControllerFactory::class,
            Controller\BlogController::class => Factory\Controller\BlogControllerFactory::class,
            Controller\InitController::class => InvokableFactory::class,
        ],
        'aliases' => [
            'dashboard' => Controller\Admin\DashboardController::class,
        ]
    ],
    'console' => [
        'router' => [
            'routes' => [
                'init-all' => [
                    'options' => [
                        'route' => 'init all',
                        'defaults' => [
                            'controller' => Controller\InitController::class,
                            'action' => 'index',
                        ],
                    ],
                ],
            ],
        ],
    ],
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Entity'],
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver',
                ],
            ],
        ],
        'fixtures' => [
            __NAMESPACE__ . '_fixture' => __DIR__ . '/../src/DataFixtures',
        ],
    ],
    'form_elements' => [
        'factories' => [
            Form\Api\PostForm::class => AbstractRestFormFactory::class,
            Form\Api\CategoryForm::class => AbstractRestFormFactory::class,
            Form\Api\CommentForm::class => AbstractRestFormFactory::class,
            Form\View\PostForm::class => Factory\Form\VewPostFormFactory::class,
            Form\View\CategoryForm::class => Factory\Form\ViewCategoryFormFactory::class,
            Form\CommentForm::class => Factory\Form\CommentFormFactory::class,
            Form\View\TagForm::class => Factory\Form\ViewTagFormFactory::class,
        ],
    ],
    'hydrators' => [
        'factories' => [
            Hydrator\Api\PostHydrator::class => Factory\Hydrator\PostHydratorFactory::class,
            Hydrator\Api\CategoryHydrator::class => Factory\Hydrator\CategoryHydratorFactory::class,
            Hydrator\Api\CommentHydrator::class => Factory\Hydrator\CommentHydratorFactory::class,
            Hydrator\View\PostHydrator::class => Factory\Hydrator\ViewPostHydratorFactory::class,
            Hydrator\View\CategoryHydrator::class => Factory\Hydrator\ViewCategoryHydratorFactory::class,
            Hydrator\View\TagHydrator::class => InvokableFactory::class,
        ],
    ],
    'input_filters' => [
        'factories' => [
            Filter\Api\PostFilter::class => Factory\Filter\PostFilterFactory::class,
            Filter\Api\CommentFilter::class => Factory\Filter\CommentFilterFactory::class,
            Filter\View\PostFilter::class => Factory\Filter\PostFilterFactory::class,
            Filter\CommentFilter::class => Factory\Filter\CommentFilterFactory::class,
            Filter\View\TagFilter::class => InvokableFactory::class,
        ],
    ],
    'navigation' => [
        'default' => [
            [
                'label' => 'Home',
                'route' => 'home',
            ]
        ],
        'admin' => [
            [
                'label' => 'Home',
                'route' => 'home',
                'icon' => 'fa fa-home',
                'resource' => Controller\BlogController::class,
            ],
            [
                'label' => 'Dashboard',
                'route' => 'admin/front',
                'icon' => 'fa fa-dashboard',
                'controller' => 'dashboard',
                'resource' => 'dashboard',
            ],
            [
                'label' => 'Posts',
                'route' => 'admin/front',
                'icon' => 'fa fa-clipboard',
                'controller' => 'post',
                'resource' => 'post',
                'pages' => [
                    [
                        'label' => 'Create',
                        'route' => 'admin/front',
                        'controller' => 'post',
                        'action' => 'add',
                        'resource' => 'post',
                        'privilege' => 'add',
                        'icon' => 'fa fa-plus'
                    ],
                    [
                        'label' => 'List',
                        'route' => 'admin/front',
                        'controller' => 'post',
                        'action' => 'index',
                        'resource' => 'post',
                        'privilege' => 'index',
                        'icon' => 'fa fa-list'
                    ],
                ],
            ],
            [
                'label' => 'Tags',
                'route' => 'admin/front',
                'icon' => 'fa fa-tag',
                'controller' => 'tag',
                'resource' => 'tag',
                'pages' => [
                    [
                        'label' => 'Create',
                        'route' => 'admin/front',
                        'controller' => 'tag',
                        'action' => 'add',
                        'resource' => 'tag',
                        'privilege' => 'add',
                        'icon' => 'fa fa-plus'
                    ],
                    [
                        'label' => 'List',
                        'route' => 'admin/front',
                        'controller' => 'tag',
                        'action' => 'index',
                        'resource' => 'tag',
                        'privilege' => 'index',
                        'icon' => 'fa fa-list'
                    ],
                ],
            ],
            [
                'label' => 'Categories',
                'route' => 'admin/front',
                'icon' => 'fa fa-certificate',
                'controller' => 'category',
                'resource' => 'category',
                'pages' => [
                    [
                        'label' => 'Create',
                        'route' => 'admin/front',
                        'controller' => 'category',
                        'action' => 'add',
                        'resource' => 'category',
                        'privilege' => 'add',
                        'icon' => 'fa fa-plus'
                    ],
                    [
                        'label' => 'List',
                        'route' => 'admin/front',
                        'controller' => 'category',
                        'action' => 'index',
                        'resource' => 'category',
                        'privilege' => 'index',
                        'icon' => 'fa fa-list'
                    ],
                ]
            ],
        ]
    ],
    'router' => [
        'router_class' => TranslatorAwareTreeRouteStack::class,
        'routes' => [
            'home' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/',
                    'defaults' => [
                        'controller' => Controller\BlogController::class,
                        'action' => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'post' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '{post}/:slug',
                            'defaults' => [
                                'controller' => Controller\BlogController::class,
                                'action' => 'post',
                            ],
                        ],
                    ],
                    'category' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '{category}/:slug',
                            'defaults' => [
                                'controller' => Controller\BlogController::class,
                                'action' => 'category',
                            ],
                        ],
                    ],
                ],
            ],
            'admin' => [
                'options' => [
                    'defaults' => [
                        'controller' => Controller\Admin\DashboardController::class,
                    ],
                ],
            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            Listener\CategoryListener::class => Factory\Listener\CategoryListenerFactory::class,
            Listener\CommentListener::class => Factory\Listener\CommentListenerFactory::class,
            'Navigation' => Factory\Helper\NavigationFactory::class,
            'admin' => \paml\Auth\Factory\AbstractAclNavigationFactory::class,
        ],
    ],
//    'view_helpers' => [
//        'factories' => [
//            Form\FormSelect::class => InvokableFactory::class,
//        ],
//        'aliases' => [
//            'formSelect' => Form\FormSelect::class,
//        ]
//    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'layout/admin'           => __DIR__ . '/../view/layout/admin.phtml',
            'layout/auth'           => __DIR__ . '/../view/layout/auth.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
            'partial/navigation'             => __DIR__ . '/../view/layout/partial/navigation.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ]
    ],
];
