<?php

namespace paml\Auth;

use paml\Auth\Listener\AuthListener;
use paml\Auth\Listener\DefaultRoleListener;
use paml\Auth\Listener\HistoryListener;
use paml\Auth\Listener\LayoutListener;
use paml\Auth\Listener\RedirectListener;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Mvc\MvcEvent;
use Zend\Session\SessionManager;

class Module implements ConfigProviderInterface
{
    const AUTH_ROLE_SUPER_ADMIN = 'super-admin';

    const AUTH_USER_HISTORY = 'authUserHistory';

    const AUTH_USER_LOGGED_IN = 'authUserLoggedIn';

    const AUTH_USER_REGISTERED_IN = 'authUserRegisteredIn';

    const AUTH_REDIRECT_AFTER_LOGIN = 'authRedirectAfterLogin';

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function onBootstrap(MvcEvent $event)
    {
        $serviceManager = $event->getApplication()->getServiceManager();
        $eventManager = $event->getTarget()->getEventManager();

        $sessionManager = $serviceManager->get(SessionManager::class);

        /** @var LayoutListener $layoutListener */
        $layoutListener = $serviceManager->get(LayoutListener::class);
        $layoutListener->attach($eventManager, 1);

        /** @var AuthListener $authListener */
        $authListener = $serviceManager->get(AuthListener::class);
        $authListener->attach($eventManager, 100);

        /** @var HistoryListener $historyListener */
        $historyListener = $serviceManager->get(HistoryListener::class);
        $historyListener->attach($eventManager, 102);

        /** @var DefaultRoleListener $defaultRoleListener */
        $defaultRoleListener = $serviceManager->get(DefaultRoleListener::class);
        $defaultRoleListener->attach($eventManager, 103);
    }
}
