<?php

namespace paml\Auth\Entity\Interfaces;

interface Encryptable
{
    public function encryptField(string $value): string;

    public function verifyEncryptableFieldValue(?string $encryptedValue, string $value): bool;
}
