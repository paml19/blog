<?php

namespace paml\Auth\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="paml\Auth\Repository\HistoryRepository")
 * @ORM\Table(name="auth_history")
 * @GEDMO\Loggable
 */
class History
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", unique=true, type="guid", nullable=false)
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="date_add", type="datetime")
     */
    private $dateAdd;

    /**
     * @ORM\Column(name="ip_address", type="string")
     */
    private $ipAddress;

    /**
     * @ORM\ManyToOne(targetEntity="paml\Auth\Entity\User", inversedBy="history")
     * @ORM\JoinColumn(name="user", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\Column(name="register", type="boolean", nullable=false)
     */
    private $register = false;

    /**
     * @ORM\Column(name="login", type="boolean", nullable=false)
     */
    private $login = false;

    /**
     * @ORM\Column(name="success", type="boolean", nullable=false)
     */
    private $success = false;

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): History
    {
        $this->id = $id;
        return $this;
    }

    public function getIpAddress(): string
    {
        return $this->ipAddress;
    }

    public function setIpAddress(string $ipAddress): History
    {
        $this->ipAddress = $ipAddress;
        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): History
    {
        $user->addHistory($this);
        $this->user = $user;
        return $this;
    }

    public function getDateAdd(): \DateTime
    {
        return $this->dateAdd;
    }

    public function getRegister(): bool
    {
        return $this->register;
    }

    public function setRegister(bool $register): History
    {
        $this->register = $register;
        return $this;
    }

    public function getLogin(): bool
    {
        return $this->login;
    }

    public function setLogin(bool $login): History
    {
        $this->login = $login;
        return $this;
    }

    public function getSuccess(): bool
    {
        return $this->success;
    }

    public function setSuccess(bool $success): History
    {
        $this->success = $success;
        return $this;
    }
}
