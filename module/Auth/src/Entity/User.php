<?php

namespace paml\Auth\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use paml\Auth\Entity\Interfaces\Encryptable;
use Doctrine\Common\Collections\ArrayCollection;
use paml\Auth\Entity\Traits\EntityEncryptableTrait;

/**
 * @ORM\Table(name="auth_user")
 * @ORM\Entity(repositoryClass="\paml\Auth\Repository\UserRepository")
 * @GEDMO\Loggable
 * @Gedmo\SoftDeleteable(fieldName="dateDelete", timeAware=false)
 */
class User implements Encryptable
{
    use EntityEncryptableTrait;

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid", unique=true, nullable=false)
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="name", type="string", length=50)
     */
    private $name;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="surname", type="string", length=100)
     */
    private $surname;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="login", type="string", length=50, unique=true, nullable=true)
     */
    private $login;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="email", type="string", length=100, unique=true)
     */
    private $email;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="password", type="string", length=100, nullable=true)
     */
    private $password;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="date_add", type="datetime")
     */
    private $dateAdd;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="date_edit", type="datetime", nullable=true)
     */
    private $dateEdit;

    /**
     * @ORM\Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active = false;

    /**
     * @ORM\Column(name="system_admin", type="boolean", nullable=false)
     */
    private $systemAdmin = false;

    /**
     * @ORM\OneToMany(targetEntity="paml\Auth\Entity\History", mappedBy="user")
     */
    private $history;

    /**
     * @ORM\ManyToMany(targetEntity="paml\Auth\Entity\Role", cascade={"persist"})
     * @ORM\JoinTable(name="auth_users_roles")
     */
    private $roles;

    /**
     * @ORM\ManyToOne(targetEntity="paml\Auth\Entity\Role")
     * @ORM\JoinColumn(name="default_role", referencedColumnName="id")
     */
    private $defaultRole;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="token", type="string", length=100, unique=true, nullable=true)
     */
    private $token;

    public function __construct()
    {
        $this->roles = new ArrayCollection();
        $this->history = new ArrayCollection();
        $this->setToken();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id): User
    {
        $this->id = $id;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): User
    {
        $this->name = $name;
        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(?string $surname): User
    {
        $this->surname = $surname;
        return $this;
    }

    public function getLogin(): ?string
    {
        return $this->login;
    }

    public function setLogin(string $login): User
    {
        $this->login = $login;
        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): User
    {
        $this->email = $email;
        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): User
    {
        $this->password = $this->encryptField($password);
        return $this;
    }

    public function verifyPassword(string $password): bool
    {
        return $this->verifyEncryptableFieldValue($this->getPassword(), $password);
    }

    public function setDateAdd(?\DateTime $dateAdd): self
    {
        $this->dateAdd = $dateAdd;
        return $this;
    }

    public function getDateAdd(): ?\DateTime
    {
        return $this->dateAdd;
    }

    public function getDateEdit(): ?\DateTime
    {
        return $this->dateEdit;
    }

    public function getDateDelete(): ?\DateTime
    {
        return $this->dateDelete;
    }

    public function setDateDelete(? \DateTime $dateDelete): self
    {
        $this->dateDelete = $dateDelete;
        return $this;
    }

    public function isActive()
    {
        return $this->active;
    }

    public function setActive(?bool $active): User
    {
        $this->active = $active;
        return $this;
    }

    public function getHistory(): Collection
    {
        return $this->history;
    }

    public function addHistory(History $authHistory): User
    {
        if ($this->history->contains($authHistory)) {
            return $this;
        }

        $this->history->add($authHistory);
        return $this;
    }

    public function getRoles()
    {
        return $this->roles;
    }

    public function setRoles($roles): User
    {
        $this->roles = $roles;
        return $this;
    }

    public function addRole(Role $role): User
    {
        if ($this->roles->contains($role)) {
            return $this;
        }

        $this->roles->add($role);
        return $this;
    }

    public function removeRole(Role $role): User
    {
        if (! $this->roles->contains($role)) {
            return $this;
        }

        $this->roles->removeElement($role);
        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(): self
    {
        $this->token = $this->generateRandomString(100);
        return $this;
    }

    public function isSystemAdmin(): bool
    {
        return $this->systemAdmin;
    }

    public function setSystemAdmin(bool $systemAdmin): self
    {
        $this->systemAdmin = $systemAdmin;
        return $this;
    }

    public function setDefaultRole(?Role $role): self
    {
        if ($role) {
            $this->defaultRole = $role;
        } else {
            $this->defaultRole = end($this->roles);
        }

        return $this;
    }

    public function getDefaultRole(): ?Role
    {
        return $this->defaultRole;
    }

    private function generateRandomString(int $length = 10): string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function toAuthArray(): array
    {
        /** @var History $lastHistory */
        $lastHistory = $this->getHistory()->last();

        if ($lastHistory) {
            $lastHistory = [
                'id' => $lastHistory->getId(),
                'dateAdd' => $lastHistory->getDateAdd()->format('d.m.Y H:i:s'),
                'register' => $lastHistory->getRegister(),
                'login' => $lastHistory->getLogin(),
                'ip' => $lastHistory->getIpAddress(),
                'success' => $lastHistory->getSuccess()
            ];
        }

        $roles = [];

        /** @var Role $role */
        foreach ($this->getRoles() as $role) {
            if ($role->getActive()) {
                $roles[$role->getId()] = [
                    'id' => $role->getId(),
                    'name' => $role->getName(),
                    'slug' => $role->getSlug(),
                ];
            }
        }

        ksort($roles);

        return [
            'id' => $this->id,
            'name' => $this->name,
            'surname' => $this->surname,
            'email' => $this->email,
            'login' => $this->login,
            'roles' => $roles,
            'history' => $lastHistory,
            'activeRole' => $this->getDefaultRole() ? $roles[$this->getDefaultRole()->getId()] : end($roles),
        ];
    }
}
