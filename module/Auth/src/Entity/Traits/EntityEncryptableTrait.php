<?php

namespace paml\Auth\Entity\Traits;

trait EntityEncryptableTrait
{
    protected $hashOptions = ['cost' => 11];

    public function encryptField(string $value): string
    {
        return password_hash($value, PASSWORD_BCRYPT, $this->hashOptions);
    }

    public function verifyEncryptableFieldValue(?string $encryptedValue, string $value): bool
    {
        return password_verify($value, $encryptedValue);
    }
}
