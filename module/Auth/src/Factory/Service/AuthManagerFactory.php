<?php

namespace paml\Auth\Factory\Service;

use Interop\Container\ContainerInterface;
use paml\Auth\Service\AuthManager;
use Zend\Authentication\AuthenticationService;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\Session\SessionManager;

class AuthManagerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('Config');

        if (! isset($config['auth']['remember_me_time'])) {
            $config['auth']['remember_me_time'] = 60 * 60 * 24;
        }

        return new AuthManager(
            $container->get(AuthenticationService::class),
            $container->get(SessionManager::class),
            $config['auth']['remember_me_time']
        );
    }
}
