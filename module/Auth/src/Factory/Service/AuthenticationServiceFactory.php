<?php

namespace paml\Auth\Factory\Service;

use Zend\Session\SessionManager;
use paml\Auth\Service\AuthAdapter;
use Interop\Container\ContainerInterface;
use Zend\Authentication\AuthenticationService;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\Authentication\Storage\Session as SessionStorage;

class AuthenticationServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new AuthenticationService(
            new SessionStorage(
                'Zend_Auth',
                'session',
                $container->get(SessionManager::class)
            ),
            $container->get(AuthAdapter::class)
        );
    }
}
