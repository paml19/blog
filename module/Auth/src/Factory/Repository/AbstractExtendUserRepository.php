<?php

namespace paml\Auth\Factory\Repository;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

abstract class AbstractExtendUserRepository implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ) {
        $entityManager = $container->get(EntityManager::class);
        $config = $container->get('Config');
        if (! isset($config['auth'])) {
            throw new \Exception('Must implement `auth` root into config');
        }

        if (! isset($config['auth']['default_role'])) {
            $config['auth']['default_role'] = 'guest';
        }

        return [
            'entityManager' => $entityManager,
            'config' => $config,
            'modules' => $container->get('ApplicationConfig')['modules'],
        ];
    }
}
