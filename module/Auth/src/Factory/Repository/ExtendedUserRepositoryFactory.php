<?php

namespace paml\Auth\Factory\Repository;

use Interop\Container\ContainerInterface;
use paml\Auth\Entity\User;
use Zend\ServiceManager\Factory\FactoryInterface;

class ExtendedUserRepositoryFactory extends AbstractExtendUserRepository implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ) {
        $invoke = parent::__invoke($container, $requestedName, $options);

        return new $requestedName(
            $invoke['entityManager'],
            $invoke['entityManager']->getClassMetadata(User::class),
            $invoke['config']['auth']['default_role'],
            $invoke['modules']
        );
    }
}
