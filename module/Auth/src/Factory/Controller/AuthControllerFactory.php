<?php

namespace paml\Auth\Factory\Controller;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use paml\Auth\Controller\AuthController;
use paml\Auth\Entity\Role;
use paml\Auth\Form\AuthForms;
use paml\Auth\Form\Login;
use paml\Auth\Form\Register;
use paml\Auth\Repository\ExtendedUserRepository;
use paml\Auth\Service\AuthManager;
use paml\SessionTranslator\Service\TranslatorService;
use Zend\ServiceManager\Factory\FactoryInterface;

class AuthControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new AuthController(
            $container->get(ExtendedUserRepository::class),
            $container->get(AuthForms::class),
            $container->get(AuthManager::class),
            $container->get('Auth\Session'),
            $container->get(EntityManager::class)->getRepository(Role::class),
            (isset($container->get('Config')['auth']['after_login_redirect']) ? $container->get('Config')['auth']['after_login_redirect'] : null)
        );
        $controller->setTranslator($container->get(TranslatorService::class));

        return $controller;
    }
}
