<?php

namespace paml\Auth\Factory\Controller;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use paml\Auth\Controller\UserController;
use paml\Auth\Entity\Role;
use paml\Auth\Entity\User;
use Zend\ServiceManager\Factory\FactoryInterface;

class UserControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $container->get(EntityManager::class);

        return new UserController(
            $entityManager->getRepository(User::class),
            $entityManager->getRepository(Role::class),
            $container->get('Config')['auth']['admin_password']
        );
    }
}
