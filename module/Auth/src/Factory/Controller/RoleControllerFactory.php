<?php

namespace paml\Auth\Factory\Controller;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use paml\Auth\Controller\RoleController;
use paml\Auth\Entity\Role;
use Zend\ServiceManager\Factory\FactoryInterface;

class RoleControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new RoleController(
            $container->get(EntityManager::class)->getRepository(Role::class),
            $container->get('Config')['roles']
        );
    }
}
