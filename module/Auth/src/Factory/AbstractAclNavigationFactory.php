<?php

namespace paml\Auth\Factory;

use Interop\Container\ContainerInterface;
use paml\Auth\Service\AclService;
use paml\Auth\Service\AuthManager;
use Zend\Navigation\Service\DefaultNavigationFactory;
use Zend\Permissions\Acl\Acl;
use Zend\Permissions\Acl\Resource\GenericResource;
use Zend\Permissions\Acl\Role\GenericRole;
use Zend\ServiceManager\Factory\FactoryInterface;

class AbstractAclNavigationFactory extends DefaultNavigationFactory implements FactoryInterface
{
    /** @var string */
    private $navigationName;

    /** @var Acl */
    private $acl;

    /** @var AuthManager */
    private $authManager;

    /** @var bool */
    private $inheritanceRoles;

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $aclService = $container->get(AclService::class);
        $this->inheritanceRoles = $container->get('Config')['auth']['inheritance_roles'];

        $this->navigationName = $requestedName;
        $this->acl = $aclService->prepareAcl($container->get('Config'));
        $this->authManager = $container->get(AuthManager::class);
        return parent::__invoke($container, $requestedName, $options);
    }

    protected function getName()
    {
        parent::getName();
        return $this->navigationName;
    }

    protected function getPagesFromConfig($config = null)
    {
        $rolesSlugs = [];
        foreach ($this->authManager->getIdentity()['roles'] as $role) {
            $rolesSlugs[] = $role['slug'];
        }

        $roleSlug = $this->authManager->getIdentity()['activeRole']['slug'];
        $navigationConfig = [];

        foreach ($config as $navigationItem) {
            if (isset($navigationItem['pages'])) {
                $aclPages = [];

                foreach ($navigationItem['pages'] as $page) {
                    $guard = true;
                    if ($this->inheritanceRoles) {
                        foreach ($rolesSlugs as $rolesSlug) {
                            if (
                                isset($page['privilege'])
                                &&
                                isset($page['resource'])
                                &&
                                ! $this->acl->isAllowed($rolesSlug, $page['resource'], $page['privilege'])
                            ) {
                                $guard = false;
                                break;
                            } elseif (
                                ! isset($page['privilege'])
                                &&
                                isset($page['resource'])
                                &&
                                ! $this->acl->isAllowed($rolesSlug, $page['resource'])
                            ) {
                                $guard = false;
                                break;
                            }
                        }
                    } else {
                        if (
                            isset($page['privilege'])
                            &&
                            isset($page['resource'])
                            &&
                            ! $this->acl->isAllowed($roleSlug, $page['resource'], $page['privilege'])
                        ) {
                            $guard = false;
                        } elseif (
                            ! isset($page['privilege'])
                            &&
                            isset($page['resource'])
                            &&
                            ! $this->acl->isAllowed($roleSlug, $page['resource'])
                        ) {
                            $guard = false;
                        }
                    }

                    if ($guard) {
                        $aclPages[] = $page;
                    }
                }

                if (! empty($aclPages)) {
                    $navigationItem['pages'] = $aclPages;
                    $navigationConfig[] = $navigationItem;
                }
            } else {
                if ($this->inheritanceRoles) {
                    $guard = false;

                    foreach ($rolesSlugs as $rolesSlug) {
                        if (isset($navigationItem['resource']) && $this->acl->isAllowed($rolesSlug, $navigationItem['resource'])) {
                            $guard = true;
                            break;
                        }
                    }

                    if ($guard) {
                        $navigationConfig[] = $navigationItem;
                    }
                } else {
                    if (isset($navigationItem['resource']) && $this->acl->isAllowed($roleSlug, $navigationItem['resource'])) {
                        $navigationConfig[] = $navigationItem;
                    }
                }
            }
        }

        return parent::getPagesFromConfig($navigationConfig);
    }
}
