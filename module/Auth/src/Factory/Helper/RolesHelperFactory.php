<?php

namespace paml\Auth\Factory\Helper;

use Interop\Container\ContainerInterface;
use paml\Auth\Helper\RolesHelper;
use paml\Auth\Service\AuthManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\View\Renderer\PhpRenderer;

class RolesHelperFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new RolesHelper(
            $container->get(AuthManager::class),
            $container->get(PhpRenderer::class)
        );
    }
}
