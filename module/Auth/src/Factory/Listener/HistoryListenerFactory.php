<?php

namespace paml\Auth\Factory\Listener;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use paml\Auth\Entity\History;
use paml\Auth\Entity\User;
use paml\Auth\Listener\HistoryListener;
use Zend\ServiceManager\Factory\FactoryInterface;

class HistoryListenerFactory implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ): HistoryListener {
        return new HistoryListener(
            $container->get(EntityManager::class)->getRepository(History::class),
            $container->get(EntityManager::class)->getRepository(User::class)
        );
    }
}
