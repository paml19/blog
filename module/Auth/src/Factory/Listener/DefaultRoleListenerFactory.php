<?php

namespace paml\Auth\Factory\Listener;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use paml\Auth\Entity\Role;
use paml\Auth\Entity\User;
use paml\Auth\Listener\DefaultRoleListener;
use Zend\ServiceManager\Factory\FactoryInterface;

class DefaultRoleListenerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new DefaultRoleListener(
            $container->get(EntityManager::class)->getRepository(User::class),
            $container->get(EntityManager::class)->getRepository(Role::class)
        );
    }
}
