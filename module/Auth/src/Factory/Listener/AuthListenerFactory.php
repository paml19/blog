<?php

namespace paml\Auth\Factory\Listener;

use paml\Auth\Service\AclService;
use paml\Auth\Service\AuthManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class AuthListenerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('Config');

        if (! isset($config['acl'])) {
            throw new \Exception('Brak węzła acl w konfiguracji!');
        }

        $aclService = $container->get(AclService::class);
        $acl = $aclService->prepareAcl($config);

        return new $requestedName(
            $acl,
            $container->get(AuthManager::class),
            $container->get('Auth\Session'),
            $config
        );
    }
}
