<?php

namespace paml\Auth\Factory\Listener;

use Interop\Container\ContainerInterface;
use paml\Auth\Listener\LayoutListener;
use Zend\ServiceManager\Factory\FactoryInterface;

class LayoutListenerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new LayoutListener(
            $container->get('Config')['auth']['layouts']
        );
    }
}
