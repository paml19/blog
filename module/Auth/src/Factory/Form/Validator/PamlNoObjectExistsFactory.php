<?php

namespace paml\Auth\Factory\Form\Validator;

use Interop\Container\ContainerInterface;
use paml\Auth\Form\Validator\PamlNoObjectExists;
use Zend\ServiceManager\Factory\FactoryInterface;

class PamlNoObjectExistsFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new PamlNoObjectExists(
            $options,
            $container->get('ApplicationConfig')['modules']
        );
    }
}
