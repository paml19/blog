<?php

namespace paml\Auth\Factory\Form;

use Interop\Container\ContainerInterface;
use paml\Auth\Form\Filter\LoginFilter;
use paml\Auth\Form\Login;
use paml\Auth\Form\LoginForm;
use paml\SessionTranslator\Service\TranslatorService;
use Zend\Hydrator\ClassMethods;
use Zend\ServiceManager\Factory\FactoryInterface;

class LoginFormFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $form = new LoginForm('login-form', []);

        $form->setTranslator($container->get(TranslatorService::class));
        $form->setInputFilter($container->get('InputFilterManager')->get(LoginFilter::class));
        $form->setHydrator($container->get('HydratorManager')->get(ClassMethods::class));

        return $form;
    }
}
