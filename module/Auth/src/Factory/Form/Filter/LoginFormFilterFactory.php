<?php

namespace paml\Auth\Factory\Form\Filter;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use paml\Auth\Entity\User;
use paml\Auth\Form\Filter\LoginFilter;
use Zend\ServiceManager\Factory\FactoryInterface;

class LoginFormFilterFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new LoginFilter(
            $container->get(EntityManager::class)->getRepository(User::class)
        );
    }
}
