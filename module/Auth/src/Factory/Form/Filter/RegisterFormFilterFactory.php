<?php

namespace paml\Auth\Factory\Form\Filter;

use Interop\Container\ContainerInterface;
use paml\Auth\Form\Filter\RegisterFilter;
use paml\Auth\Repository\ExtendedUserRepository;
use Zend\ServiceManager\Factory\FactoryInterface;

class RegisterFormFilterFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new RegisterFilter(
            $container->get(ExtendedUserRepository::class)
        );
    }
}
