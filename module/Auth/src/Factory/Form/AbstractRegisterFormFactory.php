<?php

namespace paml\Auth\Factory\Form;

use Interop\Container\ContainerInterface;
use paml\Auth\Entity\User;
use paml\Auth\Form\Filter\RegisterFilter;
use paml\Auth\Form\RegisterForm;
use paml\SessionTranslator\Service\TranslatorService;
use Zend\Hydrator\ClassMethods;
use Zend\ServiceManager\Factory\FactoryInterface;

class AbstractRegisterFormFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $form = new $requestedName('register', []);

        $form->setTranslator($container->get(TranslatorService::class));
        $form->setHydrator($container->get('HydratorManager')->get(ClassMethods::class));
        $form->setInputFilter($container->get('InputFilterManager')->get(RegisterFilter::class));
        $form->setObject(new User());

        return $form;
    }
}
