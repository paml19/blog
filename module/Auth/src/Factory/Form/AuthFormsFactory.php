<?php

namespace paml\Auth\Factory\Form;

use Interop\Container\ContainerInterface;
use paml\Auth\Form\AuthForms;
use paml\Auth\Form\LoginForm;
use paml\Auth\Form\RegisterForm;
use Zend\ServiceManager\Factory\FactoryInterface;

class AuthFormsFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new AuthForms(
            $container->get('FormElementManager')->get(RegisterForm::class),
            $container->get('FormElementManager')->get(LoginForm::class)
        );
    }
}
