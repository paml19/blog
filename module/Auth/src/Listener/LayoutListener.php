<?php

namespace paml\Auth\Listener;

use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\ListenerAggregateTrait;
use Zend\Mvc\Application;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\Mvc\MvcEvent;
use Zend\Console\Request as ConsoleRequest;

class LayoutListener implements ListenerAggregateInterface
{
    use ListenerAggregateTrait;

    private $mainLayout;

    private $templates;

    private $excluded;

    public function __construct(array $layouts)
    {
        $this->mainLayout = $layouts['main_layout'];
        $this->templates = $layouts['templates'];
        $this->excluded = isset($layouts['excluded']) ? $layouts['excluded'] : [];
    }

    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $sharedEventManager = $events->getSharedManager();

        $sharedEventManager->attach(
            AbstractActionController::class,
            MvcEvent::EVENT_DISPATCH,
            [
                $this,
                'onDispatch',
            ],
            $priority
        );

        $sharedEventManager->attach(
            Application::class,
            MvcEvent::EVENT_RENDER,
            [
                $this,
                'onRender',
            ],
            $priority
        );
    }

    public function onDispatch(MvcEvent $mvcEvent)
    {
        if ($this->controllerCheck($mvcEvent)) {
            return true;
        }
        if ($this->mainLayout) {
            $viewModel = $mvcEvent->getViewModel();
            $viewModel->setTemplate($this->mainLayout);
        }
    }

    public function onRender(MvcEvent $mvcEvent)
    {
        if ($this->controllerCheck($mvcEvent)) {
            return true;
        }

        if ($mvcEvent->getRouteMatch()) {
            $actionName = $mvcEvent->getRouteMatch()->getParam('action', null);
            $actionName = str_replace('-', '', lcfirst(ucwords($actionName, '-')));

            if ($this->templates[$actionName]) {
                $mvcEvent->getResult()->setTemplate($this->templates[$actionName]);
            }
        }
    }

    private function controllerCheck(MvcEvent $mvcEvent): bool
    {
        if ($mvcEvent->getRequest() instanceof ConsoleRequest) {
            return true;
        }

        if ($mvcEvent->getRouteMatch()) {
            $controllerName = $mvcEvent->getRouteMatch()->getParam('controller', null);

            if (is_subclass_of($controllerName, AbstractRestfulController::class)) {
                return true;
            }

            if (
                in_array($controllerName, $this->excluded)
                ||
                ! in_array('Auth', explode('\\', $controllerName))
                ||
                ! strstr($controllerName, 'Auth') !== false
            ) {
                return true;
            }
        }

        return false;
    }
}
