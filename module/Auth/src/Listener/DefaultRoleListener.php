<?php

namespace paml\Auth\Listener;

use paml\Auth\Entity\User;
use paml\Auth\Module;
use paml\Auth\Repository\RoleRepository;
use paml\Auth\Repository\UserRepository;
use Zend\EventManager\EventInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\ListenerAggregateTrait;
use Zend\Mvc\Controller\AbstractController;

class DefaultRoleListener implements ListenerAggregateInterface
{
    use ListenerAggregateTrait;

    private $userRepository;

    private $roleRepository;

    public function __construct(UserRepository $userRepository, RoleRepository $roleRepository)
    {
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
    }

    public function attach(EventManagerInterface $events, $priority = 1): void
    {
        $sharedEventManager = $events->getSharedManager();

        $sharedEventManager->attach(
            AbstractController::class,
            Module::AUTH_USER_LOGGED_IN,
            [
                $this,
                'saveDefaultRole'
            ],
            $priority
        );
    }

    public function saveDefaultRole(EventInterface $event)
    {
        $identity = $event->getParam('identity');

        /** @var User $user */
        $user = $this->userRepository->find($identity['id']);
        $role = $this->roleRepository->find(end($identity['roles'])['id']);

        if (! $user->getDefaultRole()) {
            $user->setDefaultRole($role);
            $this->userRepository->save($user);
        }
    }
}
