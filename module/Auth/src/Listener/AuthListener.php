<?php

namespace paml\Auth\Listener;

use paml\Auth\Exception\AuthorizationException;
use paml\Auth\Service\AuthManager;
use Zend\Mvc\Controller\AbstractController;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\Mvc\MvcEvent;
use Zend\Permissions\Acl\Acl;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateTrait;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\Session\Container;
use Zend\Console\Request as ConsoleRequest;

class AuthListener implements ListenerAggregateInterface
{
    use ListenerAggregateTrait;

    private $acl;

    private $authManager;

    private $sessionContainer;

    private $config;

    public function __construct(
        Acl $acl,
        AuthManager $authManager,
        Container $sessionContainer,
        array $config
    ) {
        $this->acl = $acl;
        $this->authManager = $authManager;
        $this->sessionContainer = $sessionContainer;
        $this->config = $config;
    }

    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $sharedEventManager = $events->getSharedManager();

        $sharedEventManager->attach(
            AbstractController::class,
            MvcEvent::EVENT_DISPATCH,
            [
                $this,
                'onDispatch',
            ],
            $priority
        );
    }

    public function onDispatch(MvcEvent $mvcEvent)
    {
        if ($mvcEvent->getRequest() instanceof ConsoleRequest) {
            return true;
        }

        $controllerName = $mvcEvent->getRouteMatch()->getParam('controller', null);

        if ($this->checkAuth($mvcEvent)) {
            return true;
        }

        if ($mvcEvent->getTarget()->params()->fromHeader('Authorization')) {
            return true;
        }

        if (! $this->authManager->hasIdentity()) {
            if ($this->acl->isAllowed(null, $controllerName)) {
                return true;
            }

            $this->sessionContainer->route = $mvcEvent->getRequest()->getUri();

            return $mvcEvent->getTarget()->plugin('redirect')->toRoute('auth/login');
        }

        if (! $this->acl->hasResource($controllerName)) {
            $this->notPermitted();
        }

        $actionName = $mvcEvent->getRouteMatch()->getParam('action', null);
        $actionName = str_replace('-', '', lcfirst(ucwords($actionName, '-')));

        if (isset($this->config['auth']['inheritance_roles']) && $this->config['auth']['inheritance_roles']) {
            $roles = $this->authManager->getIdentity()['roles'];

            $accessGuard = false;

            foreach ($roles as $role) {
                if (! $this->acl->hasRole($role['slug'])) {
                    continue;
                }

                if ($this->acl->isAllowed($role['slug'], $controllerName, $actionName)) {
                    $accessGuard = true;
                    break;
                }
            }

            if (!$accessGuard) {
                $this->notPermitted();
            }
        } else {
            $activeRole = $this->authManager->getIdentity()['activeRole'];

            if (! $this->acl->isAllowed($activeRole['slug'], $controllerName, $actionName)) {
                $this->notPermitted();
            }
        }
    }

    private function notPermitted()
    {
        $systemAdminMail = null;

        if (isset($this->config['auth']['system_admin_mail'])) {
            $systemAdminMail = $this->config['auth']['system_admin_mail'];
        }

        throw new AuthorizationException(
            'Skontaktuj się z administratorem systemu ' . $systemAdminMail,
            5002
        );
    }

    protected function checkAuth(MvcEvent $mvcEvent): bool
    {
        $controllerName = $mvcEvent->getRouteMatch()->getParam('controller', null);

        if (
            in_array('Auth', explode('\\', $controllerName))
            ||
            strstr($controllerName, 'Auth') !== false
            ||
            $mvcEvent->getTarget() instanceof AbstractRestfulController
        ) {
            return true;
        }

        return false;
    }
}
