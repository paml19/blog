<?php

namespace paml\Auth\Listener;

use paml\Auth\Module;
use Zend\EventManager\EventInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\ListenerAggregateTrait;
use Zend\Mvc\Controller\AbstractActionController;

class RedirectListener implements ListenerAggregateInterface
{
    use ListenerAggregateTrait;

    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $sharedEventManager = $events->getSharedManager();

        $sharedEventManager->attach(
            AbstractActionController::class,
            Module::AUTH_REDIRECT_AFTER_LOGIN,
            [
                $this,
                'onDispatch',
            ],
            $priority
        );
    }

    public function onDispatch(EventInterface $mvcEvent)
    {
        $activeRole = $mvcEvent->getParam('activeRole');
        $route = $mvcEvent->getParam('route');
        $route->setPath('/' . $activeRole['slug']);
    }
}
