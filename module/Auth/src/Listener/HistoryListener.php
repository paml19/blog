<?php

namespace paml\Auth\Listener;

use paml\Auth\Entity\History;
use paml\Auth\Module;
use paml\Auth\Repository\HistoryRepository;
use paml\Auth\Repository\UserRepository;
use Zend\EventManager\EventInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\ListenerAggregateTrait;
use Zend\Mvc\Controller\AbstractController;

class HistoryListener implements ListenerAggregateInterface
{
    const HISTORY_TYPE_REGISTER = 'register';

    const HISTORY_TYPE_LOGIN = 'login';

    use ListenerAggregateTrait;

    private $historyRepository;

    private $userRepository;

    public function __construct(
        HistoryRepository $historyRepository,
        UserRepository $userRepository
    ) {
        $this->historyRepository = $historyRepository;
        $this->userRepository = $userRepository;
    }

    public function attach(EventManagerInterface $events, $priority = 1): void
    {
        $sharedEventManager = $events->getSharedManager();

        $sharedEventManager->attach(
            AbstractController::class,
            Module::AUTH_USER_HISTORY,
            [
                $this,
                'saveHistory'
            ],
            $priority
        );
    }

    public function saveHistory(EventInterface $event): void
    {
        $userId = $event->getParam('userId', null);
        $userLogin = $event->getParam('userLogin', null);
        $success = $event->getParam('success', false);
        $type = $event->getParam('type');

        if ($userId) {
            $user = $this->userRepository->find($userId);
        } else {
            $user = $this->userRepository->findOneBy(['login' => $userLogin]);
            if (! $user) {
                $user = $this->userRepository->findOneBy(['email' => $userLogin]);
            }
        }

        if ($user) {
            $history = (new History())
                ->setUser($user)
                ->setSuccess($success)
                ->setIpAddress($this->getIp($event));

            switch ($type) {
                case self::HISTORY_TYPE_REGISTER:
                    $history->setRegister(true);
                    break;
                case self::HISTORY_TYPE_LOGIN:
                    $history->setLogin(true);
                    break;
            }

            $this->historyRepository->save($history);
        }
    }

    private function getIp(EventInterface $event): string
    {
        $server = $event->getTarget()->getRequest()->getServer();

        if (array_key_exists('HTTP_X_FORWARDED_FOR', $server) && ! empty($server->get('HTTP_X_FORWARDED_FOR'))) {
            if (strpos($server->get('HTTP_X_FORWARDED_FOR'), ',') > 0) {
                $addr = explode(",", $server->get('HTTP_X_FORWARDED_FOR'));
                return trim($addr[0]);
            } else {
                return $server->get('HTTP_X_FORWARDED_FOR');
            }
        } else {
            return $server->get('REMOTE_ADDR');
        }
    }
}
