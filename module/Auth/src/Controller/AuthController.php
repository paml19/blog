<?php

namespace paml\Auth\Controller;

use paml\Auth\Entity\Role;
use paml\Auth\Entity\User;
use paml\Auth\Exception\AuthorizationException;
use paml\Auth\Form\AuthForms;
use paml\Auth\Form\Constants\Login;
use paml\Auth\Listener\HistoryListener;
use paml\Auth\Module;
use paml\Auth\Repository\ExtendedUserRepository;
use paml\Auth\Repository\RoleRepository;
use paml\Auth\Service\AuthManager;
use paml\SessionTranslator\Traits\TranslatorTrait;
use Zend\Authentication\Result;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\Plugin\FlashMessenger\FlashMessenger;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;

class AuthController extends AbstractActionController
{
    private $userRepository;

    private $registerForm;

    private $loginForm;

    private $authManager;

    private $sessionContainer;

    private $authForms;

    private $roleRepository;

    private $afterLoginRedirect;

    use TranslatorTrait;

    public function __construct(
        ExtendedUserRepository $userRepository,
        AuthForms $authForms,
        AuthManager $authManager,
        Container $sessionContainer,
        RoleRepository $roleRepository,
        array $afterLoginRedirect
    ) {
        $this->userRepository = $userRepository;
        $this->registerForm = $authForms->getRegisterForm();
        $this->loginForm = $authForms->getLoginForm();
        $this->authManager = $authManager;
        $this->sessionContainer = $sessionContainer;
        $this->authForms = $authForms;
        $this->roleRepository = $roleRepository;
        $this->afterLoginRedirect = $afterLoginRedirect;
    }

    public function loginAction()
    {
        $changeRole = $this->params()->fromQuery('changeRole', false);
        $request = $this->getRequest();

        if ($changeRole) {
            $this->login(null, null, true, null, $changeRole);
        }

        if ($request->isPost()) {
            $this->loginForm->setData($request->getPost());
            if ($this->loginForm->isValid()) {
                $data = $this->loginForm->getData();
                $this->login(
                    $data[Login::LOGIN],
                    $data[Login::PASSWORD],
                    $data[Login::REMEMBER_ME] == '' ? true : false,
                    null,
                    null
                );
            }
        }

        return new ViewModel([
            'form' => $this->loginForm
        ]);
    }

    private function login(
        ?string $login,
        ?string $password,
        ?bool $rememberMe,
        ?object $authAdapter,
        ?string $userId
    ) {
        $result = $this->authManager->login(
            $login,
            $password,
            (bool) $rememberMe,
            $authAdapter,
            $userId
        );

        if ($result->getCode() == Result::SUCCESS) {
            $this->getEventManager()->trigger(
                Module::AUTH_USER_LOGGED_IN,
                $this,
                [
                    'userId' => $this->authManager->getIdentity()['id'],
                    'identity' => $this->authManager->getIdentity()
                ]
            );

            $this->getEventManager()->trigger(
                Module::AUTH_USER_HISTORY,
                $this,
                [
                    'userId' => $this->authManager->getIdentity()['id'],
                    'type' => HistoryListener::HISTORY_TYPE_LOGIN,
                    'success' => true,
                ]
            );

            $this->flashMessenger()->addMessage(sprintf(
                '<strong>' . $this->translate('Last login') . '</strong><br>' . $this->translate('Date') . ': %s<br>IP: %s',
                $this->authManager->getIdentity()['history']['dateAdd'],
                $this->authManager->getIdentity()['history']['ip']
            ), FlashMessenger::NAMESPACE_DEFAULT);

            if (isset($this->sessionContainer->route)) {
                $this->getEventManager()->trigger(
                    Module::AUTH_REDIRECT_AFTER_LOGIN,
                    $this,
                    [
                        'activeRole' => $this->authManager->getIdentity()['activeRole'],
                        'route' => $this->sessionContainer->route,
                    ]
                );

                return $this->redirect()->toUrl($this->sessionContainer->route);
            }

            if (isset($this->afterLoginRedirect)) {
                return $this->redirect()->toRoute($this->afterLoginRedirect['route'], $this->afterLoginRedirect['options']);
            }

            return $this->redirect()->toRoute('home');
        } else {
            $this->getEventManager()->trigger(
                Module::AUTH_USER_HISTORY,
                $this,
                [
                    'userLogin' => $login,
                    'type' => HistoryListener::HISTORY_TYPE_LOGIN
                ]
            );

            $messages = [];
            foreach ($result->getMessages() as $message) {
                $messages[Login::LOGIN][] = $message;
            }
            $this->loginForm->setMessages($messages);
        }
    }

    public function logoutAction()
    {
        $changeRole = $this->params()->fromQuery('changeRole', false);
        unset($this->sessionContainer->route);
        $this->authManager->logout();

        $this->flashMessenger()->addMessage($this->translate('Successfull logout'), FlashMessenger::NAMESPACE_INFO);

        if ($changeRole) {
            return $this->redirect()->toRoute('auth/login', [], ['query' => ['changeRole' => $changeRole]]);
        }

        return $this->redirect()->toRoute('auth/login');
    }

    public function registerAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $this->registerForm->setData($request->getPost());
            if ($this->registerForm->isValid()) {
                /** @var User $user */
                $user = $this->registerForm->getData();
                $user->setToken();
                $this->userRepository->saveWithDefaultRole($user);

                $this->getEventManager()->trigger(
                    Module::AUTH_USER_REGISTERED_IN,
                    $this,
                    ['user' => $user]
                );

                $this->getEventManager()->trigger(
                    Module::AUTH_USER_HISTORY,
                    $this,
                    [
                        'userId' => $user->getId(),
                        'type' => HistoryListener::HISTORY_TYPE_REGISTER,
                        'success' => true
                    ]
                );

                return $this->redirect()->toRoute('auth/login');
            }
        }

        return new ViewModel([
            'form' => $this->registerForm
        ]);
    }

    public function verifyAction()
    {
        $token = $this->params()->fromQuery('token');

        /** @var User $user */
        $user = $this->userRepository->findOneBy(['token' => $token]);
        if (! $user) {
            $this->flashMessenger()->addMessage($this->translate('User not exists'), FlashMessenger::NAMESPACE_ERROR);
            return $this->redirect()->toRoute('auth/login');
        }

        $user->setActive(true)
            ->setToken();

        $this->userRepository->save($user);

        $this->flashMessenger()->addMessage($this->translate('User activated'), FlashMessenger::NAMESPACE_SUCCESS);
        return $this->redirect()->toRoute('auth/login');
    }

    public function changeRoleAction()
    {
        $roleId = $this->params()->fromRoute('id');

        /** @var Role $role */
        $role = $this->roleRepository->find($roleId);
        /** @var User $user */
        $user = $this->userRepository->find($this->authManager->getIdentity()['id']);

        $user->setDefaultRole($role);
        $this->userRepository->save($user);

        return $this->redirect()->toRoute('auth/logout', [], ['query' => ['changeRole' => $user->getId()]]);
    }
}
