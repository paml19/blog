<?php

namespace paml\Auth\Controller;

use paml\Auth\Entity\Role;
use paml\Auth\Repository\RoleRepository;
use Zend\Mvc\Console\Controller\AbstractConsoleController;

class RoleController extends AbstractConsoleController
{
    private $roleRepository;

    private $roles;

    public function __construct(RoleRepository $roleRepository, array $roles)
    {
        $this->roleRepository = $roleRepository;
        $this->roles = $roles;
    }

    public function initRolesAction()
    {
        $superAdmin = $this->roleRepository->findOneBy(['slug' => 'super-admin']);
        if (! $superAdmin) {
            $superAdmin = (new Role)
                ->setName('Super admin')
                ->setSlug('super-admin')
                ->setActive(true)
                ->setSystemAdmin(true);

            $this->roleRepository->save($superAdmin);
        }

        foreach ($this->roles as $role) {
            if (! $this->roleRepository->findOneBy(['slug' => $role['slug']])) {
                $role = (new Role())
                    ->setName($role['name'])
                    ->setSlug($role['slug'])
                    ->setActive(true);

                $this->roleRepository->save($role);
            }
        }
    }
}
