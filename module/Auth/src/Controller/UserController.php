<?php

namespace paml\Auth\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use paml\Auth\Entity\Role;
use paml\Auth\Entity\User;
use paml\Auth\Repository\RoleRepository;
use paml\Auth\Repository\UserRepository;
use Zend\Mvc\Console\Controller\AbstractConsoleController;

class UserController extends AbstractConsoleController
{
    private $userRepository;

    private $roleRepository;

    private $adminPassword;

    public function __construct(UserRepository $userRepository, RoleRepository $roleRepository, string $adminPassword)
    {
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
        $this->adminPassword = $adminPassword;
    }

    public function initAdminAction()
    {
        $adminRole = $this->roleRepository->findOneBy(['slug' => 'super-admin']);
        if (! $adminRole) {
            $adminRole = (new Role)
                ->setName('Super admin')
                ->setSlug('super-admin')
                ->setActive(true)
                ->setSystemAdmin(true);

            $this->roleRepository->save($adminRole);
        }

        $roles = new ArrayCollection($this->roleRepository->findAll());

        $user = $this->userRepository->findOneBy(['email' => 'admin@' . \Application\Module::APP_NAME . '.pl']);
        if (! $user) {
            $user = new User;
        }

        $user->setName('')
            ->setSurname('')
            ->setEmail('admin@' . \Application\Module::APP_NAME . '.pl')
            ->setLogin('admin')
            ->setPassword($this->adminPassword)
            ->setDefaultRole($adminRole)
            ->setRoles($roles)
            ->setActive(true)
            ->setSystemAdmin(true);

        $this->userRepository->save($user);
    }
}
