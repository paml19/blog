<?php

namespace paml\Auth\Repository;

use Doctrine\ORM\EntityRepository;
use paml\Auth\Entity\Role;

class RoleRepository extends EntityRepository
{
    public function save(Role $role): void
    {
        $this->getEntityManager()->persist($role);
        $this->getEntityManager()->flush();
    }

    public function merge(Role $role): void
    {
        $this->getEntityManager()->persist($role);
        $this->getEntityManager()->flush();
    }

    public function delete(Role $role): void
    {
        $this->getEntityManager()->remove($role);
        $this->getEntityManager()->flush();
    }

    public function findRoles(array $roleSlugs): array
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder->select('r')
            ->from($this->getEntityName(), 'r');

        foreach ($roleSlugs as $roleSlug) {
            $queryBuilder->orWhere($queryBuilder->expr()->like('r.slug', '\'%' . $roleSlug . '%\''));
        }

        return $queryBuilder->getQuery()->getResult();
    }
}
