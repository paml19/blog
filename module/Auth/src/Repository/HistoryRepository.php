<?php

namespace paml\Auth\Repository;

use Doctrine\ORM\EntityRepository;
use paml\Auth\Entity\History;

class HistoryRepository extends EntityRepository
{
    public function save(History $history): void
    {
        $this->getEntityManager()->persist($history);
        $this->getEntityManager()->flush($history);
    }
}
