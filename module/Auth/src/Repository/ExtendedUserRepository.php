<?php

namespace paml\Auth\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping;
use Doctrine\ORM\Query\Expr\Join;
use paml\Auth\Entity\Role;
use paml\Auth\Entity\User;

class ExtendedUserRepository extends UserRepository
{
    private $defaultRole;

    private $modules;

    public function __construct(
        $em,
        Mapping\ClassMetadata $class,
        string $defaultRole,
        array $modules
    ) {
        parent::__construct($em, $class);
        $this->defaultRole = $defaultRole;
        $this->modules = $modules;
    }

    public function saveWithDefaultRole(User $user): void
    {
        $role = $this->getEntityManager()->getRepository(Role::class)->findOneBy([
            'name' => $this->defaultRole
        ]);

        if (! $role) {
            $role = (new Role())
                ->setName(ucfirst($this->defaultRole))
                ->setSlug($this->defaultRole)
                ->setActive(true);

            $this->getEntityManager()->persist($role);
            $this->getEntityManager()->flush($role);
        }

        $user->addRole($role);

        if ($old = $this->getEntityManager()->getRepository(User::class)->findOneBy(['email' => $user->getEmail()])) {
            $user->setId($old->getId());
            $user->setDateAdd($old->getDateAdd());
            $this->merge($user);
        } else {
            $this->save($user);
        }
    }

    public function findUserWithExtensions(array $finder): ?array
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $queryBuilder->select('u')
            ->from($this->getEntityName(), 'u')
            ->where($queryBuilder->expr()->eq('u.' . key($finder), ':finder'))
            ->setParameter(':finder', current($finder));

        if (in_array('paml\Auth\Google', $this->modules) !== false) {
            $queryBuilder->leftJoin(
                'paml\Auth\Google\Entity\User',
                'gu',
                Join::WITH,
                $queryBuilder->expr()->eq('u.id', 'gu.user')
            );
            $queryBuilder->addSelect('gu');
        }

        if (in_array('paml\Auth\Facebook', $this->modules) !== false) {
            $queryBuilder->leftJoin(
                'paml\Auth\Facebook\Entity\User',
                'fu',
                Join::WITH,
                $queryBuilder->expr()->eq('u.id', 'fu.user')
            );
            $queryBuilder->addSelect('fu');
        }

        return $queryBuilder->getQuery()->getResult();
    }
}
