<?php

namespace paml\Auth\Repository;

use Doctrine\ORM\EntityRepository;
use paml\Auth\Entity\User;

class UserRepository extends EntityRepository
{
    public function save(User $user): void
    {
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush($user);
    }

    public function merge(User $user): void
    {
        $this->getEntityManager()->merge($user);
        $this->getEntityManager()->flush();
    }

    public function update(): void
    {
        $this->getEntityManager()->flush();
    }

    public function delete(User $user): void
    {
        $this->getEntityManager()->remove($user);
        $this->getEntityManager()->flush();
    }
}
