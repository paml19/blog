<?php

namespace paml\Auth\Helper;

use paml\Auth\Service\AuthManager;
use Zend\View\Helper\AbstractHelper;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;

class RolesHelper extends AbstractHelper
{
    private $authManager;

    private $phpRenderer;

    public function __construct(AuthManager $authManager, PhpRenderer $phpRenderer)
    {
        $this->authManager = $authManager;
        $this->phpRenderer = $phpRenderer;
    }

    public function __invoke()
    {
        $roles = $this->authManager->getIdentity()['roles'];
        $activeRole = $this->authManager->getIdentity()['activeRole'];

        unset($roles[$activeRole['id']]);

        $view = (new ViewModel)
            ->setVariable('roles', $roles)
            ->setTemplate('auth/roles');

        return $this->phpRenderer->render($view);
    }
}
