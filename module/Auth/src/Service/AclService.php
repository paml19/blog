<?php

namespace paml\Auth\Service;

use paml\Auth\Operation\Controller\UserController;
use Zend\Permissions\Acl\Acl;
use Zend\Permissions\Acl\Resource\GenericResource;
use Zend\Permissions\Acl\Role\GenericRole;

class AclService
{
    public function prepareAcl($config): Acl
    {
        $acl = new Acl();

        if ($config['acl']['resources']['controllers_from_factories']) {
            foreach ($config['controllers']['factories'] as $controller => $factory) {
                if (! $acl->hasResource($controller)) {
                    $acl->addResource(new GenericResource($controller));
                }
            }

            if (isset($config['controllers']['aliases'])) {
                foreach ($config['controllers']['aliases'] as $alias => $controller) {
                    if (! $acl->hasResource($alias)) {
                        $acl->addResource(new GenericResource($alias));
                    }
                }
            }
        }

        if (isset($config['acl']['resources']['controllers'])) {
            foreach ($config['acl']['resources']['controllers'] as $controller) {
                if (! $acl->hasResource($controller)) {
                    $acl->addResource($controller);
                }
            }
        }

        foreach ($config['acl']['privileges'] as $role => $controllers) {
            $acl->addRole(new GenericRole($role));

            if ($controllers == '*' and ! is_array($controllers)) {
                $acl->allow($role);
                continue;
            }

            if (key($controllers) == '*' and is_array($controllers['*'])) {
                $acl->allow($role);
                $controllersToDeny = $controllers['*'];
                foreach ($controllersToDeny as $key => $controllerToDenyResources) {
                    if (! $acl->hasResource($key)) {
                        $acl->addResource($key);
                    }

                    if ($controllerToDenyResources == '*') {
                        $acl->deny($role, $key);
                    }

                    if (! empty($controllerToDenyResources)) {
                        $acl->deny($role, $key, $controllerToDenyResources);
                    }
                }
                continue;
            }

            foreach ((array) $controllers as $controller => $resources) {
                if (! $acl->hasResource($controller)) {
                    continue;
                }

                if ($resources == '*') {
                    $acl->allow($role, $controller);
                    continue;
                }

                if (! empty($resources)) {
                    $acl->allow($role, $controller, $resources);
                }
            }
        }

        if (isset($config['acl']['resources']['excluded'])) {
            foreach ($config['acl']['resources']['excluded'] as $excluded) {
                if (! $acl->hasResource($excluded)) {
                    $acl->addResource($excluded);
                }
                $acl->allow(null, $excluded);
            }
        }

        return $acl;
    }
}