<?php

namespace paml\Auth\Service;

use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Result;
use Zend\Session\SessionManager;

class AuthManager
{
    private $authenticationService;

    private $sessionManager;

    private $rememberMeTime;

    public function __construct(
        AuthenticationService $authenticationService,
        SessionManager $sessionManager,
        int $rememberMeTime
    ) {
        $this->authenticationService = $authenticationService;
        $this->sessionManager = $sessionManager;
        $this->rememberMeTime = $rememberMeTime;
    }

    public function login(
        ?string $login,
        ?string $password,
        ?bool $rememberMe,
        ?object $authAdapter,
        ?string $userId = null
    ): Result {
        if ($authAdapter) {
            $this->authenticationService->setAdapter($authAdapter);
        }

        $this->authenticationService->getAdapter()->setLogin($login);
        $this->authenticationService->getAdapter()->setPassword($password);
        $this->authenticationService->getAdapter()->setUserId($userId);

        $result = $this->authenticationService->authenticate();

        if ($result->getCode() == Result::SUCCESS) {
            if ($rememberMe) {
                $this->sessionManager->rememberMe($this->rememberMeTime);
            }
            return $result;
        }

        return $result;
    }

    public function logout(): void
    {
        $this->authenticationService->clearIdentity();
        $this->sessionManager->destroy();
    }

    public function getIdentity(): ?array
    {
        return $this->authenticationService->getIdentity();
    }

    public function hasIdentity(): bool
    {
        return $this->authenticationService->hasIdentity();
    }
}
