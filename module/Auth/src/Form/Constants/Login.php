<?php

namespace paml\Auth\Form\Constants;

class Login
{
    const LOGIN = 'login';

    const PASSWORD = 'password';

    const REMEMBER_ME = 'rememberMe';

    const REMEMBER_ME_CHECKED = true;

    const REMEMBER_ME_UNCHECKED = false;

    const CSRF = 'csrf';

    const SUBMIT = 'submit';
}
