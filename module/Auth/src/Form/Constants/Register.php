<?php

namespace paml\Auth\Form\Constants;

class Register
{
    const NAME = 'name';

    const SURNAME = 'surname';

    const LOGIN = 'login';

    const EMAIL = 'email';

    const PASSWORD = 'password';

    const RE_PASSWORD = 'rePassword';

    const CSRF = 'csrf';

    const SUBMIT = 'submit';
}
