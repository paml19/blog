<?php

namespace paml\Auth\Form;

use paml\Auth\Form\Constants\Register;
use paml\SessionTranslator\Interfaces\TranslatorInterface;
use paml\SessionTranslator\Traits\TranslatorTrait;
use Zend\Form\Element\Csrf;
use Zend\Form\Element\Email;
use Zend\Form\Element\Password;
use Zend\Form\Element\Submit;
use Zend\Form\Element\Text;
use Zend\Form\Form;

class RegisterForm extends Form implements TranslatorInterface
{
    use TranslatorTrait;

    public function init()
    {
        $this->add([
            'name' => Register::NAME,
            'type' => Text::class,
            'options' => [
                'label' => $this->translate('Name')
            ],
            'attributes' => [
                'class' => 'form-control input-lg inverse-mode',
                'id' => 'js-' . $this->getAttribute('name') . '-' . Register::NAME,
                'required' => false,
            ],
        ]);

        $this->add([
            'name' => Register::SURNAME,
            'type' => Text::class,
            'options' => [
                'label' => $this->translate('Surname'),
            ],
            'attributes' => [
                'class' => 'form-control input-lg inverse-mode',
                'id' => 'js-' . $this->getAttribute('name') . '-' . Register::SURNAME,
                'required' => false,
            ],
        ]);

        $this->add([
            'name' => Register::LOGIN,
            'type' => Text::class,
            'options' => [
                'label' => $this->translate('Login'),
            ],
            'attributes' => [
                'class' => 'form-control input-lg inverse-mode',
                'id' => 'js-' . $this->getAttribute('name') . '-' . Register::LOGIN,
                'required' => true,
            ],
        ]);

        $this->add([
            'name' => Register::PASSWORD,
            'type' => Password::class,
            'options' => [
                'label' => $this->translate('Password'),
            ],
            'attributes' => [
                'class' => 'form-control input-lg inverse-mode',
                'id' => 'js-' . $this->getAttribute('name') . '-' . Register::PASSWORD,
                'required' => true,
                'data-toggle' => 'password',
                'data-placement' => 'after',
            ],
        ]);

        $this->add([
            'name' => Register::RE_PASSWORD,
            'type' => Password::class,
            'options' => [
                'label' => $this->translate('Repeat password'),
            ],
            'attributes' => [
                'class' => 'form-control input-lg inverse-mode',
                'id' => 'js-' . $this->getAttribute('name') . '-' . Register::RE_PASSWORD,
                'required' => true,
                'data-toggle' => 'password',
                'data-placement' => 'after',
            ],
        ]);

        $this->add([
            'name' => Register::EMAIL,
            'type' => Email::class,
            'options' => [
                'label' => $this->translate('Email'),
            ],
            'attributes' => [
                'class' => 'form-control input-lg inverse-mode',
                'id' => 'js-' . $this->getAttribute('name') . '-' . Register::EMAIL,
                'required' => true,
            ],
        ]);

//        $this->add([
//            'name' => Register::CSRF,
//            'type' => Csrf::class,
//            'options' => [
//                'csrf_options' => [
//                    'timeout' => 600,
//                ],
//            ],
//        ]);

        $this->add([
            'name' => Register::SUBMIT,
            'type' => Submit::class,
            'attributes' => [
                'value' => $this->translate('Register to service'),
                'class' => 'btn btn-block btn-primary btn-lg',
                'id' => 'js-' . $this->getAttribute('name') . '-' . Register::SUBMIT,
            ],
        ]);
    }
}
