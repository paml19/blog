<?php

namespace paml\Auth\Form;

use paml\Auth\Form\Constants\Login;
use paml\SessionTranslator\Interfaces\TranslatorInterface;
use paml\SessionTranslator\Traits\TranslatorTrait;
use Zend\Form\Element\Checkbox;
use Zend\Form\Element\Csrf;
use Zend\Form\Element\Password;
use Zend\Form\Element\Submit;
use Zend\Form\Element\Text;
use Zend\Form\Form;

class LoginForm extends Form implements TranslatorInterface
{
    use TranslatorTrait;

    public function init()
    {
        $this->add([
            'name' => Login::LOGIN,
            'type' => Text::class,
            'options' => [
                'label' => $this->translate('Login/Email'),
            ],
            'attributes' => [
                'class' => 'form-control input-lg inverse-mode',
                'id' => 'js-' . $this->getAttribute('name') . '-' . Login::LOGIN,
                'required' => true,
            ]
        ]);

        $this->add([
            'name' => Login::PASSWORD,
            'type' => Password::class,
            'options' => [
                'label' => $this->translate('Password'),
            ],
            'attributes' => [
                'class' => 'form-control input-lg inverse-mode',
                'id' => 'js-' . $this->getAttribute('name') . '-' . Login::PASSWORD,
                'required' => true,
                'data-toggle' => 'password',
                'data-placement' => 'after',
            ],
        ]);

        $this->add([
            'name' => Login::REMEMBER_ME,
            'type' => Checkbox::class,
            'options' => [
                'label' => $this->translate('Remember me'),
                'use_hidden_element' => false,
                'checked_value' => Login::REMEMBER_ME_CHECKED,
                'unchecked_value' => Login::REMEMBER_ME_UNCHECKED,
            ],
            'attributes' => [
                'class' => 'form-control',
                'id' => 'js-' . $this->getAttribute('name') . '-' . Login::REMEMBER_ME,
            ],
        ]);

//        $this->add([
//            'name' => Login::CSRF,
//            'type' => Csrf::class,
        ////            'options' => [
        ////                'csrf_options' => [
        ////                    'timeout' => 600,
        ////                ],
        ////            ],
//        ]);

        $this->add([
            'name' => Login::SUBMIT,
            'type' => Submit::class,
            'attributes' => [
                'value' => $this->translate('Login to service'),
                'class' => 'btn btn-block btn-primary btn-lg',
                'id' => 'js-' . $this->getAttribute('name') . '-' . Login::SUBMIT,
            ],
        ]);
    }
}
