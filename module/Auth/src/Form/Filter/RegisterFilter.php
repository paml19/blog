<?php

namespace paml\Auth\Form\Filter;

use DoctrineModule\Validator\NoObjectExists;
use paml\Auth\Form\Constants\Register;
use paml\Auth\Form\Validator\PamlNoObjectExists;
use paml\Auth\Form\Validator\PasswordStrengthValidator;
use paml\Auth\Repository\ExtendedUserRepository;
use Zend\Filter\StringTrim;
use Zend\Filter\StripNewlines;
use Zend\Filter\StripTags;
use Zend\InputFilter\InputFilter;
use Zend\Validator\Csrf;
use Zend\Validator\EmailAddress;
use Zend\Validator\Identical;
use Zend\Validator\StringLength;

class RegisterFilter extends InputFilter
{
    private $userRepository;

    public function __construct(ExtendedUserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function init()
    {
        $this->add([
            'name' => Register::NAME,
            'required' => false,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => StripNewlines::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'max' => 50,
                    ],
                ],
            ],
        ]);

        $this->add([
            'name' => Register::SURNAME,
            'required' => false,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => StripNewlines::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'max' => 100,
                    ],
                ],
            ],
        ]);

        $this->add([
            'name' => Register::LOGIN,
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => StripNewlines::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'max' => 50,
                    ],
                ],
                [
                    'name' => PamlNoObjectExists::class,
                    'options' => [
                        'object_repository' => $this->userRepository,
                        'fields' => 'login',
                        'find_method' => 'findUserWithExtensions',
                        'messages' => [
                            NoObjectExists::ERROR_OBJECT_FOUND => 'Użytkownik z danym loginem już istnieje',
                        ],
                    ],
                ],
            ],
        ]);

        $this->add([
            'name' => Register::PASSWORD,
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => StripNewlines::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'max' => 50,
                        'min' => 1,
                    ],
                ],
                [
                    'name' => PasswordStrengthValidator::class,
                ],
            ],
        ]);

        $this->add([
            'name' => Register::RE_PASSWORD,
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => StripNewlines::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'max' => 50,
                        'min' => 1,
                    ],
                ],
                [
                    'name' => Identical::class,
                    'options' => [
                        'token' => Register::PASSWORD,
                        'messages' => [
                            Identical::NOT_SAME => 'Podane hasła nie są takie same',
                        ],
                    ],
                ],
                [
                    'name' => PasswordStrengthValidator::class,
                ],
            ],
        ]);

        $this->add([
            'name' => Register::EMAIL,
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => StripNewlines::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'max' => 50,
                        'min' => 1,
                    ],
                ],
                [
                    'name' => PamlNoObjectExists::class,
                    'options' => [
                        'object_repository' => $this->userRepository,
                        'fields' => 'email',
                        'find_method' => 'findUserWithExtensions',
                        'messages' => [
                            NoObjectExists::ERROR_OBJECT_FOUND => 'Użytkownik z danym adresem email już istnieje',
                        ],
                    ],
                ],
                ['name' => EmailAddress::class],
            ],
        ]);

//        $this->add([
//            'name' => Register::CSRF,
//            'required' => true,
//            'validators' => [
//                [
//                    'name' => Csrf::class,
//                    'options' => [
//                        'messages' => [
//                            Csrf::NOT_SAME => 'Przesłany formularz nie pochodzi z oczekiwanej strony',
//                        ],
//                    ],
//                ],
//            ],
//        ]);
    }
}
