<?php

namespace paml\Auth\Form\Filter;

use paml\Auth\Form\Constants\Login;
use paml\Auth\Form\Validator\PasswordStrengthValidator;
use paml\Auth\Form\Validator\UserLoginEmailExists;
use paml\Auth\Repository\UserRepository;
use Zend\Filter\StringTrim;
use Zend\Filter\StripNewlines;
use Zend\Filter\StripTags;
use Zend\InputFilter\InputFilter;
use Zend\Validator\Csrf;
use Zend\Validator\StringLength;

class LoginFilter extends InputFilter
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function init()
    {
        $this->add([
            'name' => Login::LOGIN,
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => StripNewlines::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'max' => 50,
                    ],
                ],
                [
                    'name' => UserLoginEmailExists::class,
                    'options' => [
                        'object_repository' => $this->userRepository,
                        'fields' => ['login', 'email'],
                    ],
                ],
            ],
        ]);

        $this->add([
            'name' => Login::PASSWORD,
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => StripNewlines::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'max' => 50,
                        'min' => 1,
                    ],
                ],
                [
                    'name' => PasswordStrengthValidator::class,
                ],
            ],
        ]);

        $this->add([
            'name' => Login::REMEMBER_ME,
            'required' => false,
        ]);

//        $this->add([
//            'name' => Login::CSRF,
//            'required' => true,
//            'validators' => [
//                [
//                    'name' => Csrf::class,
//                    'options' => [
//                        'messages' => [
//                            Csrf::NOT_SAME => 'Przesłany formularz nie pochodzi z oczekiwanej strony',
//                        ],
//                    ],
//                ],
//            ],
//        ]);
    }
}
