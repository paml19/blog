<?php

namespace paml\Auth\Form\Validator;

use DoctrineModule\Validator\NoObjectExists;
use paml\Auth\Entity\User;

class PamlNoObjectExists extends NoObjectExists
{
    private $findMethod;

    private $modules;

    public function __construct(array $options, array $modules)
    {
        parent::__construct($options);

        $this->findMethod = isset($options['find_method']) ? $options['find_method'] : null;
        $this->modules = $modules;
    }

    public function isValid($value)
    {
        $isValid = true;
        $cleanedValue = $this->cleanSearchValue($value);

        if ($this->findMethod) {
            $method = $this->findMethod;
            $match = $this->objectRepository->$method($cleanedValue);
        } else {
            $match = $this->objectRepository->findOneBy($cleanedValue);
        }

        if (is_array($match)) {
            foreach ($match as $matched) {
                if ($matched instanceof User) {
                    $isValid = false;
                }

                if (in_array('paml\Auth\Google', $this->modules) !== false) {
                    if ($matched instanceof \paml\Auth\Google\Entity\User) {
                        $isValid = true;
                        break;
                    }
                }

                if (in_array('paml\Auth\Facebook', $this->modules) !== false) {
                    if ($matched instanceof \paml\Auth\Facebook\Entity\User) {
                        $isValid = true;
                        break;
                    }
                }
            }
        } elseif (is_object($match)) {
            $isValid = false;
        }

        if (! $isValid) {
            $this->error(self::ERROR_OBJECT_FOUND, $value);
        }

        return $isValid;
    }
}
