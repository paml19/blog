<?php

namespace paml\Auth\Form\Validator;

use Zend\Validator\AbstractValidator;

class UserLoginEmailExists extends AbstractValidator
{
    const LOGIN_NOT_EXISTS = 'loginNotExists';

    protected $messageTemplates = [
        self::LOGIN_NOT_EXISTS => 'Użytkownik o danym loginie nie istnieje',
    ];

    public function isValid($value): bool
    {
        $isValid = false;

        if (! $this->getOption('object_repository')) {
            throw new \Exception('No object repository provided');
        }

        if (! $this->getOption('fields')) {
            throw new \Exception('No fields provided');
        }

        $objectRepository = $this->getOption('object_repository');
        $fields = $this->getOption('fields');

        foreach ($fields as $field) {
            if ($objectRepository->findOneBy([$field => $value])) {
                $isValid = true;
                return $isValid;
            }
        }

        $this->error(self::LOGIN_NOT_EXISTS);
        return $isValid;
    }
}
