<?php

namespace paml\Auth\Form\Validator;

use Zend\Validator\AbstractValidator;

class PasswordStrengthValidator extends AbstractValidator
{
    const DIGITS = 'digits';

    const UPPER_CASE = 'upperCase';

    const LOWER_CASE = 'lowerCase';

    const SPECIAL_CHARS = 'specialChars';

    protected $messageTemplates = [
        self::DIGITS => 'Brak cyfr',
        self::UPPER_CASE => 'Brak dużych znaków',
        self::LOWER_CASE => 'Brak małych znaków',
        self::SPECIAL_CHARS => 'Brak znaków specjalnych',
    ];

    public function isValid($value): bool
    {
        $isValid = true;

        if (! preg_match('/[A-Z]/', $value)) {
            $isValid = false;
            $this->error(self::UPPER_CASE);
        }

        if (! preg_match('/[0-9]/', $value)) {
            $isValid = false;
            $this->error(self::DIGITS);
        }

        if (! preg_match('/[a-z]/', $value)) {
            $isValid = false;
            $this->error(self::LOWER_CASE);
        }

        if (! preg_match('/[\W]+/', $value)) {
            $isValid = false;
            $this->error(self::SPECIAL_CHARS);
        }

        return $isValid;
    }
}
