<?php

namespace paml\Auth\Form;

class AuthForms
{
    private $registerForm;

    private $loginForm;

    public function __construct(RegisterForm $registerForm, LoginForm $loginForm)
    {
        $this->registerForm = $registerForm;
        $this->loginForm = $loginForm;
    }

    public function getRegisterForm(): RegisterForm
    {
        return $this->registerForm;
    }

    public function getLoginForm(): LoginForm
    {
        return $this->loginForm;
    }
}
