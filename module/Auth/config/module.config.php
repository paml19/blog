<?php
namespace paml\Auth;

use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Zend\Authentication\AuthenticationService;
use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'controllers' => [
        'factories' => [
            Controller\AuthController::class => Factory\Controller\AuthControllerFactory::class,
            Controller\RoleController::class => Factory\Controller\RoleControllerFactory::class,
            Controller\UserController::class => Factory\Controller\UserControllerFactory::class,
        ],
    ],
    'console' => [
        'router' => [
            'routes' => [
                'init-roles' => [
                    'options' => [
                        'route' => 'init roles',
                        'defaults' => [
                            'controller' => Controller\RoleController::class,
                            'action' => 'initRoles',
                        ],
                    ],
                ],
                'init-admin' => [
                    'options' => [
                        'route' => 'init admin',
                        'defaults' => [
                            'controller' => Controller\UserController::class,
                            'action' => 'initAdmin',
                        ],
                    ],
                ],
            ],
        ],
    ],
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Entity'],
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver',
                ],
            ],
        ],
    ],
    'form_elements' => [
        'factories' => [
            Form\RegisterForm::class => Factory\Form\AbstractRegisterFormFactory::class,
            Form\LoginForm::class => Factory\Form\LoginFormFactory::class,
        ],
    ],
    'input_filters' => [
        'factories' => [
            Form\Filter\RegisterFilter::class => Factory\Form\Filter\RegisterFormFilterFactory::class,
            Form\Filter\LoginFilter::class => Factory\Form\Filter\LoginFormFilterFactory::class,
        ]
    ],
    'router' => [
        'routes' => [
            'auth' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/auth',
                    'defaults' => [
                        'controller' => Controller\AuthController::class,
                        'action' => 'login',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'login' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/login',
                            'defaults' => [
                                'controller' => Controller\AuthController::class,
                                'action' => 'login',
                            ],
                        ],
                    ],
                    'register' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/register',
                            'defaults' => [
                                'controller' => Controller\AuthController::class,
                                'action' => 'register',
                            ],
                        ],
                    ],
                    'logout' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/logout',
                            'defaults' => [
                                'controller' => Controller\AuthController::class,
                                'action' => 'logout',
                            ],
                        ],
                    ],
                    'verify' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/verify',
                            'defaults' => [
                                'controller' => Controller\AuthController::class,
                                'action' => 'verify',
                            ],
                        ],
                    ],
                    'change-role' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/change-role/:id',
                            'defaults' => [
                                'controller' => Controller\AuthController::class,
                                'action' => 'changeRole',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            AuthenticationService::class => Factory\Service\AuthenticationServiceFactory::class,
            Form\AuthForms::class => Factory\Form\AuthFormsFactory::class,
            Listener\AuthListener::class => Factory\Listener\AuthListenerFactory::class,
            Listener\LayoutListener::class => Factory\Listener\LayoutListenerFactory::class,
            Listener\HistoryListener::class => Factory\Listener\HistoryListenerFactory::class,
            Listener\RedirectListener::class => InvokableFactory::class,
            Listener\DefaultRoleListener::class => Factory\Listener\DefaultRoleListenerFactory::class,
            Repository\ExtendedUserRepository::class => Factory\Repository\ExtendedUserRepositoryFactory::class,
            Service\AuthAdapter::class => Factory\Service\AuthAdapterFactory::class,
            Service\AuthManager::class => Factory\Service\AuthManagerFactory::class,
            Service\AclService::class => InvokableFactory::class,
        ],
    ],
    'session_containers' => [
        'Auth\Session'
    ],
    'validators' => [
        'factories' => [
            Form\Validator\PasswordStrengthValidator::class => InvokableFactory::class,
            Form\Validator\UserLoginEmailExists::class => InvokableFactory::class,
            Form\Validator\PamlNoObjectExists::class => Factory\Form\Validator\PamlNoObjectExistsFactory::class,
        ],
    ],
    'view_helpers' => [
        'factories' => [
            Helper\RolesHelper::class => Factory\Helper\RolesHelperFactory::class,
        ],
        'aliases' => [
            'roles' => Helper\RolesHelper::class,
        ],
    ],
    'view_manager' => [
        'template_map' => [
            'auth\roles' => __DIR__ . '/../view/auth/roles.phtml',
        ],
        'template_path_stack' => [
            'paml\Auth' => __DIR__ . '/../view',
        ],
    ],
];
