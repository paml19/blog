<?php

namespace paml\File\Library;

class Module
{
    const CATALOG_CREATED = 'catalogCreated';

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }
}
