<?php

namespace paml\File\Library\Service;

use Doctrine\Common\Collections\Collection;
use paml\File\Entity\File;
use paml\File\Library\Entity\Catalog;
use paml\File\Library\Repository\CatalogRepository;

class CatalogManager
{
    private $catalogRepository;

    private $publicPath;

    public function __construct(CatalogRepository $catalogRepository, string $publicPath)
    {
        $this->catalogRepository = $catalogRepository;
        $this->publicPath = $publicPath;
    }

    public function createCatalog(string $catalogName): Catalog
    {
        $slug = str_replace(' ', '-', strtolower($catalogName));

        $catalog = $this->catalogRepository->findOneBy([Catalog::SLUG => $slug]);

        if (! $catalog) {
            $catalog = (new Catalog)
                ->setName($catalogName)
                ->setSize(0)
                ->setSlug($slug);
        }

        $this->catalogRepository->save($catalog);
        $targetDir = $this->publicPath . $catalog->getPath();

        if (! file_exists($targetDir)) {
            mkdir($targetDir, 0777, true);
        }

        return $catalog;
    }

    public function createCatalogWithFile(string $catalogName, File $file): Catalog
    {
        $slug = str_replace(' ', '-', strtolower($catalogName));

        $catalog = $this->catalogRepository->findOneBy([Catalog::SLUG => $slug]);

        if (! $catalog) {
            $catalog = (new Catalog)
                ->setName($catalogName)
                ->setSlug($slug);
        }
        $catalog->setSize($catalog->getSize() + $file->getSize())
            ->addFile($file);

        $this->catalogRepository->persist($catalog);
        $targetDir = $this->publicPath . $catalog->getPath();

        if (! file_exists($targetDir)) {
            mkdir($targetDir, 0777, true);
        }

        return $catalog;
    }

    public function createCatalogWithFiles(string $catalogName, Collection $files): Catalog
    {
        $filesSize = 0;

        /** @var File $file */
        foreach ($files as $file) {
            $filesSize += $file->getSize();
        }

        $slug = str_replace(' ', '-', strtolower($catalogName));

        $catalog = $this->catalogRepository->findOneBy([Catalog::SLUG => $slug]);

        if (! $catalog) {
            $catalog = (new Catalog)
                ->setName($catalogName)
                ->setSlug($slug);
        }
        $catalog->setSize($filesSize)
            ->setFiles($files);

        $this->catalogRepository->persist($catalog);
        $targetDir = $this->publicPath . $catalog->getPath();

        if (! file_exists($targetDir)) {
            mkdir($targetDir, 0777, true);
        }

        return $catalog;
    }
}
