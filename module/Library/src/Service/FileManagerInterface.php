<?php

namespace paml\File\Library\Service;

use paml\File\Entity\File;
use paml\File\Service\FileManagerInterface as BaseFileManagerInterface;

interface FileManagerInterface extends BaseFileManagerInterface
{
    public function uploadFile(
        string $sourcePath,
        string $sourceFilename,
        int $sourceSize,
        string $sourceType,
        string $catalogName): File;
}
