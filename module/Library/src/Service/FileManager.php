<?php


namespace paml\File\Library\Service;

use paml\File\Entity\File;
use paml\File\Library\Repository\CatalogRepository;
use paml\File\Repository\FileRepository;
use paml\File\Service\FileManager as BaseFileManager;

class FileManager extends BaseFileManager implements FileManagerInterface
{
    private $catalogManager;

    private $catalogRepository;

    public function __construct(
        FileRepository $fileRepository,
        string $publicPath,
        CatalogManager $catalogManager,
        CatalogRepository $catalogRepository
    ) {
        parent::__construct($fileRepository, $publicPath);
        $this->catalogManager = $catalogManager;
        $this->catalogRepository = $catalogRepository;
    }

    public function uploadFile(
        string $sourcePath,
        string $sourceFilename,
        int $sourceSize,
        string $sourceType,
        string $catalogName
    ): File {
        $file = (new File)
            ->setSize($sourceSize)
            ->setName($sourceFilename)
            ->setType($sourceType);

        $this->fileRepository->persist($file);

        $catalog = $this->catalogManager->createCatalogWithFile($catalogName, $file);

        $targetDir = $this->publicPath . $catalog->getPath() . $file->getNoUploadPathPath();
        $targetPath = $targetDir . $sourceFilename;

        if (! file_exists($targetDir)) {
            mkdir($targetDir, 0777, true);
        }

        if (rename($sourcePath, $targetPath)) {
            return $file;
        } else {
            throw new \RuntimeException('The file could not be uploaded');
        }
    }

    public function getFile(File $file): string
    {
        $catalog = $this->catalogRepository->findCatalogFromFile($file);
        return file_get_contents($this->publicPath . $catalog->getPath() . $file->getNoUploadPathFilePath());
    }

    public function getFilePath(File $file): string
    {
        $catalog = $this->catalogRepository->findCatalogFromFile($file);
        return $this->publicPath . $catalog->getPath() . $file->getNoUploadPathFilePath();
    }
}
