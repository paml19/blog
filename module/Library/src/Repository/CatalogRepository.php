<?php

namespace paml\File\Library\Repository;

use Doctrine\ORM\EntityRepository;
use paml\File\Entity\File;
use paml\File\Library\Entity\Catalog;

class CatalogRepository extends EntityRepository
{
    public function save(Catalog $catalog): void
    {
        $this->getEntityManager()->persist($catalog);
        $this->getEntityManager()->flush();
    }

    public function persist(Catalog $catalog): void
    {
        $this->getEntityManager()->persist($catalog);
    }

    public function findCatalogFromFile(File $file): Catalog
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder->select('c')
            ->from($this->getEntityName(), 'c')
            ->where($queryBuilder->expr()->isMemberOf(':fileId', 'c.files'))
            ->setParameter(':fileId', $file);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }
}
