<?php

namespace paml\File\Library\Hydrator;

use Doctrine\Common\Collections\ArrayCollection;
use paml\File\Library\Entity\Catalog;
use paml\File\Library\Service\CatalogManager;
use Zend\Hydrator\AbstractHydrator;

class CatalogHydrator extends AbstractHydrator
{
    private $fileHydrator;

    private $catalogManager;

    public function __construct(FileHydrator $fileHydrator, CatalogManager $catalogManager)
    {
        $this->fileHydrator = $fileHydrator;
        $this->catalogManager = $catalogManager;
    }

    public function extract($object)
    {
        $files = new ArrayCollection;

        foreach ($object->getFiles() as $file) {
            $files->add($this->fileHydrator->extract($file));
        }

        return [
            Catalog::ID => $object->getId(),
            Catalog::NAME => $object->getName(),
            Catalog::DATE_ADD => $object->getDateAdd() ? $object->getDateAdd()->format('d.m.Y H:i:s') : null,
            Catalog::SIZE => $object->getSize(),
            Catalog::FILES => $files->toArray(),
        ];
    }

    public function hydrate(array $data, $object)
    {
        if (isset($data['catalog'])) {
            $object = $this->catalogManager->createCatalog($data['catalog']);
        }

        return $object;
    }
}
