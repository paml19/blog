<?php

namespace paml\File\Library\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use paml\File\Entity\File;

/**
 * @ORM\Table(name="file_catalog")
 * @ORM\Entity(repositoryClass="paml\File\Library\Repository\CatalogRepository")
 * @GEDMO\Loggable
 * @Gedmo\SoftDeleteable(fieldName="dateDelete", timeAware=true)
 */
class Catalog
{
    const UPLOAD_PATH = '/upload/';

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid", unique=true, nullable=false)
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;
    const ID = 'id';

    /**
     * @ORM\Column(type="string", name="name", nullable=true)
     */
    private $name;
    const NAME = 'name';

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(name="slug", type="string", nullable=true, unique=true)
     */
    private $slug;
    const SLUG = 'slug';

    /**
     * @ORM\Column(type="integer", name="size", nullable=true)
     */
    private $size;
    const SIZE = 'size';

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="date_add", type="datetime")
     */
    private $dateAdd;
    const DATE_ADD = 'dateAdd';

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="date_edit", type="datetime", nullable=true)
     */
    private $dateEdit;
    const DATE_EDIT = 'dateEdit';

    /**
     * @ORM\Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;
    const DATE_DELETE = 'dateDelete';

    /**
     * @ORM\ManyToMany(targetEntity="paml\File\Entity\File")
     * @ORM\JoinTable(name="file_catalog_files",
     *      joinColumns={@ORM\JoinColumn(name="catalog", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="file", referencedColumnName="id", unique=true)}
     *      )
     */
    private $files;
    const FILES = 'files';
    const FILE = 'file';

    public function __construct()
    {
        $this->files = new ArrayCollection;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;
        return $this;
    }

    public function getSize(): ?int
    {
        return $this->size;
    }

    public function setSize(int $size): self
    {
        $this->size = $size;
        return $this;
    }

    public function getFiles(): Collection
    {
        return $this->files;
    }

    public function setFiles(Collection $files): self
    {
        $this->files = $files;
        return $this;
    }

    public function addFile(File $file): self
    {
        if (! $this->files->contains($file)) {
            $this->files->add($file);
        }

        return $this;
    }

    public function removeFile(File $file): self
    {
        if ($this->files->contains($file)) {
            $this->files->removeElement($file);
        }

        return $this;
    }

    public function getDateAdd(): ?\DateTime
    {
        return $this->dateAdd;
    }

    public function getDateEdit(): ?\DateTime
    {
        return $this->dateEdit;
    }

    public function getDateDelete(): ?\DateTime
    {
        return $this->dateDelete;
    }

    public function getPath(): ?string
    {
        if ($this->id) {
            return self::UPLOAD_PATH . $this->id;
        }

        return null;
    }
}
