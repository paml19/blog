<?php

namespace paml\File\Library\Factory\Hydrator;

use Interop\Container\ContainerInterface;
use paml\File\Library\Hydrator\FileHydrator;
use paml\File\Library\Service\FileManager;
use Zend\ServiceManager\Factory\FactoryInterface;

class FileHydratorFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): FileHydrator
    {
        return new FileHydrator(
            $container->get(FileManager::class)
        );
    }
}
