<?php

namespace paml\File\Library\Factory\Hydrator;

use Interop\Container\ContainerInterface;
use paml\File\Library\Hydrator\CatalogHydrator;
use paml\File\Library\Hydrator\FileHydrator;
use paml\File\Library\Service\CatalogManager;
use Zend\ServiceManager\Factory\FactoryInterface;

class CatalogHydratorFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): CatalogHydrator
    {
        return new CatalogHydrator(
            $container->get('HydratorManager')->get(FileHydrator::class),
            $container->get(CatalogManager::class)
        );
    }
}
