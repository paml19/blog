<?php

namespace paml\File\Library\Factory\Service;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use paml\File\Library\Entity\Catalog;
use paml\File\Library\Service\CatalogManager;
use Zend\ServiceManager\Factory\FactoryInterface;

class CatalogManagerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): CatalogManager
    {
        return new CatalogManager(
            $container->get(EntityManager::class)->getRepository(Catalog::class),
            $container->get('Config')['file']['public_path']
        );
    }
}
