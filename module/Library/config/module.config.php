<?php
namespace paml\File\Library;

use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use paml\File\Controller\FileController;

return [
    'acl' => [
        'resources' => [
            'excluded' => [
                Controller\FileController::class,
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            FileController::class => Factory\Controller\FileControllerFactory::class,
        ],
    ],
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ],
            ],
        ],
    ],
    'hydrators' => [
        'factories' => [
            Hydrator\FileHydrator::class => Factory\Hydrator\FileHydratorFactory::class,
            Hydrator\CatalogHydrator::class => Factory\Hydrator\CatalogHydratorFactory::class,
        ],
    ],
    'service_manager' => [
        'factories' => [
            Service\FileManager::class => Factory\Service\FileManagerFactory::class,
            Service\CatalogManager::class => Factory\Service\CatalogManagerFactory::class,
        ],
    ],
];
