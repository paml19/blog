<?php

namespace paml\EntityRest;

use paml\EntityRest\Listener\RestOptionsListener;
use Zend\Mvc\MvcEvent;

class Module
{
    const EVENT_GET = 'eventGet';

    const EVENT_GET_LIST = 'eventGetList';

    const EVENT_CREATE = 'eventCreate';

    const EVENT_CREATED = 'eventCreated';

    const EVENT_UPDATE = 'eventUpdate';

    const EVENT_UPDATED = 'eventUpdated';

    const EVENT_DELETE = 'eventDelete';

    const EVENT_DELETED = 'eventDeleted';

    public function getConfig()
    {
        if (! file_exists(__DIR__ . '/../../../config/autoload/entityRest.global.php')) {
            throw new \Exception('No file entityRest.global.php provided, copy dist from module config to ./config/autoload/');
        }

        $mainConfig = include __DIR__ . '/../../../config/autoload/entityRest.global.php';
        $fileConfig = include __DIR__ . '/../config/module.config.php';

        $controllersAliases = [
            'controllers' => [
                'aliases' => [],
            ]
        ];

        $entitiesFiles = scandir(__DIR__ . '/../../' . $mainConfig['entity_rest']['main_module'] . '/src/Entity');
        $entitiesFiles = array_slice($entitiesFiles, 2, count($entitiesFiles) - 2);

        foreach ($entitiesFiles as $entityFile) {
            $entity = explode('.', $entityFile)[0];
            $entityStrLen = strlen($entity);

            if (substr($entity, $entityStrLen - 1, $entityStrLen) == 'y') {
                $aliasName = substr($entity, 0, $entityStrLen - 1) . 'ies';
            } else {
                $aliasName = $entity . 's';
            }

            $controllersAliases['controllers']['aliases'][mb_strtolower($aliasName)] = $mainConfig['entity_rest']['main_module'] . '\\Controller\Api\\' .
                $entity . 'Controller';
        }

        $config = array_merge_recursive($fileConfig, $controllersAliases);

        return $config;
    }

    public function onBootstrap(MvcEvent $event)
    {
        $serviceManager = $event->getApplication()->getServiceManager();
        $eventManager = $event->getTarget()->getEventManager();

        /** @var RestOptionsListener $restOptionsListener */
        $restOptionsListener = $serviceManager->get(RestOptionsListener::class);
        $restOptionsListener->attach($eventManager);
    }
}
