<?php

namespace paml\EntityRest\Listener;

use paml\EntityRest\Controller\EntityRestControllerInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\ListenerAggregateTrait;
use Zend\Http\Response;
use Zend\Mvc\MvcEvent;

class RestOptionsListener implements ListenerAggregateInterface
{
    use ListenerAggregateTrait;

    private $allowedMethods;

    public function __construct(array $allowedMethods)
    {
        $this->allowedMethods = $allowedMethods;
    }

    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $sharedEventManager = $events->getSharedManager();

        $sharedEventManager->attach(
            EntityRestControllerInterface::class,
            MvcEvent::EVENT_DISPATCH,
            [
                $this,
                'checkOptions'
            ],
            $priority
        );
    }

    public function checkOptions(MvcEvent $event)
    {
        $response = $event->getResponse();
        $request = $event->getRequest();
        $method = $request->getMethod();

        if (! in_array($method, $this->allowedMethods)) {
            $response->setStatusCode(Response::STATUS_CODE_405);
            throw new \Exception('Method Not Allowed');
            return $response;
        }
    }
}
