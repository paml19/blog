<?php


namespace paml\EntityRest\Factory;

use Interop\Container\ContainerInterface;
use paml\EntityRest\Listener\RestOptionsListener;
use Zend\ServiceManager\Factory\FactoryInterface;

class RestOptionsListenerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): RestOptionsListener
    {
        return new RestOptionsListener(
            $container->get('Config')['entity_rest']['allowed_methods']
        );
    }
}
