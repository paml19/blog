<?php

namespace paml\EntityRest\Factory;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class AbstractRestControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $mainModule = $container->get('Config')['entity_rest']['main_module'];
        $entityName = explode('\\', $requestedName);
        $entityName = str_replace('Controller', '', $entityName[count($entityName) - 1]);

        return new $requestedName(
            $container->get(EntityManager::class)->getRepository($mainModule . '\\Entity\\' . $entityName),
            $container->get('HydratorManager')->get($mainModule . '\\Hydrator\\Api\\' . $entityName . 'Hydrator'),
            $container->get('HydratorManager')->get($mainModule . '\\Hydrator\\Api\\' . $entityName . 'ListHydrator'),
            $container->get('Config')['entity_rest']['defaults'],
            $container->get('FormElementManager')->get($mainModule . '\\Form\\Api\\' . $entityName . 'Form')
        );
    }
}
