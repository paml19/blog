<?php

namespace paml\EntityRest\Factory;

use Interop\Container\ContainerInterface;
use paml\EntityRest\Controller\DefaultController;
use Zend\ServiceManager\Factory\FactoryInterface;

class DefaultControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): DefaultController
    {
        return new DefaultController(
            $container->get('Config')['entity_rest']['defaults']
        );
    }
}
