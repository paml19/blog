<?php

namespace paml\EntityRest\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class AbstractRestFormFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $mainModule = $container->get('Config')['entity_rest']['main_module'];
        $entityName = explode('\\', $requestedName);
        $entityName = str_replace('Form', '', $entityName[count($entityName) - 1]);
        $entityObjectName = $mainModule . '\\Entity\\' . $entityName;

        $form = new $requestedName(strtolower($entityName) . '-form');
        $form->setInputFilter($container->get('InputFilterManager')->get($mainModule . '\\Filter\\Api\\' . $entityName .
            'Filter'));
        $form->setHydrator($container->get('HydratorManager')->get($mainModule . '\\Hydrator\\Api\\' . $entityName . 'Hydrator'));
        $form->setObject(new $entityObjectName);

        return $form;
    }
}
