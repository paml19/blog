<?php

namespace paml\EntityRest\Controller;

interface EntityRestControllerInterface
{
    public function get($id);

    public function getList();

    public function create($data);

    public function update($id, $data);

    public function delete($id);
}
