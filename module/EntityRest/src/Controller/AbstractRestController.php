<?php

namespace paml\EntityRest\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use paml\EntityOperation\Hydrator\HydratorInterface;
use paml\EntityRest\Module;
use paml\EntityOperation\Repository\AbstractEntityRepository;
use paml\EntityOperation\Repository\EntityRepositoryInterface;
use Zend\Form\FormInterface;
use Zend\Http\Response;
use Zend\Json\Json;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

abstract class AbstractRestController extends AbstractRestfulController implements EntityRestControllerInterface
{
    protected $entityRepository;

    protected $hydrator;

    protected $listHydrator;

    protected $defaults;

    protected $form;

    public function __construct(
        AbstractEntityRepository $entityRepository,
        HydratorInterface $hydrator,
        HydratorInterface $listHydrator,
        array $defaults,
        FormInterface $form
    ) {
        $this->entityRepository = $entityRepository;
        $this->hydrator = $hydrator;
        $this->listHydrator = $listHydrator;
        $this->defaults = $defaults;
        $this->form = $form;
    }

    public function get($id)
    {
        $object = $this->entityRepository->get($id);

        if (! $object) {
            $this->response->setStatusCode(Response::STATUS_CODE_404);
            return new JsonModel([
                'status' => 'error',
                'msg' => 'Not found entity with id=' . $id
            ]);
        }

        $this->getEventManager()->trigger(Module::EVENT_GET, $this, ['object' => $object]);

        return new JsonModel($this->hydrator->extract($object));
    }

    public function getList()
    {
        $page = (int) $this->params()->fromQuery('page', $this->defaults['page']);
        $count = (int) $this->params()->fromQuery('count', $this->defaults['count']);
        $sort = (string) $this->params()->fromQuery('sort', $this->defaults['sort']);
        $order = (string) $this->params()->fromQuery('order', $this->defaults['order']);

        $list = $this->entityRepository->getList($page, $count, $sort, $order);

        $items = new ArrayCollection();
        if ($list->count() >= $page) {
            foreach ($list as $item) {
                $items->add($item);
            }
        }

        $this->getEventManager()->trigger(Module::EVENT_GET_LIST, $this, ['list' => $items]);

        return new JsonModel([
            'data' => array_map(function ($object) {
                return $this->listHydrator->extract($object);
            }, $items->toArray()),
            'pagination' => [
                'current' => $list->getCurrentPageNumber(),
                'pages' => $list->count(),
                'items' => $list->getCurrentItemCount(),
                'total' => $list->getTotalItemCount()
            ],
            'params' => [
                'page' => $page,
                'count' => $count,
                'sort' => $sort,
                'order' => $order
            ]
        ]);
    }

    public function create($data)
    {
        $data = array_merge_recursive(
            $data,
            $this->getRequest()->getFiles()->toArray()
        );

        if (empty($data)) {
            $this->response->setStatusCode(Response::STATUS_CODE_404);

            return new JsonModel([
                'status' => 'error',
                'msg' => 'Not found data'
            ]);
        }

        $postData = [];
        foreach ($data as $key => $datum) {
            if (! is_array($datum) && json_decode($datum)) {
                $postData[$key] = Json::decode($datum);
            } else {
                $postData[$key] = $datum;
            }
        }

        $this->getEventManager()->trigger(Module::EVENT_CREATE, $this, ['data' => $postData]);

        try {
            $this->form->setData($postData);
            if ($this->form->isValid()) {
                $object = $this->form->getData();

                $this->entityRepository->save($object);

                $this->getEventManager()->trigger(Module::EVENT_CREATED, $this, ['object' => $object]);
            } else {
                $this->response->setStatusCode(Response::STATUS_CODE_500);

                return new JsonModel([
                    'status' => 'error',
                    'msg' => $this->form->getMessages(),
                    'code' => $this->form->isValid()
                ]);
            }
        } catch (\RuntimeException $exception) {
            $this->response->setStatusCode(Response::STATUS_CODE_500);

            return new JsonModel([
                'status' => 'error',
                'msg' => $exception->getMessage(),
                'code' => $exception->getCode()
            ]);
        }

        $this->response->setStatusCode(Response::STATUS_CODE_201);
        return new JsonModel([
            'status' => 'created'
        ]);
    }

    public function update($id, $data)
    {
        if (empty($data)) {
            $this->response->setStatusCode(Response::STATUS_CODE_404);

            return new JsonModel([
                'status' => 'error',
                'msg' => 'Not found data'
            ]);
        }

        $object = $this->entityRepository->find($id);

        if (! $object) {
            $this->response->setStatusCode(Response::STATUS_CODE_404);
            return new JsonModel([
                'status' => 'error',
                'msg' => 'Not found entity with id=' . $id
            ]);
        }

        $postData = [];
        foreach ($data as $key => $datum) {
            if (json_decode($datum)) {
                $postData[$key] = Json::decode($datum);
            } else {
                $postData[$key] = $datum;
            }
        }

        $this->getEventManager()->trigger(Module::EVENT_UPDATE, $this, ['data' => $postData]);

        $object = $this->hydrator->hydrate($postData, $object);
        try {
            $this->form->bind($object);
            if ($this->form->isValid()) {
                $object = $this->form->getData();

                $this->entityRepository->merge($object);

                $this->getEventManager()->trigger(Module::EVENT_UPDATED, $this, ['object' => $object]);
            } else {
                $this->response->setStatusCode(Response::STATUS_CODE_500);

                return new JsonModel([
                    'status' => 'error',
                    'msg' => $this->form->getMessages(),
                    'code' => $this->form->isValid()
                ]);
            }
        } catch (\RuntimeException $exception) {
            $this->response->setStatusCode(Response::STATUS_CODE_500);

            return new JsonModel([
                'status' => 'error',
                'msg' => $exception->getMessage(),
                'code' => $exception->getCode()
            ]);
        }

        $this->response->setStatusCode(Response::STATUS_CODE_201);
        return new JsonModel([
            'status' => 'updated'
        ]);
    }

    public function delete($id)
    {
        $entity = $this->entityRepository->find($id);

        if (! $entity) {
            $this->response->setStatusCode(Response::STATUS_CODE_404);
            return new JsonModel([
                'status' => 'error',
                'msg' => 'Not found entity with id=' . $id
            ]);
        }

        $this->getEventManager()->trigger(Module::EVENT_DELETE, $this, ['object' => $entity]);

        $this->entityRepository->delete($entity);

        $this->getEventManager()->trigger(Module::EVENT_DELETED, $this, ['object' => $entity]);

        return new JsonModel([
            'status' => 'deleted',
        ]);
    }
}
