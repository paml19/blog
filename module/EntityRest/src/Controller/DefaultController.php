<?php

namespace paml\EntityRest\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use DoctrineModule\Paginator\Adapter\Collection as CollectionAdapter;
use Zend\Http\Response;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\Paginator\Paginator;
use Zend\View\Model\JsonModel;

class DefaultController extends AbstractRestfulController implements EntityRestControllerInterface
{
    private $defaults;

    public function __construct(array $defaults)
    {
        $this->defaults = $defaults;
    }

    public function getList()
    {
        $page = (int) $this->params()->fromQuery('page', $this->defaults['page']);
        $count = (int) $this->params()->fromQuery('count', $this->defaults['count']);
        $sort = (string) $this->params()->fromQuery('sort', $this->defaults['sort']);
        $order = (string) $this->params()->fromQuery('order', $this->defaults['order']);

        $timestamp = intval((new \DateTime)->getTimestamp() / 100000);

        $data = [];
        for ($i = 0; $i <= $timestamp; $i++) {
            $default = $timestamp - $i;
            $data[] = [
                'id' => $i,
                'name' => 'Item ' . $default,
                'dateAdd' => (new \DateTime)->format('d.m.Y H:i:s'),
            ];
        }

        $functionSort = SORT_ASC;
        if ($sort == 'desc') {
            $functionSort = SORT_DESC;
        }

        array_multisort(array_map(function ($element) use ($order) {
            return $element[$order];
        }, $data), $functionSort, $data);

        $collection = new ArrayCollection($data);
        $adapter = new CollectionAdapter($collection);

        $list = (new Paginator($adapter))
            ->setCurrentPageNumber($page)
            ->setItemCountPerPage($count);

        $items = new ArrayCollection;
        if ($list->count() >= $page) {
            foreach ($list as $item) {
                $items->add($item);
            }
        }

        return new JsonModel([
            'data' => array_map(function ($item) {
                return $item;
            }, $items->toArray()),
            'pagination' => [
                'current' => $list->getCurrentPageNumber(),
                'pages' => $list->count(),
                'items' => $list->getCurrentItemCount(),
                'total' => $list->getTotalItemCount()
            ],
            'params' => [
                'page' => $page,
                'count' => $count,
                'sort' => $sort,
                'order' => $order,
            ],
        ]);
    }

    public function get($id)
    {
        return new JsonModel([
            'data' => [
                'id' => $id,
                'name' => 'Item ' . $id,
            ],
        ]);
    }

    public function create($data)
    {
        $this->response->setStatusCode(Response::STATUS_CODE_201);
        return new JsonModel([
            'data' => $data
        ]);
    }

    public function update($id, $data)
    {
        $this->response->setStatusCode(Response::STATUS_CODE_201);
        return new JsonModel([
            'data' => $data,
            'id' => $id
        ]);
    }

    public function delete($id)
    {
        return new JsonModel([
            'deleted object with id=' . $id
        ]);
    }
}
