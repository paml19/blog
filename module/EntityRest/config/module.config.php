<?php

namespace paml\EntityRest;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;

return [
    'controllers' => [
        'factories' => [
            Controller\DefaultController::class => Factory\DefaultControllerFactory::class,
        ],
        'aliases' => [
            'defaults' => Controller\DefaultController::class,
            'default' => Controller\DefaultController::class,
        ]
    ],
    'router' => [
        'routes' => [
            'api' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/api',
                    'defaults' => [
                        'controller' => Controller\DefaultController::class,
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'rest' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/:controller[/:id]',
                        ],
                    ],
                ],
            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            Listener\RestOptionsListener::class => Factory\RestOptionsListenerFactory::class,
        ],
    ],
    'view_manager' => [
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
];
