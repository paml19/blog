<?php
namespace paml\Auth\Rest;

use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'controllers' => [
        'factories' => [
            Controller\AuthController::class => Factory\AuthControllerFactory::class,
        ],
    ],
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ],
            ],
        ],
    ],
    'router' => [
        'routes' => [
            'auth' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/auth',
                ],
                'may_terminate' => false,
                'child_routes' => [
                    'rest' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/rest[/:action[/:id]]',
                            'defaults' => [
                                'controller' => Controller\AuthController::class,
                                'action' => 'index',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            Listener\AuthListener::class => Factory\AuthListenerFactory::class,
            Service\AuthorizationParamsParser::class => InvokableFactory::class,
            Service\AuthAdapter::class => Factory\AuthAdapterFactory::class,
            Service\TokenService::class => Factory\TokenServiceFactory::class,
        ],
    ],
];
