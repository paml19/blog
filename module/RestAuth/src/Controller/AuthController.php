<?php

namespace paml\Auth\Rest\Controller;

use paml\Auth\Listener\HistoryListener;
use paml\Auth\Module;
use paml\Auth\Rest\Entity\User;
use paml\Auth\Rest\Service\AuthAdapter;
use paml\Auth\Rest\Service\AuthorizationParamsParser;
use paml\Auth\Rest\Service\TokenService;
use paml\Auth\Service\AuthManager;
use Zend\Authentication\Result;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class AuthController extends AbstractRestfulController
{
    private $authManager;

    private $authorizationParamsParser;

    private $authAdapter;

    private $tokenService;

    public function __construct(
        AuthManager $authManager,
        AuthorizationParamsParser $authorizationParamsParser,
        AuthAdapter $authAdapter,
        TokenService $tokenService
    ) {
        $this->authManager = $authManager;
        $this->authorizationParamsParser = $authorizationParamsParser;
        $this->authAdapter = $authAdapter;
        $this->tokenService = $tokenService;
    }

    public function getTokenAction()
    {
        $authorizationParams = $this->authorizationParamsParser->parseBaseAuth($this->params());

        $result = $this->authManager->login(
            $authorizationParams->get('login'),
            $authorizationParams->get('password'),
            null,
            $this->authAdapter,
            null
        );

        if ($result->getCode() == Result::SUCCESS) {
            $this->getEventManager()->trigger(
                Module::AUTH_USER_LOGGED_IN,
                $this,
                [
                    'userId' => $this->authManager->getIdentity()[User::USER]['id'],
                    'identity' => $this->authManager->getIdentity()[User::USER]
                ]
            );

            $this->getEventManager()->trigger(
                Module::AUTH_USER_HISTORY,
                $this,
                [
                    'userId' => $this->authManager->getIdentity()[User::USER]['id'],
                    'type' => HistoryListener::HISTORY_TYPE_LOGIN,
                    'success' => true,
                ]
            );

            $accessToken = $this->tokenService->createToken($result->getIdentity()['id']);

            return new JsonModel($accessToken->toAuthArray());
        }

        return new JsonModel([
            'status' => 'error',
            'msg' => $result->getMessages(),
            'code' => $result->getCode()
        ]);
    }

    public function indexAction()
    {
        $authorizationString = $this->authorizationParamsParser->parseBearerAuth($this->params());
        $valid = $this->tokenService->validateAccessToken($authorizationString);

        if ($valid) {
            return new JsonModel([
                'status' => 'ok',
            ]);
        }

        return new JsonModel([
            'status' => 'error',
            'msg' => 'Access token is not valid'
        ]);
    }
}
