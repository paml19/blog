<?php

namespace paml\Auth\Rest\Service;

use paml\Auth\Entity\User as BaseUser;
use paml\Auth\Repository\UserRepository as BaseUserRepository;
use paml\Auth\Rest\Entity\User;
use paml\Auth\Rest\Repository\UserRepository;
use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Result;

class AuthAdapter implements AdapterInterface
{
    private $login;

    private $userId;

    private $password;

    private $baseUserRepository;

    private $userRepository;

    public function __construct(BaseUserRepository $baseUserRepository, UserRepository $userRepository)
    {
        $this->baseUserRepository = $baseUserRepository;
        $this->userRepository = $userRepository;
    }

    public function authenticate()
    {
        /** @var BaseUser $baseUser */
        $baseUser = $this->baseUserRepository->findOneBy(['login' => $this->login]);

        if (! $baseUser) {
            $baseUser = $this->baseUserRepository->findOneBy(['email' => $this->login]);
        }

        if ($this->userId) {
            $baseUser = $this->baseUserRepository->find($this->userId);
            return new Result(Result::SUCCESS, $baseUser->toAuthArray(), [
                'Authenticated successfully'
            ]);
        }

        if (! $baseUser) {
            return new Result(Result::FAILURE_IDENTITY_NOT_FOUND, null, [
                'Identity not found'
            ]);
        }

        if (! $baseUser->isActive()) {
            return new Result(Result::FAILURE, null, [
                'Status inactive'
            ]);
        }

        if (! $baseUser->verifyPassword($this->password)) {
            return new Result(Result::FAILURE_CREDENTIAL_INVALID, null, [
                'Invalid credential'
            ]);
        }

        $user = $this->userRepository->findOneBy([User::USER => $baseUser]);

        if (! $user) {
            $user = (new User)
                ->setUser($baseUser);

            $this->userRepository->save($user);
        }

        return new Result(Result::SUCCESS, $user->toAuthArray(), [
            'Authenticated successfully'
        ]);
    }

    public function setLogin($login)
    {
        $this->login = $login;
        return $this;
    }

    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    public function setUserId(?string $userId)
    {
        $this->userId = $userId;
    }
}
