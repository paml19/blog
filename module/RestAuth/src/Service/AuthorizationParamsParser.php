<?php

namespace paml\Auth\Rest\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Zend\Mvc\Controller\Plugin\Params;

class AuthorizationParamsParser
{
    public function parseBaseAuth(Params $params): ?ArrayCollection
    {
        $authorizationString = $params->fromHeader('Authorization')->getFieldValue();
        $authorizationString = explode(' ', $authorizationString);

        if ($authorizationString[0] != 'Base') {
            $authorizationString = base64_decode($authorizationString[1]);
            $authorizationParamsString = explode(':', $authorizationString);

            $authorizationParams = [
                'login' => $authorizationParamsString[0],
                'password' => $authorizationParamsString[1],
            ];

            return new ArrayCollection($authorizationParams);
        }

        return null;
    }

    public function parseBearerAuth(Params $params): ?string
    {
        $authorizationString = $params->fromHeader('Authorization')->getFieldValue();
        $authorizationString = explode(' ', $authorizationString);

        if ($authorizationString[0] == 'Bearer') {
            return $authorizationString[1];
        }

        return null;
    }

    public function getType(Params $params)
    {
        $authorizationString = $params->fromHeader('Authorization')->getFieldValue();
        $authorizationString = explode(' ', $authorizationString);

        return $authorizationString[0];
    }
}
