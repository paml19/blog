<?php

namespace paml\Auth\Rest\Service;

use paml\Auth\Rest\Entity\User;
use paml\Auth\Rest\Entity\AccessToken;
use paml\Auth\Rest\Repository\UserRepository;
use paml\Auth\Rest\Repository\AccessTokenRepository;

class TokenService
{
    private $accessTokenRepository;

    private $userRepository;

    private $config;

    public function __construct(
        AccessTokenRepository $accessTokenRepository,
        UserRepository $userRepository,
        array $config
    ) {
        $this->accessTokenRepository = $accessTokenRepository;
        $this->userRepository = $userRepository;
        $this->config = $config;
    }

    public function createToken(string $userId): AccessToken
    {
        /** @var User $user */
        $user = $this->userRepository->find($userId);
        if ($user) {
            $this->validateTokens($user);

            $token = (new AccessToken)
                ->setAccessToken($this->randomAlphanumericString($this->config['token_length']))
                ->setUser($user)
                ->setDateExpire((new \DateTime)->modify('+ ' . $this->config['token_expire_time'] . ' seconds'));

            $this->accessTokenRepository->save($token);

            return $token;
        }
    }

    public function validateAccessToken(string $accessToken): ?AccessToken
    {
        /** @var AccessToken $accessToken */
        $accessToken = $this->accessTokenRepository->findOneBy([AccessToken::ACCESS_TOKEN => $accessToken]);

        if ($accessToken) {
            $this->validateTokens($accessToken->getUser());

            if (! $accessToken->getDateDelete()) {
                return $accessToken;
            }
        }

        return null;
    }

    private function validateTokens(User $user): void
    {
        $accessTokens = $this->accessTokenRepository->findBy([AccessToken::USER => $user]);

        /** @var AccessToken $accessToken */
        foreach ($accessTokens as $accessToken) {
            if ($accessToken->getDateExpire() <= (new \DateTime)) {
                $this->accessTokenRepository->delete($accessToken);
            }
        }
    }

    private function randomAlphanumericString($length): string
    {
        $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()_+<>?;';
        return substr(str_shuffle($chars), 0, $length);
    }
}
