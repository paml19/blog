<?php

namespace paml\Auth\Rest\Factory;

use Interop\Container\ContainerInterface;
use paml\Auth\Rest\Controller\AuthController;
use paml\Auth\Rest\Service\AuthAdapter;
use paml\Auth\Rest\Service\AuthorizationParamsParser;
use paml\Auth\Rest\Service\TokenService;
use paml\Auth\Service\AuthManager;
use Zend\ServiceManager\Factory\FactoryInterface;

class AuthControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): AuthController
    {
        return new AuthController(
            $container->get(AuthManager::class),
            $container->get(AuthorizationParamsParser::class),
            $container->get(AuthAdapter::class),
            $container->get(TokenService::class)
        );
    }
}
