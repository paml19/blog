<?php

namespace paml\Auth\Rest\Factory;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use paml\Auth\Rest\Entity\AccessToken;
use paml\Auth\Rest\Entity\User;
use paml\Auth\Rest\Service\TokenService;
use Zend\ServiceManager\Factory\FactoryInterface;

class TokenServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): TokenService
    {
        return new TokenService(
            $container->get(EntityManager::class)->getRepository(AccessToken::class),
            $container->get(EntityManager::class)->getRepository(User::class),
            $container->get('Config')['auth_rest']
        );
    }
}
