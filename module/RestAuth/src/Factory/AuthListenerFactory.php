<?php

namespace paml\Auth\Rest\Factory;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use paml\Auth\Rest\Entity\User;
use paml\Auth\Entity\User as BaseUser;
use paml\Auth\Rest\Listener\AuthListener;
use paml\Auth\Rest\Service\AuthAdapter;
use paml\Auth\Rest\Service\AuthorizationParamsParser;
use paml\Auth\Rest\Service\TokenService;
use paml\Auth\Service\AclService;
use paml\Auth\Service\AuthManager;
use Zend\ServiceManager\Factory\FactoryInterface;

class AuthListenerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): AuthListener
    {
        $aclService = $container->get(AclService::class);

        return new AuthListener(
            $container->get(EntityManager::class)->getRepository(User::class),
            $container->get(EntityManager::class)->getRepository(BaseUser::class),
            $container->get(AuthorizationParamsParser::class),
            $container->get(TokenService::class),
            $container->get(AuthAdapter::class),
            $container->get('Config')['controllers'],
            $container->get(AuthManager::class),
            $container->get('Config')['auth'],
            $container->get('Config')['auth_rest']['white_list'],
            $aclService->prepareAcl($container->get('Config'))
        );
    }
}
