<?php

namespace paml\Auth\Rest\Factory;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use paml\Auth\Rest\Service\AuthAdapter;
use paml\Auth\Entity\User as BaseUser;
use paml\Auth\Rest\Entity\User;
use Zend\ServiceManager\Factory\FactoryInterface;

class AuthAdapterFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): AuthAdapter
    {
        return new AuthAdapter(
            $container->get(EntityManager::class)->getRepository(BaseUser::class),
            $container->get(EntityManager::class)->getRepository(User::class)
        );
    }
}
