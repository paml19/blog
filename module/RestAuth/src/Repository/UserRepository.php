<?php

namespace paml\Auth\Rest\Repository;

use Doctrine\ORM\EntityRepository;
use paml\Auth\Rest\Entity\User;

class UserRepository extends EntityRepository
{
    public function save(User $user): void
    {
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();
    }

    public function merge(User $user): void
    {
        $this->getEntityManager()->merge($user);
        $this->getEntityManager()->flush();
    }

    public function delete(User $user): void
    {
        $this->getEntityManager()->remove($user);
        $this->getEntityManager()->flush();
    }
}
