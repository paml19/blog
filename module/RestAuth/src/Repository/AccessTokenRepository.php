<?php

namespace paml\Auth\Rest\Repository;

use Doctrine\ORM\EntityRepository;
use paml\Auth\Rest\Entity\AccessToken;

class AccessTokenRepository extends EntityRepository
{
    public function save(AccessToken $accessToken): void
    {
        $this->getEntityManager()->persist($accessToken);
        $this->getEntityManager()->flush();
    }

    public function merge(AccessToken $accessToken): void
    {
        $this->getEntityManager()->persist($accessToken);
        $this->getEntityManager()->flush();
    }

    public function delete(AccessToken $accessToken): void
    {
        $this->getEntityManager()->remove($accessToken);
        $this->getEntityManager()->flush();
    }
}
