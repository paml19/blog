<?php

namespace paml\Auth\Rest\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="auth_rest_access_token")
 * @ORM\Entity(repositoryClass="paml\Auth\Rest\Repository\AccessTokenRepository")
 * @GEDMO\Loggable
 * @Gedmo\SoftDeleteable(fieldName="dateDelete", timeAware=true)
 */
class AccessToken
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid", unique=true, nullable=false)
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;
    const ID = 'id';

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="date_add", type="datetime")
     */
    private $dateAdd;
    const DATE_ADD = 'dateAdd';

    /**
     * @ORM\Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;
    const DATE_DELETE = 'dateDelete';

    /**
     * @ORM\Column(name="access_token", type="string", length=200, unique=true, nullable=false)
     */
    private $accessToken;
    const ACCESS_TOKEN = 'accessToken';

    /**
     * @ORM\Column(name="date_expire", type="datetime", nullable=true)
     */
    private $dateExpire;
    const DATE_EXPIRE = 'dateExpire';

    /**
     * @ORM\ManyToOne(targetEntity="paml\Auth\Rest\Entity\User", inversedBy="accessTokens")
     * @ORM\JoinColumn(name="user", referencedColumnName="id")
     */
    private $user;
    const USER = 'user';

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getAccessToken(): ?string
    {
        return $this->accessToken;
    }

    public function setAccessToken(string $accessToken): self
    {
        $this->accessToken = $accessToken;
        return $this;
    }

    public function getDateExpire(): ?\DateTime
    {
        return $this->dateExpire;
    }

    public function setDateExpire(\DateTime $dateExpire): self
    {
        $this->dateExpire = $dateExpire;
        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;
        return $this;
    }

    public function getDateAdd(): ?\DateTime
    {
        return $this->dateAdd;
    }

    public function getDateDelete(): ?\DateTime
    {
        return $this->dateDelete;
    }

    public function toAuthArray(): array
    {
        return [
            self::ACCESS_TOKEN => $this->accessToken,
            self::DATE_EXPIRE => $this->dateExpire->format('d.m.Y H:i:s')
        ];
    }
}
