<?php

namespace paml\Auth\Rest\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="auth_rest_user")
 * @ORM\Entity(repositoryClass="paml\Auth\Rest\Repository\UserRepository")
 * @GEDMO\Loggable
 * @Gedmo\SoftDeleteable(fieldName="dateDelete", timeAware=true)
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid", unique=true, nullable=false)
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;
    const ID = 'id';

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="date_add", type="datetime")
     */
    private $dateAdd;
    const DATE_ADD = 'dateAdd';

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="date_edit", type="datetime", nullable=true)
     */
    private $dateEdit;
    const DATE_EDIT = 'dateEdit';

    /**
     * @ORM\Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;
    const DATE_DELETE = 'dateDelete';

    /**
     * @ORM\OneToOne(targetEntity="paml\Auth\Entity\User")
     * @ORM\JoinColumn(name="user", referencedColumnName="id")
     */
    private $user;
    const USER = 'user';

    /**
     * @ORM\OneToMany(targetEntity="paml\Auth\Rest\Entity\AccessToken", mappedBy="user")
     */
    private $accessTokens;
    const ACCESS_TOKENS = 'accessTokens';

    public function __construct()
    {
        $this->accessTokens = new ArrayCollection;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getDateAdd(): ?\DateTime
    {
        return $this->dateAdd;
    }

    public function getDateEdit(): ?\DateTime
    {
        return $this->dateEdit;
    }

    public function getDateDelete(): ?\DateTime
    {
        return $this->dateDelete;
    }

    public function getUser(): ?\paml\Auth\Entity\User
    {
        return $this->user;
    }

    public function setUser(\paml\Auth\Entity\User $user): self
    {
        $this->user = $user;
        return $this;
    }

    public function getAccessTokens(): Collection
    {
        return $this->accessTokens;
    }

    public function setAccessTokens(Collection $accessTokens): self
    {
        $this->accessTokens = $accessTokens;
        return $this;
    }

    public function addAccessToken(AccessToken $accessToken): self
    {
        if (! $this->accessTokens->contains($accessToken)) {
            $this->accessTokens->add($accessToken);
        }

        return $this;
    }

    public function removeAccessToken(AccessToken $accessToken): self
    {
        if ($this->accessTokens->contains($accessToken)) {
            $this->accessTokens->removeElement($accessToken);
        }

        return $this;
    }

    public function toAuthArray()
    {
        return [
            self::ID => $this->id,
            self::DATE_ADD => $this->dateAdd->format('d.m.Y H:i:s'),
            self::DATE_EDIT => $this->dateEdit ? $this->dateEdit->format('d.m.Y H:i:s') : null,
            self::DATE_DELETE => $this->dateDelete ? $this->dateDelete->format('d.m.Y H:i:s') : null,
            self::USER => $this->user->toAuthArray(),
        ];
    }
}
