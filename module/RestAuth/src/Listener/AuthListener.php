<?php

namespace paml\Auth\Rest\Listener;

use paml\Auth\Entity\Role;
use paml\Auth\Exception\AuthorizationException;
use paml\Auth\Module;
use paml\Auth\Repository\UserRepository as BaseUserRepository;
use paml\Auth\Rest\Entity\User;
use paml\Auth\Rest\Repository\UserRepository;
use paml\Auth\Rest\Service\AuthAdapter;
use paml\Auth\Rest\Service\AuthorizationParamsParser;
use paml\Auth\Rest\Service\TokenService;
use paml\Auth\Service\AuthManager;
use Zend\Authentication\Result;
use Zend\EventManager\EventInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\ListenerAggregateTrait;
use Zend\Mvc\Controller\AbstractController;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\Mvc\MvcEvent;
use Zend\Permissions\Acl\Acl;

class AuthListener implements ListenerAggregateInterface
{
    use ListenerAggregateTrait;

    private $userRepository;

    private $mainUserRepository;

    private $authorizationParamsParser;

    private $tokenService;

    private $authAdapter;

    private $controllersConfig;

    private $authManager;

    private $authConfig;

    private $whiteList;

    private $acl;

    public function __construct(
        UserRepository $userRepository,
        BaseUserRepository $mainUserRepository,
        AuthorizationParamsParser $authorizationParamsParser,
        TokenService $tokenService,
        AuthAdapter $authAdapter,
        array $controllersConfig,
        AuthManager $authManager,
        array $authConfig,
        array $whiteList,
        Acl $acl
    ) {
        $this->userRepository = $userRepository;
        $this->mainUserRepository = $mainUserRepository;
        $this->authorizationParamsParser = $authorizationParamsParser;
        $this->tokenService = $tokenService;
        $this->authAdapter = $authAdapter;
        $this->controllersConfig = $controllersConfig;
        $this->authManager = $authManager;
        $this->authConfig = $authConfig;
        $this->whiteList = $whiteList;
        $this->acl = $acl;
    }

    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $sharedEventManager = $events->getSharedManager();

        $sharedEventManager->attach(
            AbstractController::class,
            Module::AUTH_USER_LOGGED_IN,
            [
                $this,
                'userLogged',
            ],
            $priority
        );

        $sharedEventManager->attach(
            AbstractController::class,
            Module::AUTH_USER_REGISTERED_IN,
            [
                $this,
                'userRegistered',
            ],
            $priority
        );

        $sharedEventManager->attach(
            AbstractRestfulController::class,
            MvcEvent::EVENT_DISPATCH,
            [
                $this,
                'onDispatch',
            ],
            $priority
        );
    }

    public function onDispatch(MvcEvent $event): void
    {
        if ($event->getRouteMatch()) {
            $controllerName = $event->getRouteMatch()->getParam('controller', null);

            if (! $this->authManager->hasIdentity()
                &&
                (
                    is_subclass_of($controllerName, AbstractRestfulController::class)
                    ||
                    is_subclass_of($this->controllersConfig['aliases'][$controllerName], AbstractRestfulController::class)
                )
            ) {
                if (! in_array($event->getRequest()->getServer()->get('REMOTE_ADDR'), $this->whiteList)) {
                    $authorizationParams = $this->authorizationParamsParser->getType($event->getTarget()->params());
                    $accessToken = null;
                    $userIdentity = null;

                    if ($authorizationParams == 'Bearer') {
                        $authorizationString = $this->authorizationParamsParser->parseBearerAuth($event->getTarget()->params());

                        $accessToken = $this->tokenService->validateAccessToken($authorizationString);

                        if (! $accessToken) {
                            $this->notPermitted();
                        }
                    } elseif ($authorizationParams == 'Basic') {
                        $authorizationParams = $this->authorizationParamsParser->parseBaseAuth($event->getTarget()->params());
                        $result = $this->authManager->login(
                            $authorizationParams->get('login'),
                            $authorizationParams->get('password'),
                            null,
                            $this->authAdapter,
                            null
                        );

                        if ($result->getCode() != Result::SUCCESS) {
                            $this->notPermitted();
                        }

                        $userIdentity = $result->getIdentity();
                    }

                    $roles = null;

                    if ($accessToken) {
                        $roles = $accessToken->getUser()->getUser()->getRoles();
                    } elseif ($userIdentity) {
                        $roles = $this->userRepository->find($userIdentity['id'])->getRoles();
                    }

                    $actionName = $event->getRouteMatch()->getParam('action', null);
                    $actionName = str_replace('-', '', lcfirst(ucwords($actionName, '-')));
                    $accessGuard = false;

                    /** @var Role $role */
                    foreach ($roles as $role) {
                        if (! $this->acl->hasRole($role->getSlug())) {
                            continue;
                        }

                        if ($this->acl->isAllowed($role->getSlug(), $controllerName, $actionName)) {
                            $accessGuard = true;
                            break;
                        }
                    }

                    if (! $accessGuard) {
                        $this->notPermitted();
                    }
                }
            }
        }
    }

    public function userLogged(EventInterface $event): void
    {
        $baseUserId = $event->getParam('userId');
        $baseUser = $this->mainUserRepository->find($baseUserId);

        /** @var User $user */
        $user = $this->userRepository->findOneBy(['user' => $baseUser]);
        if (! $user) {
            $user = (new User)
                ->setUser($baseUser);

            $this->userRepository->save($user);
        } else {
            $user->setUser($baseUser);

            $this->userRepository->merge($user);
        }
    }

    public function userRegistered(EventInterface $event): void
    {
        $baseUser = $event->getParam('user');

        $user = $this->userRepository->findOneBy(['user' => $baseUser]);
        if (! $user) {
            $user = (new User)
                ->setUser($baseUser);

            $this->userRepository->save($user);
        } else {
            $user->setUser($user);

            $this->userRepository->merge($user);
        }
    }

    private function notPermitted()
    {
        $systemAdminMail = null;

        if (isset($this->authConfig['system_admin_mail'])) {
            $systemAdminMail = $this->authConfig['system_admin_mail'];
        }

        throw new \Exception(
            'Skontaktuj się z administratorem systemu ' . $systemAdminMail,
            5002
        );
    }
}
