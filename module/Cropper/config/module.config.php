<?php

namespace paml\File\Image\Cropper;

use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Zend\Form\ElementFactory;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Domain/Model'],
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Domain\Model' => __NAMESPACE__ . '_driver',
                ],
            ],
        ],
    ],
    'form_elements' => [
        'factories' => [
            Presentation\Form\Element\Image::class => ElementFactory::class,
        ],
    ],
    'service_manager' => [
        Application\CommandBus::class => Application\Factory\CommandBusFactory::class,
        Application\Command\SaveCroppedImageHandler::class => Application\Factory\Handler\SaveCroppedImageHandlerFactory::class,
    ],
    'view_helpers' => [
        'factories' => [
            Presentation\Form\View\FormImageCrop::class => Presentation\Factory\Form\View\FormImageCropFactory::class,
        ],
        'aliases' => [
            'formImageCrop' => Presentation\Form\View\FormImageCrop::class,
        ],
    ],
    'view_manager' => [
        'template_map' => [
            'cropper/image' => __DIR__ . '/../view/form/image.phtml',
        ],
    ],
];
