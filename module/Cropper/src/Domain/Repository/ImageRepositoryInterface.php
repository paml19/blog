<?php

declare(strict_types=1);

namespace paml\File\Image\Cropper\Domain\Repository;

use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use paml\File\Image\Cropper\Domain\Model\Image;

/**
 * Interface ImageRepositoryInterface
 * @package paml\File\FormImage\Cropper\Domain\Repository
 */
interface ImageRepositoryInterface
{
    /**
     * @param Image $image
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(Image $image): void;

    /**
     * @param $id
     * @return Image|null
     */
    public function find($id);
}