<?php

declare(strict_types=1);

namespace paml\File\Image\Cropper\Domain\Model;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use paml\File\Entity\File;

/**
 * Class Image
 * @package paml\File\FormImage\Cropper\Domain\Model
 *
 * @ORM\Entity(repositoryClass="paml\File\Image\Cropper\Infrastructure\Repository\ImageRepository")
 * @ORM\Table(name="file_cropped_image")
 * @Gedmo\SoftDeleteable(fieldName="dateDelete", timeAware=true)
 */
class Image
{
    const UPLOAD_PATH = '/cropped/';

    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="guid", unique=true, nullable=false)
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;
    const ID = 'id';

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     */
    private $name;
    const NAME = 'name';

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    private $size;
    const SIZE = 'size';

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="date_add", type="datetime")
     */
    private $dateAdd;
    const DATE_ADD = 'dateAdd';

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="date_edit", type="datetime", nullable=true)
     */
    private $dateEdit;
    const DATE_EDIT = 'dateEdit';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;
    const DATE_DELETE = 'dateDelete';

    /**
     * @var File
     *
     * @ORM\OneToOne(targetEntity="\paml\File\Entity\File")
     * @ORM\JoinColumn(name="file", referencedColumnName="id")
     */
    private $file;
    const FILE = 'file';

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     */
    private $dimensionX;
    const DIMENSION_X = 'dimensionX';

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     */
    private $dimensionY;
    const DIMENSION_Y = 'dimensionY';

    /**
     * FormImage constructor.
     * @param string $id
     * @param string $name
     * @param int $size
     * @param File $file
     * @param int $dimensionX
     * @param int $dimensionY
     */
    public function __construct(string $id, string $name, int $size, File $file, int $dimensionX, int $dimensionY)
    {
        $this->id = $id;
        $this->name = $name;
        $this->size = $size;
        $this->file = $file;
        $this->dimensionX = $dimensionX;
        $this->dimensionY = $dimensionY;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getSize(): int
    {
        return $this->size;
    }

    /**
     * @return File
     */
    public function getFile(): File
    {
        return $this->file;
    }

    /**
     * @return int
     */
    public function getDimensionX(): int
    {
        return $this->dimensionX;
    }

    /**
     * @return int
     */
    public function getDimensionY(): int
    {
        return $this->dimensionY;
    }

    /**
     * @return \DateTime
     */
    public function getDateAdd(): \DateTime
    {
        return $this->dateAdd;
    }

    /**
     * @return \DateTime
     */
    public function getDateEdit(): ?\DateTime
    {
        return $this->dateEdit;
    }

    /**
     * @return \DateTime
     */
    public function getDateDelete(): ?\DateTime
    {
        return $this->dateDelete;
    }

    /**
     * @param string $name
     * @return Image
     */
    public function setName(string $name): Image
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @param int $size
     * @return Image
     */
    public function setSize(int $size): Image
    {
        $this->size = $size;
        return $this;
    }

    /**
     * @param int $dimensionX
     * @return Image
     */
    public function setDimensionX(int $dimensionX): Image
    {
        $this->dimensionX = $dimensionX;
        return $this;
    }

    /**
     * @param int $dimensionY
     * @return Image
     */
    public function setDimensionY(int $dimensionY): Image
    {
        $this->dimensionY = $dimensionY;
        return $this;
    }
}
