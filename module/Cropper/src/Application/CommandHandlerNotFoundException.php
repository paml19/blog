<?php

declare(strict_types=1);

namespace paml\File\Image\Cropper\Application;

/**
 * Class CommandHandlerNotFoundException
 * @package paml\File\FormImage\Cropper\Application
 */
class CommandHandlerNotFoundException extends \Exception
{

}