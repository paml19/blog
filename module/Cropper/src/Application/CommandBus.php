<?php

declare(strict_types=1);

namespace paml\File\Image\Cropper\Application;

use Interop\Container\ContainerInterface;

/**
 * Class CommandBus
 * @package paml\File\FormImage\Cropper\Application
 */
class CommandBus
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * CommandBus constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param object $command
     * @throws CommandHandlerNotFoundException
     */
    public function handle(object $command): void
    {
        $handlerName = $this->getHandlerName($command);

        if (! $this->container->has($handlerName)) {
            throw new CommandHandlerNotFoundException(get_class($command));
        }

        $handler = $this->container->get($handlerName);
        $handler->handle($command);
    }

    /**
     * @param object $command
     * @return string
     */
    public function getHandlerName(object $command): string
    {
        $commandClass = get_class($command);
        return substr($commandClass, 0, strpos($commandClass, 'Command')) . 'Handler';
    }
}