<?php

declare(strict_types=1);

namespace paml\File\Image\Cropper\Application\Command;

use paml\File\Entity\File;

/**
 * Class SaveCroppedImageCommand
 * @package paml\File\FormImage\Cropper\Application\Command
 */
final class SaveCroppedImageCommand
{
    /** @var string */
    public $id;

    /** @var string */
    public $name;

    /** @var integer */
    public $size;

    /** @var File */
    public $file;

    /** @var integer */
    public $dimensionX;

    /** @var integer */
    public $dimensionY;

    /**
     * @param string $id
     * @param string $name
     * @param int $size
     * @param File $file
     * @param int $dimensionX
     * @param int $dimensionY
     */
    public function __construct(string $id, string $name, int $size, File $file, int $dimensionX, int $dimensionY)
    {
        $this->id = $id;
        $this->name = $name;
        $this->size = $size;
        $this->file = $file;
        $this->dimensionX = $dimensionX;
        $this->dimensionY = $dimensionY;
    }
}
