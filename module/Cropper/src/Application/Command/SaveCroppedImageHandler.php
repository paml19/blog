<?php

declare(strict_types=1);

namespace paml\File\Image\Cropper\Application\Command;

use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use paml\File\Image\Cropper\Domain\Model\Image;
use paml\File\Image\Cropper\Domain\Repository\ImageRepositoryInterface;

/**
 * Class SaveCroppedImageHandler
 * @package paml\File\FormImage\Cropper\Application\Handler
 */
final class SaveCroppedImageHandler
{
    /**
     * @var ImageRepositoryInterface
     */
    private $imageRepository;

    /**
     * SaveCroppedImageHandler constructor.
     * @param ImageRepositoryInterface $imageRepository
     */
    public function __construct(ImageRepositoryInterface $imageRepository)
    {
        $this->imageRepository = $imageRepository;
    }

    /**
     * @param SaveCroppedImageCommand $command
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function handle(SaveCroppedImageCommand $command): void
    {
        $image = $this->imageRepository->find($command->id);

        if (! $image) {
            $image = new Image(
                $command->id,
                $command->name,
                $command->size,
                $command->file,
                $command->dimensionX,
                $command->dimensionY
            );
        }

        if ($image->getName() != $command->name) {
            $image->setName($command->name);
        }

        if ($image->getSize() != $command->size) {
            $image->setSize($command->size);
        }

        if ($image->getDimensionX() != $command->dimensionX) {
            $image->setDimensionX($command->dimensionX);
        }

        if ($image->getDimensionY() != $command->dimensionY) {
            $image->setDimensionY($command->dimensionY);
        }

        $this->imageRepository->save($image);
    }
}