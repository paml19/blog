<?php

declare(strict_types=1);

namespace paml\File\Image\Cropper\Application\Factory\Handler;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use paml\File\Image\Cropper\Application\Command\SaveCroppedImageHandler;
use paml\File\Image\Cropper\Domain\Model\Image;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class SaveCroppedImageHandlerFactory
 * @package paml\File\FormImage\Cropper\Application\Factory\Handler
 */
class SaveCroppedImageHandlerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return SaveCroppedImageHandler
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): SaveCroppedImageHandler
    {
        return new SaveCroppedImageHandler($container->get(EntityManager::class)->getRepository(Image::class));
    }
}