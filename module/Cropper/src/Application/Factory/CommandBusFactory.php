<?php

declare(strict_types=1);

namespace paml\File\Image\Cropper\Application\Factory;

use Interop\Container\ContainerInterface;
use paml\File\Image\Cropper\Application\CommandBus;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class CommandBusFactory
 * @package paml\File\FormImage\Cropper\Application\Factory
 */
class CommandBusFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return CommandBus
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): CommandBus
    {
        return new CommandBus($container);
    }
}