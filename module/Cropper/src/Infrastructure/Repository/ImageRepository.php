<?php

declare(strict_types=1);

namespace paml\File\Image\Cropper\Infrastructure\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use paml\File\Image\Cropper\Domain\Model\Image;
use paml\File\Image\Cropper\Domain\Repository\ImageRepositoryInterface;

/**
 * Class ImageRepository
 * @package paml\File\FormImage\Cropper\Infrastructure\Repository
 */
class ImageRepository extends EntityRepository implements ImageRepositoryInterface
{
    /**
     * @param Image $image
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(Image $image): void
    {
        $this->getEntityManager()->persist($image);
        $this->getEntityManager()->flush($image);
    }
}