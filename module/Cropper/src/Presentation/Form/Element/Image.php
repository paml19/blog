<?php

declare(strict_types=1);

namespace paml\File\Image\Cropper\Presentation\Form\Element;

use Zend\Form\Element;

/**
 * Class FormImage
 * @package paml\File\FormImage\Cropper\Presentation\Form\Element
 */
class Image extends Element\Text
{
    /**
     * @var array
     */
    protected $attributes = [
        'type' => 'hidden',
        'value' => '',
        'class' => 'js-image-cropper-value'
    ];
}