<?php

declare(strict_types=1);

namespace paml\File\Image\Cropper\Presentation\Form\View;

use Zend\Form\ElementInterface;
use Zend\Form\View\Helper\AbstractHelper;
use Zend\Form\View\Helper\FormInput;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;

/**
 * Class FormImage
 * @package paml\File\Image\Cropper\Presentation\Form\View
 */
class FormImageCrop extends AbstractHelper
{
    public function __invoke(ElementInterface $element = null)
    {
        if (! $element) {
            return $this;
        }

        return $this->render($element);
    }

    /**
     * @param ElementInterface $element
     * @return string
     */
    public function render(ElementInterface $element)
    {
        $element = (new ViewModel)
            ->setVariables([
                'element' => $element,
            ])
            ->setTemplate('cropper/image');

        return 'asdfasdf';
    }
}