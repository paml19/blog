<?php

declare(strict_types=1);

namespace paml\File\Image\Cropper\Presentation\Factory\Form\View;

use Interop\Container\ContainerInterface;
use paml\File\Image\Cropper\Presentation\Form\View\FormImageCrop;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\View\Renderer\PhpRenderer;

/**
 * Class FormImageFactory
 * @package paml\File\Image\Cropper\Presentation\Factory\Form\View
 */
class FormImageCropFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return FormImageCrop
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): FormImageCrop
    {
        $formImage = new FormImageCrop;

        $formImage->setPhpRenderer($container->get(PhpRenderer::class));

        return  $formImage;
    }
}