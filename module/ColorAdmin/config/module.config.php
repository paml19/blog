<?php
namespace paml\Layouts\ColorAdmin;

use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\AbstractFactory\ReflectionBasedAbstractFactory;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'asset_manager' => [
        'resolver_configs' => [
            'paths' => [
                'paml\Layouts\ColorAdmin' => __DIR__ . '/../public',
            ],
        ],
    ],
    'controller_plugins' => [
        'factories' => [
            Plugin\DataTableButtons::class => ReflectionBasedAbstractFactory::class,
        ],
        'aliases' => [
            'buttons' => Plugin\DataTableButtons::class,
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\NotificationController::class => Factory\NotificationControllerFactory::class,
        ],
    ],
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Entity'],
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver',
                ],
            ],
        ],
    ],
    'router' => [
        'routes' => [
            'notification' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/notification/:action[/:id]',
                    'defaults' => [
                        'controller' => Controller\NotificationController::class,
                        'action' => 'index',
                    ]
                ],
            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            Listener\LayoutListener::class => Factory\LayoutListenerFactory::class,
            Service\Query\DataTableService::class => InvokableFactory::class,
        ],
    ],
    'view_helpers' => [
        'factories' => [
            Helper\DataTableHelper::class => Factory\DataTableHelperFactory::class,
            Helper\ExistsHelper::class => Factory\ExistsHelperFactory::class,
            Helper\PageHeaderHelper::class => ReflectionBasedAbstractFactory::class,
            Helper\LabelHelper::class => ReflectionBasedAbstractFactory::class,
            Helper\BadgeHelper::class => ReflectionBasedAbstractFactory::class,
            Helper\PanelHelper::class => ReflectionBasedAbstractFactory::class,
            Form\Helper\FormHorizontal::class => InvokableFactory::class,
            Helper\AgoHelper::class => InvokableFactory::class,
        ],
        'aliases' => [
            'dataTable' => Helper\DataTableHelper::class,
            'systemNotifications' => Helper\SystemNotificationHelper::class,
            'exists' => Helper\ExistsHelper::class,
            'pageHeader' => Helper\PageHeaderHelper::class,
            'label' => Helper\LabelHelper::class,
            'badge' => Helper\BadgeHelper::class,
            'panel' => Helper\PanelHelper::class,
            'horizontalForm' => Form\Helper\FormHorizontal::class,
            'ago' => Helper\AgoHelper::class,
        ],
    ],
    'view_manager' => [
        'template_map' => [
            'layout/color-admin' => __DIR__ . '/../view/layout/color-admin.phtml',
            'layout/color-admin-auth' => __DIR__ . '/../view/layout/color-admin-auth.phtml',
            'color-admin/element/sidebar' => __DIR__ . '/../view/partial/sidebar.phtml',
            'color-admin/element/sidebar/navigation' => __DIR__ . '/../view/partial/sidebar/navigation.phtml',
            'color-admin/element/sidebar/user' => __DIR__ . '/../view/partial/sidebar/user.phtml',
            'color-admin/element/breadcrumb' => __DIR__ . '/../view/partial/breadcrumb.phtml',
            'color-admin/element/header' => __DIR__ . '/../view/partial/header.phtml',
            'color-admin/element/header/searcher' => __DIR__ . '/../view/partial/header/searcher.phtml',
            'color-admin/element/header/notifications' => __DIR__ . '/../view/partial/header/notifications.phtml',
            'color-admin/element/header/user' => __DIR__ . '/../view/partial/header/user.phtml',
            'color-admin/element/flash-messages' => __DIR__ . '/../view/partial/flash-messages.phtml',
            'color-admin/helper/data-table/styles' => __DIR__ . '/../view/helper/data-table/styles.phtml',
            'color-admin/helper/data-table/main-scripts' => __DIR__ . '/../view/helper/data-table/main-scripts.phtml',
            'color-admin/helper/data-table/table' => __DIR__ . '/../view/helper/data-table/table.phtml',
            'color-admin/helper/data-table/table-script' => __DIR__ . '/../view/helper/data-table/table-script.phtml',
            'color-admin/helper/data-table/filters' => __DIR__ . '/../view/helper/data-table/filters.phtml',
            'color-admin/helper/data-table/extended/buttons' => __DIR__ . '/../view/helper/data-table/extended/data-table-buttons.phtml',
            'color-admin/helper/notification/notification' => __DIR__ . '/../view/helper/notification/notification.phtml',
            'color-admin/helper/simple/page-header' => __DIR__ . '/../view/helper/simple/page-header.phtml',
            'color-admin/helper/simple/label' => __DIR__ . '/../view/helper/simple/label.phtml',
            'color-admin/helper/simple/badge' => __DIR__ . '/../view/helper/simple/badge.phtml',
            'color-admin/helper/panel' => __DIR__ . '/../view/helper/panel/panel.phtml',
            'color-admin/helper/panel/header' => __DIR__ . '/../view/helper/panel/heading.phtml',
            'color-admin/helper/panel/footer' => __DIR__ . '/../view/helper/panel/footer.phtml',
            'color-admin/email/verify' => __DIR__ . '/../view/email/verify-email.phtml',
        ],
        'template_path_stack' => [
            'paml\Layouts\ColorAdmin' => __DIR__ . '/../view',
        ],
    ],
];
