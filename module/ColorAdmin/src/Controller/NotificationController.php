<?php

namespace paml\Layouts\ColorAdmin\Controller;

use Doctrine\ORM\EntityManager;
use paml\Layouts\ColorAdmin\Entity\SystemNotification;
use paml\Layouts\ColorAdmin\Repository\SystemNotificationRepository;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class NotificationController extends AbstractActionController
{
    private $systemNotificationRepository;

    private $userEntity;

    private $entityManager;

    private $extraEntity;

    public function __construct(
        SystemNotificationRepository $systemNotificationRepository,
        ?string $userEntity,
        EntityManager $entityManager,
        ?string $extraEntity
    ) {
        $this->systemNotificationRepository = $systemNotificationRepository;
        $this->userEntity = $userEntity;
        $this->entityManager = $entityManager;
        $this->extraEntity = $extraEntity;
    }

    public function listAction()
    {
        $userId = $this->identity()['id'];
        $notifications = $this->systemNotificationRepository->findUsersNotifications($this->userEntity, $userId);

        return new ViewModel([
            'notifications' => $notifications
        ]);
    }

    public function showAction()
    {
        $id = $this->params()->fromRoute('id');
        /** @var SystemNotification $notification */
        $notification = $this->systemNotificationRepository->find($id);
        $object = null;

        if ($notification->getObject()) {
            $object = $this->entityManager->find($notification->getObject(), $notification->getObjectId());
        }

        return new ViewModel([
            'notification' => $notification,
            'object' => $object
        ]);
    }

    public function viewAction()
    {
        $request = $this->getRequest();
        $model = new JsonModel;

        if ($request->isPost()) {
            $model->setVariable('status', 'ok');
            $data = $request->getPost();
            foreach ($data['notifications'] as $item) {
                $notification = $this->entityManager->getRepository($this->extraEntity)->findOneBy([
                    'notification' => $item,
                    'user' => $this->entityManager->getRepository($this->userEntity)->findOneBy([
                        'user' => $this->identity()['id']
                    ])
                ]);
                $notification->setViewed(true);
            }
            $this->entityManager->flush();
        }

        return $model;
    }
}
