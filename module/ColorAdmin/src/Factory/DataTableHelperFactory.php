<?php

namespace paml\Layouts\ColorAdmin\Factory;

use Interop\Container\ContainerInterface;
use paml\Layouts\ColorAdmin\Helper\DataTableHelper;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\View\Renderer\PhpRenderer;

class DataTableHelperFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new DataTableHelper(
            $container->get(PhpRenderer::class)
        );
    }
}
