<?php

namespace paml\Layouts\ColorAdmin\Factory;

use Interop\Container\ContainerInterface;
use paml\Layouts\ColorAdmin\Listener\LayoutListener;
use Zend\ServiceManager\Factory\FactoryInterface;

class LayoutListenerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        if (! isset($container->get('Config')['layouts']['color-admin']['use-template'])) {
            throw new \Exception('No layout color_admin use_template value');
        }

        return new LayoutListener(
            $container->get('Config')['layouts']['color-admin']['use-template'],
            $container->get('Config')['layouts']['color-admin']['navigation-name']
        );
    }
}
