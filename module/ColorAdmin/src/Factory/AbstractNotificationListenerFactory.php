<?php

namespace paml\Layouts\ColorAdmin\Factory;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use paml\Layouts\ColorAdmin\Entity\SystemNotification;
use paml\Layouts\ColorAdmin\Service\Listener\NotificationListenerProperties;
use paml\SessionTranslator\Service\TranslatorService;
use Zend\Mvc\Plugin\FlashMessenger\FlashMessenger;
use Zend\ServiceManager\Factory\FactoryInterface;

abstract class AbstractNotificationListenerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('Config');

        if (! isset($config['layouts']['events'])) {
            throw new \Exception('Events not implemented in config!');
        }

        if (! isset($config['layouts']['extra-entity'])) {
            $config['layouts']['extra-entity'] = null;
        }

        if (! isset($config['layouts']['extra-params'])) {
            $config['layouts']['extra-params'] = null;
        }

        if (! isset($config['layouts']['role-based'])) {
            $config['layouts']['role-based'] = false;
        }

        return new NotificationListenerProperties(
            $config['layouts']['events'],
            $container->get('ControllerPluginManager')->get(FlashMessenger::class),
            $container->get(EntityManager::class)->getRepository(SystemNotification::class),
            $container->get(EntityManager::class),
            $config['layouts']['extra-entity'],
            $config['layouts']['extra-params'],
            $container->get(TranslatorService::class),
            $config['layouts']['role-based']
        );
    }
}
