<?php

namespace paml\Layouts\ColorAdmin\Factory;

use Interop\Container\ContainerInterface;
use paml\Layouts\ColorAdmin\Helper\ExistsHelper;
use Zend\ServiceManager\Factory\FactoryInterface;

class ExistsHelperFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new ExistsHelper(
            $container
        );
    }
}