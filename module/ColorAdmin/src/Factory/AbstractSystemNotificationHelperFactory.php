<?php

namespace paml\Layouts\ColorAdmin\Factory;

use Interop\Container\ContainerInterface;
use paml\Layouts\ColorAdmin\Service\Helper\SystemNotificationHelperProperties;
use paml\SessionTranslator\Service\TranslatorService;
use Zend\Mvc\Plugin\Identity\Identity;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\View\Renderer\PhpRenderer;

abstract class AbstractSystemNotificationHelperFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('Config');

        if (! isset($config['layouts']['extra-entity'])) {
            $config['layouts']['extra-entity'] = null;
        }

        if (! isset($config['layouts']['extra-params'])) {
            $config['layouts']['extra-params'] = null;
        }

        return new SystemNotificationHelperProperties(
            $container->get(PhpRenderer::class),
            $container->get('ControllerPluginManager')->get(Identity::class),
            $container->get(TranslatorService::class),
            $config['layouts']['extra-entity'],
            $config['layouts']['extra-params']
        );
    }
}
