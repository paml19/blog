<?php

namespace paml\Layouts\ColorAdmin\Factory;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use paml\Auth\Entity\User;
use paml\Layouts\ColorAdmin\Controller\NotificationController;
use paml\Layouts\ColorAdmin\Entity\SystemNotification;
use Zend\ServiceManager\Factory\FactoryInterface;

class NotificationControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('Config');
        $entityManager = $container->get(EntityManager::class);

        if (! isset($config['layouts']['user-entity'])) {
            throw new \Exception('No user-entity implemented!');
        }

        if (! isset($config['layouts']['extra-entity'])) {
            $config['layouts']['extra-entity'] = null;
        }

        return new NotificationController(
            $entityManager->getRepository(SystemNotification::class),
            $config['layouts']['user-entity'],
            $entityManager,
            $config['layouts']['extra-entity']
        );
    }
}
