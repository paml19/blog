<?php

namespace paml\Layouts\ColorAdmin\Listener;

use paml\Auth\Entity\Role;
use paml\Auth\Module;
use paml\Layouts\ColorAdmin\Entity\Interfaces\UserNotificationInterface;
use paml\Layouts\ColorAdmin\Entity\SystemNotification;
use paml\Layouts\ColorAdmin\Repository\Interfaces\UserSystemNotificationRepositoryInterface;
use paml\Layouts\ColorAdmin\Service\Listener\NotificationListenerProperties;
use Zend\EventManager\EventInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\ListenerAggregateTrait;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\Controller\AbstractController;

class NotificationListener implements ListenerAggregateInterface
{
    use ListenerAggregateTrait;

    private $events;

    private $flashMessenger;

    private $systemNotificationRepository;

    private $userSystemNotificationRepository;

    private $extraEntity;

    private $extraParams;

    private $entityManager;

    private $translator;

    private $roleBased;

    public function __construct(
        NotificationListenerProperties $notificationListenerProperties,
        UserSystemNotificationRepositoryInterface $userSystemNotificationRepository
    ) {
        $this->events = $notificationListenerProperties->getEvents();
        $this->flashMessenger = $notificationListenerProperties->getFlashMessenger();
        $this->systemNotificationRepository = $notificationListenerProperties->getSystemNotificationRepository();
        $this->userSystemNotificationRepository = $userSystemNotificationRepository;
        $this->entityManager = $notificationListenerProperties->getEntityManager();
        $this->extraEntity = $notificationListenerProperties->getExtraEntity();
        $this->extraParams = $notificationListenerProperties->getExtraParams();
        $this->translator = $notificationListenerProperties->getTranslatorService()->getTranslator();
        $this->roleBased = $notificationListenerProperties->isRoleBased();
    }

    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $sharedEventManager = $events->getSharedManager();

        foreach ($this->events as $event) {
            $sharedEventManager->attach(
                AbstractController::class,
                $event,
                [
                    $this,
                    'onDispatch',
                ],
                $priority
            );
        }

        $sharedEventManager->attach(
            AbstractActionController::class,
            Module::AUTH_USER_LOGGED_IN,
            [
                $this,
                'onUserLoggedIn',
            ],
            $priority
        );
    }

    public function onDispatch(EventInterface $event)
    {
        $text = $event->getParam('text');
        $namespace = $event->getParam('namespace');
        $object = $event->getParam('object', null);
        $roles = $event->getParam('roles', null);
        $this->flashMessenger->addMessage($this->translator->translate($text), $this->translator->translate($namespace));

        $notification = (new SystemNotification)
            ->setEvent($event->getName())
            ->setNotification([
                'text' => $text,
                'namespace' => $namespace
            ]);

        if ($object) {
            $notification->setObject(get_class($object))
                ->setObjectId($object->getId());
        }

        $this->systemNotificationRepository->save($notification);

        $users = $this->userSystemNotificationRepository->findAllWithSystemNotifications();

        /** @var UserNotificationInterface $user */
        foreach ($users as $user) {
            if ($this->roleBased) {
                $userRolesCollection = $user->getUser()->getRoles();
                $userRoles = [];
                /** @var Role $item */
                foreach ($userRolesCollection as $item) {
                    $userRoles[] = $item->getSlug();
                }

                if ($roles) {
                    foreach ($roles as $role) {
                        if (in_array($role, $userRoles)) {
                            if (in_array('addSystemNotification', get_class_methods($user)) !== false) {
                                $user->addSystemNotification($notification);
                                $this->userSystemNotificationRepository->save($user);
                            } else {
                                $notifyOptions = (new $this->extraEntity)
                                    ->setUser($user)
                                    ->setNotification($notification);

                                $this->entityManager->persist($notifyOptions);
                                $this->entityManager->flush();
                            }
                        }
                    }
                }
            } else {
                if (in_array('addSystemNotification', get_class_methods($user)) !== false) {
                    $user->addSystemNotification($notification);
                    $this->userSystemNotificationRepository->save($user);
                } else {
                    $notifyOptions = (new $this->extraEntity)
                        ->setUser($user)
                        ->setNotification($notification);

                    $this->entityManager->persist($notifyOptions);
                    $this->entityManager->flush();
                }
            }
        }
    }

    public function onUserLoggedIn(EventInterface $event)
    {
        $userId = $event->getParam('userId', null);

        if ($userId) {
            $notifications = $this->userSystemNotificationRepository->findUsersSystemNotifications(
                $userId,
                5,
                $this->extraEntity,
                $this->extraParams);

            if (isset($notifications)) {
                /** @var SystemNotification $notification */
                foreach ($notifications as $notification) {
                    $this->flashMessenger->addMessage(
                        $this->translator->translate($notification->getNotification()['text']),
                        $this->translator->translate($notification->getNotification()['namespace'])
                    );
                }
            }
        }
    }
}
