<?php

namespace paml\Layouts\ColorAdmin\Listener;

use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\ListenerAggregateTrait;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;

class LayoutListener implements ListenerAggregateInterface
{
    use ListenerAggregateTrait;

    private $useTemplate = true;

    private $navigationName;

    public function __construct(bool $useTemplate, ?string $navigationName)
    {
        $this->useTemplate = $useTemplate;
        $this->navigationName = $navigationName;
    }

    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $sharedEventManager = $events->getSharedManager();

        $sharedEventManager->attach(
            AbstractActionController::class,
            MvcEvent::EVENT_DISPATCH,
            [
                $this,
                'onDispatch',
            ],
            $priority
        );
    }

    public function onDispatch(MvcEvent $mvcEvent)
    {
        if ($this->useTemplate) {
            $viewModel = $mvcEvent->getViewModel();
            $viewModel->setTemplate('layout/color-admin');
        }

        if ($this->navigationName) {
            $viewModel = $mvcEvent->getViewModel();
            $viewModel->setVariable('navigationName', $this->navigationName);
        }
    }
}
