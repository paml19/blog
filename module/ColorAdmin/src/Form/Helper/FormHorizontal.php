<?php

namespace paml\Layouts\ColorAdmin\Form\Helper;

use Zend\Form\FormInterface;
use Zend\Form\View\Helper\Form;
use Zend\View\Helper\Doctype;

class FormHorizontal extends Form
{
    public function openTag(FormInterface $form = null)
    {
        $doctype    = $this->getDoctype();
        $attributes = [];

        if (! (Doctype::HTML5 === $doctype || Doctype::XHTML5 === $doctype)) {
            $attributes = [
                'action' => '',
                'method' => 'get',
            ];
        }

        if ($form instanceof FormInterface) {
            $formAttributes = $form->getAttributes();
            if (! array_key_exists('id', $formAttributes) && array_key_exists('name', $formAttributes)) {
                $formAttributes['id'] = $formAttributes['name'];
            }
            $attributes = array_merge($attributes, $formAttributes);
        }

        if ($attributes) {
            return sprintf('<form class="form-horizontal" %s>', $this->createAttributesString($attributes));
        }

        return '<form class="form-horizontal">';
    }

}