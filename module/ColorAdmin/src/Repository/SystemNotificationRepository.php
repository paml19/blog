<?php

namespace paml\Layouts\ColorAdmin\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use paml\Layouts\ColorAdmin\Entity\SystemNotification;

class SystemNotificationRepository extends EntityRepository
{
    public function save(SystemNotification $systemNotification): void
    {
        $this->getEntityManager()->persist($systemNotification);
        $this->getEntityManager()->flush();
    }

    public function findUsersNotifications(string $userEntity, string $userId): array
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $queryBuilder->select('n, ue')
            ->from($userEntity, 'ue')
            ->join('ue.user', 'u')
            ->join('ue.systemNotifications', 'n')
            ->where($queryBuilder->expr()->eq('u.id', ':userId'))
            ->andWhere($queryBuilder->expr()->isNotNull('en.viewed'))
            ->setParameter(':userId', $userId)
            ->orderBy('n.dateAdd', 'DESC');

        return $queryBuilder->getQuery()->getResult();
    }

    public function fundUsersSystemNotificationsExtra(
        string $userId,
        string $userEntity,
        string $extraEntity,
        ?array $extraParams,
        ?int $max
    ): array {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $queryBuilder->select('n, uo')
            ->from($userEntity, 'uo')
            ->join('uo.user', 'u')
            ->join($extraEntity, 'en', Join::WITH, 'en.user', 'i.id')
            ->join('en.notification', 'n')
            ->where($queryBuilder->expr()->eq('u.id', ':userId'))
            ->andWhere($queryBuilder->expr()->isNotNull('en.viewed'))
            ->setParameter(':userId', $userId)
            ->orderBy('n.dateAdd', 'DESC');

        if ($max) {
            $queryBuilder->setMaxResults($max);
        }

        if ($extraParams) {
            $params = $queryBuilder->expr()->andX();
            foreach ($extraParams as $key => $extraParam) {
                $params->add($queryBuilder->expr()->eq($key, "{$extraParam}"));
            }
            $queryBuilder->andWhere($params);
        }

        return $queryBuilder->getQuery()->getResult();
    }
}
