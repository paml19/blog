<?php

namespace paml\Layouts\ColorAdmin\Repository\Traits;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use paml\Layouts\ColorAdmin\Entity\SystemNotification;

//class UserSystemNotificationRepositoryTrait extends EntityRepository
trait UserSystemNotificationRepositoryTrait
{
    public function findAllWithSystemNotifications(): array
    {
        return $this->findBy([
            'systemNotification' => true,
        ]);
    }

    public function findUsersSystemNotifications(
        ?string $userId,
        ?int $max,
        ?string $extraEntity,
        ?array $extraParams
    ): array {
        $target = $this
            ->getEntityManager()
            ->getClassMetadata($this->getEntityName())
            ->getAssociationMappings()['systemNotifications']['targetEntity'];

        if ($target != SystemNotification::class) {
            return $this->fndUsersSystemNotificationsExtra($userId, $extraEntity, $extraParams, $max);
        }
//
//        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
//        $queryBuilder->select('n, uo')
//            ->from($this->getEntityName(), 'uo')
//            ->join('uo.user', 'u')
//            ->join('uo.systemNotifications', 'n')
//            ->where($queryBuilder->expr()->eq('u.id', ':userId'))
//            ->setParameter(':userId', $userId)
//            ->orderBy('n.dateAdd', 'DESC');
//
//        if ($max) {
//            $queryBuilder->setMaxResults($max);
//        }
//
//        return $queryBuilder->getQuery()->getResult();
        return [];
    }

    public function fndUsersSystemNotificationsExtra(
        ?string $userId,
        string $extraEntity,
        ?array $extraParams,
        ?int $max
    ): array {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $queryBuilder->select('n')
            ->from($this->getEntityName(), 'uo')
            ->join('uo.user', 'u')
            ->join(
                $extraEntity,
                'en',
                Join::WITH,
                $queryBuilder->expr()->eq('en.user', 'uo.id')
            )
            ->join(
                SystemNotification::class,
                'n',
                Join::WITH,
                $queryBuilder->expr()->eq('en.notification', 'n.id')
            )
            ->where($queryBuilder->expr()->eq('u.id', ':userId'))
            ->andWhere($queryBuilder->expr()->isNotNull('en.viewed'))
            ->setParameter(':userId', $userId)
            ->orderBy('n.dateAdd', 'DESC');

        if ($max) {
            $queryBuilder->setMaxResults($max);
        }

        if ($extraParams) {
            $params = $queryBuilder->expr()->andX();
            foreach ($extraParams as $key => $extraParam) {
                $params->add($queryBuilder->expr()->eq($key, $extraParam));
            }
            $queryBuilder->andWhere($params);
        }

        return $queryBuilder->getQuery()->getResult();
    }
}
