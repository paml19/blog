<?php

namespace paml\Layouts\ColorAdmin\Repository\Traits;

use Doctrine\ORM\QueryBuilder;

trait ServerSideRepositoryTrait
{
    private function prepareSearchAndFilter(
        QueryBuilder $queryBuilder,
        array $form,
        array $params
    ): QueryBuilder {
        $expression = $queryBuilder->expr()->andX();

        if (! empty($form['searchForm'])) {
            $searchExpression = $queryBuilder->expr()->orX();

            foreach ((array) $params as $searchParam) {
                $searchExpression->add($queryBuilder->expr()->like($searchParam, "'%{$form['searchForm']}%'"));
            }

            $expression->add($searchExpression);
        }

        if ($expression->getParts()) {
            $queryBuilder->andWhere($expression);
        }

        foreach ($form as $key => $item) {
            if ($key != 'searchForm' && ($item != '' || $item != null)) {
                if (strstr($key, '[]') !== false) {
                    $key = str_replace(']', '', str_replace('[', '', $key));
                    $expression->add($queryBuilder->expr()->eq($key, '\'' . $item . '\''));
                } else {
                    $expression->add($queryBuilder->expr()->eq($key, '\'' . $item . '\''));
                }
            }
        }

        if ($expression->getParts()) {
            $queryBuilder->andWhere($expression);
        }

        return $queryBuilder;
    }
}
