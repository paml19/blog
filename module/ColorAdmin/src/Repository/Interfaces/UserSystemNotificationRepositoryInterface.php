<?php

namespace paml\Layouts\ColorAdmin\Repository\Interfaces;

interface UserSystemNotificationRepositoryInterface
{
    public function findAllWithSystemNotifications(): array;

    public function findUsersSystemNotifications(
        string $userId,
        ?int $max,
        ?string $extraEntity,
        ?array $extraParams
    ): array;

    public function save($userNotification): void;
}
