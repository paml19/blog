<?php

namespace paml\Layouts\ColorAdmin\Helper;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Helper\AbstractHelper;

class ExistsHelper extends AbstractHelper
{
    private $serviceManager;

    public function __construct(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }

    public function __invoke(string $id): bool
    {
        return $this->serviceManager->has($id);
    }
}