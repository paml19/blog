<?php

namespace paml\Layouts\ColorAdmin\Helper;

use paml\Layouts\ColorAdmin\Module;
use Zend\View\Helper\AbstractHelper;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;

class BadgeHelper extends AbstractHelper
{
    private $phpRenderer;

    public function __construct(PhpRenderer $phpRenderer)
    {
        $this->phpRenderer = $phpRenderer;
    }

    public function __invoke(string $value, bool $square = false, string $type = Module::COLOR_TYPE_DEFAULT)
    {
        $model = (new ViewModel)
            ->setVariables([
                'value' => $value,
                'type' => $type,
                'square' => $square,
            ])
            ->setTemplate('color-admin/helper/simple/badge');

        return $this->phpRenderer->render($model);
    }
}