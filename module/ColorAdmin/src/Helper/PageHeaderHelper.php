<?php

namespace paml\Layouts\ColorAdmin\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;

class PageHeaderHelper extends AbstractHelper
{
    private $phpRenderer;

    public function __construct(PhpRenderer $phpRenderer)
    {
        $this->phpRenderer = $phpRenderer;
    }

    public function __invoke(string $bigValue, ?string $smallValue = null)
    {
        $model = (new ViewModel)
            ->setVariables([
                'bigValue' => $bigValue,
                'smallValue' => $smallValue
            ])
            ->setTemplate('color-admin/helper/simple/page-header');

        return $this->phpRenderer->render($model);
    }
}