<?php

namespace paml\Layouts\ColorAdmin\Helper;

use Zend\Json\Json;
use Zend\View\Helper\AbstractHelper;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;

class DataTableHelper extends AbstractHelper
{
    private $phpRenderer;

    private $form;

    private $tableName;

    private $formText;

    public function __construct(PhpRenderer $phpRenderer)
    {
        $this->phpRenderer = $phpRenderer;
    }

    public function renderStyles(): string
    {
        $viewModel = (new ViewModel)
            ->setTemplate('color-admin/helper/data-table/styles');

        return $this->phpRenderer->render($viewModel);
    }

    public function renderMainScripts(): string
    {
        $viewModel = (new ViewModel)
            ->setTemplate('color-admin/helper/data-table/main-scripts');

        return $this->phpRenderer->render($viewModel);
    }

    public function setForm($form): self
    {
        $this->form = $form;
        return $this;
    }

    public function renderTable(string $tableName, array $headers): string
    {
        $this->tableName = $tableName;
        $viewModel = (new ViewModel)
            ->setVariables([
                'tableName' => $tableName,
                'headers' => $headers,
            ])
            ->setTemplate('color-admin/helper/data-table/table');

        return $this->phpRenderer->render($viewModel);
    }

    public function renderTableScript(
        string $url,
        ?string $formSubmit,
        ?string $formText,
        array $columns,
        ?array $translation
    ): string {
        $this->formText = $formText;
        $viewModel = (new ViewModel)
            ->setVariables([
                'tableName' => $this->tableName,
                'translation' => Json::encode($translation),
                'ajaxUrl' => $url,
                'form' => $this->form,
                'columns' => Json::encode($columns),
                'formSubmit' => $formSubmit,
                'formText' => $formText,
            ])
            ->setTemplate('color-admin/helper/data-table/table-script');

        return $this->phpRenderer->render($viewModel);
    }

    public function renderTableFiltersTags(string $container): string
    {
        $viewModel = (new ViewModel)
            ->setVariables([
                'form' => $this->form,
                'container' => $container,
                'formText' => $this->formText,
            ])
            ->setTemplate('color-admin/helper/data-table/filters');

        return $this->phpRenderer->render($viewModel);
    }
}
