<?php

namespace paml\Layouts\ColorAdmin\Helper;

use paml\Layouts\ColorAdmin\Entity\SystemNotification;
use paml\Layouts\ColorAdmin\Repository\Interfaces\UserSystemNotificationRepositoryInterface;
use paml\Layouts\ColorAdmin\Service\Helper\SystemNotificationHelperProperties;
use Zend\View\Helper\AbstractHelper;
use Zend\View\Model\ViewModel;

class SystemNotificationHelper extends AbstractHelper
{
    private $userSystemNotificationRepository;

    private $phpRenderer;

    private $identity;

    private $translatorService;

    private $notifications;

    public function __construct(
        UserSystemNotificationRepositoryInterface $userSystemNotificationRepository,
        SystemNotificationHelperProperties $systemNotificationHelperProperties
    ) {
        $this->userSystemNotificationRepository = $userSystemNotificationRepository;
        $this->phpRenderer = $systemNotificationHelperProperties->getPhpRenderer();
        $this->identity = $systemNotificationHelperProperties->getIdentity();
        $this->translatorService = $systemNotificationHelperProperties->getTranslatorService();

        $this->notifications = $this->userSystemNotificationRepository->findUsersSystemNotifications(
            $this->identity->getAuthenticationService()->getIdentity()['id'],
            5,
            $systemNotificationHelperProperties->getExtraEntity(),
            $systemNotificationHelperProperties->getExtraParams()
        );
    }

    public function getCount()
    {
        if (isset($this->notifications)) {
            return count($this->notifications);
        }

        return 0;
    }

    public function getNotifications()
    {
        $html = '';

        if (isset($this->notifications)) {
            /** @var SystemNotification $notification */
            foreach ($this->notifications as $key => $notification) {
                if ($key == 5) {
                    break;
                }

                $view = (new ViewModel)
                    ->setVariables([
                        'notification' => $notification,
                        'time' => $this->dateDiff($notification->getDateAdd())
                    ])
                    ->setTemplate('color-admin/helper/notification/notification');

                $html .= $this->phpRenderer->render($view);
            }
        }

        return $html;
    }

    protected function dateDiff($date)
    {
        $translator = $this->translatorService->getTranslator();
        $mydate= date("Y-m-d H:i:s");
        $datetime1 = $date;
        $datetime2 = date_create($mydate);
        $interval = date_diff($datetime1, $datetime2);

        $min = $interval->format('%i');
        $sec = $interval->format('%s');
        $hour = $interval->format('%h');
        $mon = $interval->format('%m');
        $day = $interval->format('%d');
        $year = $interval->format('%y');

        if ($interval->format('%i%h%d%m%y') == "00000") {
            return $sec . ' ' . $translator->translate('seconds');
        } elseif ($interval->format('%h%d%m%y') == "0000") {
            return $min . ' ' . $translator->translate('minutes');
        } elseif ($interval->format('%d%m%y') == "000") {
            return $hour . ' ' . $translator->translate('hours');
        } elseif ($interval->format('%m%y') == "00") {
            return $day . ' ' . $translator->translate('days');
        } elseif ($interval->format('%y') == "0") {
            return $mon . ' ' . $translator->translate('months');
        } else {
            return $year . ' ' . $translator->translate('years');
        }
    }
}
