<?php

namespace paml\Layouts\ColorAdmin\Helper;

use paml\Layouts\ColorAdmin\Module;
use Zend\View\Helper\AbstractHelper;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;

class PanelHelper extends AbstractHelper
{
    private $phpRenderer;

    public function __construct(PhpRenderer $phpRenderer)
    {
        $this->phpRenderer = $phpRenderer;
    }

    public function __invoke(
        string $type = Module::COLOR_TYPE_DEFAULT,
        ?array $header = null,
        ?string $body = null,
        ?array $footer = null
    ) {
        $model = (new ViewModel)
            ->setVariables([
                'heading' => $header,
                'footer' => $footer,
                'body' => $body,
                'type' => $type
            ])
            ->setTemplate('color-admin/helper/panel');

        return $this->phpRenderer->render($model);
    }
}