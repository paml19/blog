<?php

declare(strict_types=1);

namespace paml\Layouts\ColorAdmin\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;

class DataTableButtons extends AbstractPlugin
{
    private $route;

    private $template;

    private $params;

    private $phpRenderer;

    public function __construct(PhpRenderer $phpRenderer)
    {
        $this->phpRenderer = $phpRenderer;
    }

    public function __invoke(string $route, ?array $params = [], ?string $template = null): self
    {
        $this->route = $route;
        $this->template = $template;
        $this->params = $params;
        return $this;
    }

    public function prepareButtons(string $id): string
    {
        $viewModel = new ViewModel([
            'route' => $this->route,
            'params' => $this->params,
            'id' => $id,
        ]);

        if ($this->template) {
            $viewModel->setTemplate($this->template);
        } else {
            $viewModel->setTemplate('color-admin/helper/data-table/extended/buttons');
        }

        return $this->phpRenderer->render($viewModel);
    }
}
