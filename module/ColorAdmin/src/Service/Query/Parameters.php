<?php

namespace paml\Layouts\ColorAdmin\Service\Query;

class Parameters
{
    private $order;

    private $form;

    private $offset;

    private $limit;

    private $draw;

    public function __construct($order, $form, $offset, $limit, $draw)
    {
        $this->order = $order;
        $this->form = $form;
        $this->offset = $offset;
        $this->limit = $limit;
        $this->draw = $draw;
    }

    public function getOrder(): array
    {
        return $this->order;
    }

    public function getForm(): array
    {
        return $this->form;
    }

    public function getOffset(): int
    {
        return $this->offset;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function getDraw(): int
    {
        return $this->draw;
    }
}
