<?php

namespace paml\Layouts\ColorAdmin\Service\Query;

class DataTableService
{
    public function parseFromQuery(array $queryParams): Parameters
    {
        $order = $queryParams['order'][0];
        $offset = $queryParams['start'];
        $limit = $queryParams['length'];
        $draw = $queryParams['draw'];
        $arrForm = [];

        if (isset($queryParams['form'])) {
            $form = $queryParams['form'];
            foreach ($form as $item) {
                $arrForm[$item['name']] = $item['value'];
            }
        }

        return new Parameters($order, $arrForm, $offset, $limit, $draw);
    }
}
