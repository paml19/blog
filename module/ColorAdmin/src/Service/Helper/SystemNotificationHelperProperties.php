<?php

namespace paml\Layouts\ColorAdmin\Service\Helper;

use paml\SessionTranslator\Service\TranslatorService;
use Zend\Mvc\Plugin\Identity\Identity;
use Zend\View\Renderer\PhpRenderer;

class SystemNotificationHelperProperties
{
    private $phpRenderer;

    private $identity;

    private $translatorService;

    private $extraEntity;

    private $extraParams;

    public function __construct(
        PhpRenderer $phpRenderer,
        Identity $identity,
        TranslatorService $translatorService,
        ?string $extraEntity,
        ?array $extraParams
    ) {
        $this->phpRenderer = $phpRenderer;
        $this->identity = $identity;
        $this->translatorService = $translatorService;
        $this->extraEntity = $extraEntity;
        $this->extraParams = $extraParams;
    }

    public function getPhpRenderer(): PhpRenderer
    {
        return $this->phpRenderer;
    }

    public function getIdentity(): Identity
    {
        return $this->identity;
    }

    public function getTranslatorService(): TranslatorService
    {
        return $this->translatorService;
    }

    public function getExtraEntity(): ?string
    {
        return $this->extraEntity;
    }

    public function getExtraParams(): ?array
    {
        return $this->extraParams;
    }
}
