<?php

namespace paml\Layouts\ColorAdmin\Service\Listener;

use Doctrine\ORM\EntityManager;
use paml\Layouts\ColorAdmin\Repository\SystemNotificationRepository;
use paml\SessionTranslator\Service\TranslatorService;
use Zend\Mvc\Plugin\FlashMessenger\FlashMessenger;

class NotificationListenerProperties
{
    private $events;

    private $flashMessenger;

    private $systemNotificationRepository;

    private $extraEntity;

    private $extraParams;

    private $entityManager;

    private $translatorService;

    private $roleBased;

    public function __construct(
        array $events,
        FlashMessenger $flashMessenger,
        SystemNotificationRepository $systemNotificationRepository,
        EntityManager $entityManager,
        ?string $extraEntity,
        ?array $extraParams,
        TranslatorService $translatorService,
        bool $roleBased
    ) {
        $this->events = $events;
        $this->flashMessenger = $flashMessenger;
        $this->systemNotificationRepository = $systemNotificationRepository;
        $this->extraEntity = $extraEntity;
        $this->extraParams = $extraParams;
        $this->entityManager = $entityManager;
        $this->translatorService = $translatorService;
        $this->roleBased = $roleBased;
    }

    public function getEvents(): array
    {
        return $this->events;
    }

    public function getFlashMessenger(): FlashMessenger
    {
        return $this->flashMessenger;
    }

    public function getSystemNotificationRepository(): SystemNotificationRepository
    {
        return $this->systemNotificationRepository;
    }

    public function getExtraEntity(): ?string
    {
        return $this->extraEntity;
    }

    public function getExtraParams(): ?array
    {
        return $this->extraParams;
    }

    public function getEntityManager(): EntityManager
    {
        return $this->entityManager;
    }

    public function getTranslatorService(): TranslatorService
    {
        return $this->translatorService;
    }

    public function isRoleBased(): bool
    {
        return $this->roleBased;
    }
}
