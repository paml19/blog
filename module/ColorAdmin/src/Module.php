<?php

namespace paml\Layouts\ColorAdmin;

use paml\Layouts\ColorAdmin\Listener\LayoutListener;
use paml\Layouts\ColorAdmin\Listener\NotificationListener;
use Zend\Loader\StandardAutoloader;
use Zend\Mvc\MvcEvent;

class Module
{
    const COLOR_TYPE_DEFAULT = 'default';

    const COLOR_TYPE_DANGER = 'danger';

    const COLOR_TYPE_WARNING = 'warning';

    const COLOR_TYPE_SUCCESS = 'success';

    const COLOR_TYPE_INFO = 'info';

    const COLOR_TYPE_PRIMARY = 'primary';

    const COLOR_TYPE_INVERSE = 'inverse';

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return [
            StandardAutoloader::class => [
                'AssetManager',
            ],
        ];
    }

    public function onBootstrap(MvcEvent $event)
    {
        $serviceManager = $event->getApplication()->getServiceManager();
        $eventManager = $event->getTarget()->getEventManager();

        /** @var LayoutListener $layoutListener */
        $layoutListener = $serviceManager->get(LayoutListener::class);
        $layoutListener->attach($eventManager, 2);


        if ($serviceManager->has(NotificationListener::class)) {
            /** @var NotificationListener $notificationListener */
            $notificationListener = $serviceManager->get(NotificationListener::class);
            $notificationListener->attach($eventManager, 101);
        }
    }
}
