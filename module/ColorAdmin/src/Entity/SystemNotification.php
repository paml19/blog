<?php

namespace paml\Layouts\ColorAdmin\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use paml\Notification\Entity\Notification;

/**
 * @ORM\Table(name="notification_system")
 * @ORM\Entity(repositoryClass="paml\Layouts\ColorAdmin\Repository\SystemNotificationRepository")
 * @GEDMO\Loggable
 * @Gedmo\SoftDeleteable(fieldName="dateDelete", timeAware=false)
 */
class SystemNotification
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid", unique=true, nullable=false)
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="date_add", type="datetime")
     */
    private $dateAdd;

    /**
     * @ORM\Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @ORM\OneToOne(targetEntity="paml\Notification\Entity\Notification")
     * @ORM\JoinColumn(name="notification", referencedColumnName="id")
     */
    private $notification;

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getDateAdd(): \DateTime
    {
        return $this->dateAdd;
    }

    public function getDateDelete(): ?\DateTime
    {
        return $this->dateDelete;
    }

    public function getNotification(): ?Notification
    {
        return $this->notification;
    }

    public function setNotification(Notification $notification): self
    {
        $this->notification = $notification;
        return $this;
    }
}
