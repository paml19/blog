<?php

namespace paml\File;

use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Zend\Router\Http\Segment;

return [
    'acl' => [
        'resources' => [
            'excluded' => [
                Controller\FileController::class,
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\FileController::class => Factory\FileControllerFactory::class,
        ],
    ],
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ],
            ],
        ],
    ],
    'hydrators' => [
        'factories' => [
            Hydrator\FileHydrator::class => Factory\FileHydratorFactory::class,
        ],
    ],
    'router' => [
        'routes' => [
            'file' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/file/:action/:id',
                    'defaults' => [
                        'controller' => Controller\FileController::class,
                        'action' => 'index',
                    ],
                ],
            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            Service\FileManager::class => Factory\FileManagerFactory::class,
        ],
    ],
    'view_helpers' => [
        'factories' => [
            Helper\FileHelper::class => Factory\FileHelperFactory::class,
        ],
        'aliases' => [
            'file' => Helper\FileHelper::class,
        ],
    ],
];
