<?php

namespace paml\File;

class Module
{
    const FILE_CREATED = 'fileCreated';

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }
}
