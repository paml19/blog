<?php

namespace paml\File\Repository;

use Doctrine\ORM\EntityRepository;
use paml\File\Entity\File;

class FileRepository extends EntityRepository
{
    public function save(File $file): void
    {
        $this->getEntityManager()->persist($file);
        $this->getEntityManager()->flush();
    }

    public function merge(File $file): void
    {
        $this->getEntityManager()->merge($file);
        $this->getEntityManager()->flush();
    }

    public function delete(File $file): void
    {
        $this->getEntityManager()->remove($file);
        $this->getEntityManager()->flush();
    }

    public function persist(File $file): void
    {
        $this->getEntityManager()->persist($file);
    }
}
