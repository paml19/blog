<?php

namespace paml\File\Service;

use paml\File\Entity\File;
use paml\File\Repository\FileRepository;

interface FileManagerInterface
{
    public function upload(string $sourcePath, string $sourceFilename, int $sourceSize, string $sourceType): File;
    public function getFile(File $file): string;
    public function getFilePath(File $file): string;
}
