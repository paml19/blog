<?php

namespace paml\File\Service;

use paml\File\Entity\File;
use paml\File\Repository\FileRepository;

class FileManager implements FileManagerInterface
{
    protected $fileRepository;

    protected $publicPath;

    public function __construct(FileRepository $fileRepository, string $publicPath)
    {
        $this->fileRepository = $fileRepository;
        $this->publicPath = $publicPath;
    }

    public function upload(
        string $sourcePath,
        string $sourceFilename,
        int $sourceSize,
        string $sourceType
    ): File {
        $file = (new File)
            ->setSize($sourceSize)
            ->setName($sourceFilename)
            ->setType($sourceType);

        $this->fileRepository->persist($file);

        $targetDir = $this->publicPath . $file->getPath();
        $targetPath = $targetDir . $sourceFilename;

        if (! file_exists($targetDir)) {
            mkdir($targetDir, 0777, true);
        }

        if (rename($sourcePath, $targetPath)) {
            return $file;
        } else {
            throw new \RuntimeException('The file could not be uploaded');
        }
    }

    public function getFile(File $file): string
    {
        return file_get_contents($this->publicPath . $file->getFilePath());
    }

    public function getFilePath(File $file): string
    {
        return $this->publicPath . $file->getFilePath();
    }
}
