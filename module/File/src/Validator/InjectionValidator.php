<?php

namespace paml\File\Validator;

use Zend\Validator\AbstractValidator;

class InjectionValidator extends AbstractValidator
{
    const INJECTION = 'injection';

    protected $messageTemplates = [
        self::INJECTION => "Uploaded file is infected",
    ];

    public function isValid($value)
    {
        $content = file_get_contents($value['tmp_name']);

        if (substr_count($content, '<?php')) {
            $this->error(self::INJECTION);
            return false;
        }

        return true;
    }
}
