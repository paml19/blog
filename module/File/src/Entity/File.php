<?php

namespace paml\File\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="file_file")
 * @ORM\Entity(repositoryClass="paml\File\Repository\FileRepository")
 * @GEDMO\Loggable
 * @Gedmo\SoftDeleteable(fieldName="dateDelete", timeAware=true)
 */
class File
{
    const UPLOAD_PATH = '/upload/';

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid", unique=true, nullable=false)
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;
    const ID = 'id';

    /**
     * @ORM\Column(type="string", name="name", nullable=true)
     */
    private $name;
    const NAME = 'name';

    /**
     * @ORM\Column(type="string", name="type", nullable=true)
     */
    private $type;
    const TYPE = 'type';

    /**
     * @ORM\Column(type="integer", name="size", nullable=true)
     */
    private $size;
    const SIZE = 'size';

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="date_add", type="datetime")
     */
    private $dateAdd;
    const DATE_ADD = 'dateAdd';

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="date_edit", type="datetime", nullable=true)
     */
    private $dateEdit;
    const DATE_EDIT = 'dateEdit';

    /**
     * @ORM\Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;
    const DATE_DELETE = 'dateDelete';

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getSize(): ?int
    {
        return $this->size;
    }

    public function setSize(int $size): self
    {
        $this->size = $size;
        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;
        return $this;
    }

    public function getDateAdd(): ?\DateTime
    {
        return $this->dateAdd;
    }

    public function getDateEdit(): ?\DateTime
    {
        return $this->dateEdit;
    }

    public function getDateDelete(): ?\DateTime
    {
        return $this->dateDelete;
    }

    public function getPath(): ?string
    {
        if ($this->id) {
            return self::UPLOAD_PATH . '/' . $this->id . '/';
        }

        return null;
    }

    public function getFilePath(): ?string
    {
        if ($this->name && $this->id) {
            return self::UPLOAD_PATH . '/' . $this->id . '/' . $this->name;
        }

        return null;
    }

    public function getNoUploadPathPath(): ?string
    {
        if ($this->id) {
            return '/' . $this->id . '/';
        }

        return null;
    }

    public function getNoUploadPathFilePath(): ?string
    {
        if ($this->name && $this->id) {
            return '/' . $this->id . '/' . $this->name;
        }

        return null;
    }
}
