<?php

namespace paml\File\Factory;

use Interop\Container\ContainerInterface;
use paml\File\Hydrator\FileHydrator;
use paml\File\Service\FileManager;
use Zend\ServiceManager\Factory\FactoryInterface;

class FileHydratorFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): FileHydrator
    {
        return new FileHydrator(
            $container->get(FileManager::class)
        );
    }
}
