<?php

namespace paml\File\Factory;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use paml\File\Controller\FileController;
use paml\File\Entity\File;
use paml\File\Service\FileManager;
use Zend\ServiceManager\Factory\FactoryInterface;

class FileControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): FileController
    {
        return new FileController(
            $container->get(FileManager::class),
            $container->get(EntityManager::class)->getRepository(File::class)
        );
    }
}
