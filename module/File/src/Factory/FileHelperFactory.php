<?php

namespace paml\File\Factory;

use Interop\Container\ContainerInterface;
use paml\File\Helper\FileHelper;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\View\Helper\Url;

class FileHelperFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): FileHelper
    {
        return new FileHelper(
            $container->get('ViewHelperManager')->get(Url::class)
        );
    }
}
