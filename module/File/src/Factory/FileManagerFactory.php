<?php

namespace paml\File\Factory;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use paml\File\Entity\File;
use paml\File\Service\FileManager;
use Zend\ServiceManager\Factory\FactoryInterface;

class FileManagerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): FileManager
    {
        return new FileManager(
            $container->get(EntityManager::class)->getRepository(File::class),
            $container->get('Config')['file']['public_path']
        );
    }
}
