<?php

namespace paml\File\Hydrator;

use paml\File\Entity\File;
use paml\File\Service\FileManagerInterface;
use Zend\Hydrator\AbstractHydrator;

class FileHydrator extends AbstractHydrator
{
    private $fileManager;

    public function __construct(FileManagerInterface $fileManager)
    {
        $this->fileManager = $fileManager;
    }

    public function extract($object)
    {
        return [
            File::ID => $object->getId(),
            File::NAME => $object->getName(),
            File::TYPE => $object->getType(),
            File::SIZE => $object->getSize(),
            File::DATE_ADD => $object->getDateAdd() ? $object->getDateAdd()->format('d.m.Y H:i:s') : null,
            File::DATE_EDIT => $object->getDateEdit() ? $object->getDateEdit()->format('d.m.Y H:i:s') : null,
            File::DATE_DELETE => $object->getDateDelete() ? $object->getDateDelete()->format('d.m.Y H:i:s') : null,
        ];
    }

    public function hydrate(array $data, $object)
    {
        if (isset($data['file']['size'])) {
            $file = $data['file'];
            $object = $this->fileManager->upload(
                $file['tmp_name'],
                $file['name'],
                $file['size'],
                $file['type']
            );
        }

        return $object;
    }
}
