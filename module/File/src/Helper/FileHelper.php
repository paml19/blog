<?php

namespace paml\File\Helper;

use paml\File\Entity\File;
use Zend\View\Helper\AbstractHelper;
use Zend\View\Helper\Url;

class FileHelper extends AbstractHelper
{
    private $url;

    private $file;

    public function __construct(Url $url)
    {
        $this->url = $url;
    }

    public function __invoke(File $file): self
    {
        $this->file = $file;
        return $this;
    }

    public function view(): string
    {
        return $this->url->__invoke('file', ['action' => 'get', 'id' => $this->file->getId()]);
    }

    public function download(): string
    {
        return $this->url->__invoke('file', ['action' => 'download', 'id' => $this->file->getId()]);
    }
}
