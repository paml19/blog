<?php

namespace paml\File\Controller;

use paml\File\Entity\File;
use paml\File\Repository\FileRepository;
use paml\File\Service\FileManagerInterface;
use Zend\Mvc\Controller\AbstractActionController;

class FileController extends AbstractActionController
{
    private $fileManager;

    private $fileRepository;

    public function __construct(FileManagerInterface $fileManager, FileRepository $fileRepository)
    {
        $this->fileManager = $fileManager;
        $this->fileRepository = $fileRepository;
    }

    public function getAction()
    {
        $id = $this->params()->fromRoute('id');
        $file = $this->fileRepository->find($id);

        $this->fileManager->getFile($file);

        header('Content-Type: ' . $file->getType());
        echo $this->fileManager->getFile($file);
        exit;
    }

    public function downloadAction()
    {
        $id = $this->params()->fromRoute('id');
        /** @var File $fileObj */
        $fileObj = $this->fileRepository->find($id);
        $file = $this->fileManager->getFilePath($fileObj);

        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="' . $fileObj->getName() . '"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . $fileObj->getSize());
        readfile($file);
        exit;
    }
}
