<?php

namespace FileTest;

use Faker\Factory;
use paml\File\Entity\File;
use PHPUnit\Framework\TestCase;

class FileEntityTest extends TestCase
{
    private $file;

    private $faker;

    protected function setUp(): void
    {
        $this->file = new File;
        $this->faker = Factory::create();

        parent::setUp();
    }

    public function testHasAttributes()
    {
        self::assertClassHasAttribute('id', File::class);
        self::assertClassHasAttribute('name', File::class);
        self::assertClassHasAttribute('type', File::class);
        self::assertClassHasAttribute('size', File::class);
        self::assertClassHasAttribute('dateAdd', File::class);
        self::assertClassHasAttribute('dateEdit', File::class);
        self::assertClassHasAttribute('dateDelete', File::class);
    }

    public function testInitFileValuesNull()
    {
        self::assertNull($this->file->getId());
        self::assertNull($this->file->getName());
        self::assertNull($this->file->getType());
        self::assertNull($this->file->getSize());
        self::assertNull($this->file->getDateAdd());
        self::assertNull($this->file->getDateEdit());
        self::assertNull($this->file->getDateDelete());
    }

    public function testReturnedTypes()
    {
        $this->populateData();

        self::assertIsString($this->file->getId());
        self::assertIsString($this->file->getName());
        self::assertIsString($this->file->getType());
        self::assertIsInt($this->file->getSize());
    }

    private function populateData()
    {
        $this->file
            ->setId($this->faker->uuid)
            ->setName($this->faker->name)
            ->setType($this->faker->fileExtension)
            ->setSize($this->faker->randomDigit);
    }
}