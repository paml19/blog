<?php

namespace paml\EntityOperation;

use Doctrine\ORM\Mapping\Driver\AnnotationDriver;

return [
    'console' => [
        'router' => [
            'routes' => [
                'show-operations' => [
                    'options' => [
                        'route' => 'operations show [<actionName>] [<name>] [<page>] [<count>] [<sort>] [<order>]',
                        'defaults' => [
                            'controller' => Controller\OperationController::class,
                            'action' => 'show',
                        ],
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\OperationController::class => Factory\OperationControllerFactory::class,
        ],
    ],
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Entity'],
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver',
                ],
            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            Listener\EntityListener::class => Factory\EntityListenerFactory::class,
        ],
    ],
];
