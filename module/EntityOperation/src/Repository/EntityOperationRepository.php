<?php

namespace paml\EntityOperation\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use DoctrineModule\Paginator\Adapter\Collection as CollectionAdapter;
use paml\EntityOperation\Entity\EntityOperation;
use Doctrine\ORM\EntityRepository;
use Zend\Paginator\Paginator;

class EntityOperationRepository extends EntityRepository
{
    public function save(EntityOperation $entityOperation): void
    {
        $this->getEntityManager()->persist($entityOperation);
        $this->getEntityManager()->flush();
    }

    public function getList(
        int $page,
        int $limit,
        string $sort,
        string $order,
        ?string $action,
        ?string $name
    ): Paginator {
        if (! $action && ! $name) {
            $collection = new ArrayCollection($this->findBy(['dateDelete' => null], [$order => $sort]));
        } else {
            $queryBuilder = $this->getEntityManager()->createQueryBuilder();
            $queryBuilder->select('e')
                ->from($this->getEntityName(), 'e');

            if ($action) {
                $queryBuilder->andWhere($queryBuilder->expr()->eq('e.action', ':action'));
                $queryBuilder->setParameter(':action', $action);
            }

            if ($name) {
                $queryBuilder->andWhere($queryBuilder->expr()->eq('e.name', ':name'));
                $queryBuilder->setParameter(':name', $name);
            }

            $collection = new ArrayCollection($queryBuilder->getQuery()->getResult());
        }
        $adapter = new CollectionAdapter($collection);

        $paginator = (new Paginator($adapter))
            ->setCurrentPageNumber($page)
            ->setItemCountPerPage($limit);

        return $paginator;
    }
}
