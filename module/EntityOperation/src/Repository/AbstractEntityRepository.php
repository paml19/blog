<?php

namespace paml\EntityOperation\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use DoctrineModule\Paginator\Adapter\Collection as CollectionAdapter;
use Zend\Paginator\Paginator;

abstract class AbstractEntityRepository extends EntityRepository
{
    public function save(object $entity): void
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
    }

    public function delete(object $entity): void
    {
        $this->getEntityManager()->remove($entity);
        $this->getEntityManager()->flush();
    }

    public function merge(object $entity): void
    {
        $this->getEntityManager()->merge($entity);
        $this->getEntityManager()->flush();
    }


    public function get($id): ?object
    {
        return $this->find($id);
    }

    public function getList(int $page, int $limit, string $sort, string $order): Paginator
    {
        $collection = new ArrayCollection($this->findBy(['dateDelete' => null], [$order => $sort]));
        $adapter = new CollectionAdapter($collection);

        $paginator = (new Paginator($adapter))
            ->setCurrentPageNumber($page)
            ->setItemCountPerPage($limit);

        return $paginator;
    }
}
