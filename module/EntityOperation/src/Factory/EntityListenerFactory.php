<?php

namespace paml\EntityOperation\Factory;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use paml\EntityOperation\Entity\EntityOperation;
use paml\EntityOperation\Listener\EntityListener;
use Zend\ServiceManager\Factory\FactoryInterface;

class EntityListenerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): EntityListener
    {
        $config = $container->get('Config')['entity_operation'];

        return new EntityListener(
            $container->get(EntityManager::class)->getRepository(EntityOperation::class),
            $config['main_module'],
            $config['entities']
        );
    }
}
