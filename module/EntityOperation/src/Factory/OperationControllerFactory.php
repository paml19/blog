<?php

namespace paml\EntityOperation\Factory;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use paml\EntityOperation\Controller\OperationController;
use paml\EntityOperation\Entity\EntityOperation;
use Zend\ServiceManager\Factory\FactoryInterface;

class OperationControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): OperationController
    {
        return new OperationController(
            $container->get(EntityManager::class)->getRepository(EntityOperation::class)
        );
    }
}
