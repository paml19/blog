<?php

namespace paml\EntityOperation;

use Doctrine\ORM\EntityManager;
use paml\EntityOperation\Listener\EntityListener;
use Zend\Mvc\MvcEvent;

class Module
{
    public function getConfig()
    {
        if (! file_exists(__DIR__ . '/../../../config/autoload/entityOperation.global.php')) {
            throw new \Exception('No file entityOperation.global.php provided, copy dist from module config to ./config/autoload/');
        }

        return include __DIR__ . '/../config/module.config.php';
    }

    public function onBootstrap(MvcEvent $event)
    {
        $serviceManager = $event->getApplication()->getServiceManager();
        /** @var EntityManager $entityManager */
        $entityManager = $serviceManager->get(EntityManager::class);
        $eventEntityManager = $entityManager->getEventManager();

        $config = $serviceManager->get('Config')['entity_operation'];

        $eventEntityManager->addEventListener(
            $config['events'],
            $serviceManager->get(EntityListener::class)
        );
    }
}
