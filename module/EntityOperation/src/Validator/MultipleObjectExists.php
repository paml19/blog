<?php

namespace paml\EntityOperation\Validator;

use DoctrineModule\Validator\ObjectExists;
use Zend\Stdlib\ArrayUtils;

class MultipleObjectExists extends ObjectExists
{
    protected function cleanSearchValue($value)
    {
        $value = is_object($value) ? [$value] : (array) $value;

        if (ArrayUtils::isHashTable($value)) {
            $matchedFieldsValues = [];

            foreach ($this->fields as $field) {
                if (! array_key_exists($field, $value)) {
                    throw new Exception\RuntimeException(
                        sprintf(
                            'Field "%s" was not provided, but was expected since the configured field lists needs'
                            . ' it for validation',
                            $field
                        )
                    );
                }

                $matchedFieldsValues[$field] = $value[$field];
            }
        } else {
            $matchedFieldsValues = [];

            foreach ($this->fields as $field) {
                foreach ($value as $item) {
                    $matchedFieldsValues[][$field] = $item;
                }
            }
        }

        return $matchedFieldsValues;
    }

    public function isValid($value)
    {
        $cleanedValue = $this->cleanSearchValue($value);

        foreach ($cleanedValue as $key => $item) {
            if (array_key_exists('id', $item) && $item['id'] != '') {
                $match = $this->objectRepository->findOneBy($item);

                if (! is_object($match)) {
                    foreach ($this->fields as $field) {
                        $this->error(self::ERROR_NO_OBJECT_FOUND, $item[$field]);
                    }

                    return false;
                }
            }
        }

        return true;
    }
}
