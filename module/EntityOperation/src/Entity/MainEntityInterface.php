<?php

namespace paml\EntityOperation\Entity;

interface MainEntityInterface
{
    public function getId(): ?string;

    public function setId(string $id);

    public function getDateAdd(): ?\DateTime;

    public function getDateEdit(): ?\DateTime;

    public function getDateDelete(): ?\DateTime;
}
