<?php

namespace paml\EntityOperation\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="entity_operation_event")
 * @ORM\Entity(repositoryClass="paml\EntityOperation\Repository\EntityOperationRepository")
 * @GEDMO\Loggable
 */
class EntityOperation
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid", unique=true, nullable=false)
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;
    const ID = 'id';

    /**
     * @ORM\Column(type="string", name="name", nullable=true)
     */
    private $name;
    const NAME = 'name';

    /**
     * @ORM\Column(type="string", name="action", nullable=true)
     */
    private $action;
    const ACTION = 'action';

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="date_add", type="datetime")
     */
    private $dateAdd;
    const DATE_ADD = 'dateAdd';

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="date_edit", type="datetime", nullable=true)
     */
    private $dateEdit;
    const DATE_EDIT = 'dateEdit';

    /**
     * @ORM\Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;
    const DATE_DELETE = 'dateDelete';

    /**
     * @ORM\Column(type="json_array", name="object_data")
     */
    private $objectData;
    const OBJECT_DATA = 'objectData';

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getAction(): ?string
    {
        return $this->action;
    }

    public function setAction(string $action): self
    {
        $this->action = $action;
        return $this;
    }

    public function getDateAdd(): ?\DateTime
    {
        return $this->dateAdd;
    }

    public function getDateEdit(): ?\DateTime
    {
        return $this->dateEdit;
    }

    public function getDateDelete(): ?\DateTime
    {
        return $this->dateDelete;
    }

    public function getObjectData(): array
    {
        return $this->objectData;
    }

    public function setObjectData(array $objectData): self
    {
        $this->objectData = $objectData;
        return $this;
    }
}
