<?php

namespace paml\EntityOperation\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use paml\EntityOperation\DTO\MainEntityDTO;
use paml\EntityOperation\Entity\EntityOperation;
use paml\EntityOperation\Repository\EntityOperationRepository;
use Zend\Mvc\Console\Controller\AbstractConsoleController;

class OperationController extends AbstractConsoleController
{
    private $entityOperationRepository;

    public function __construct(EntityOperationRepository $entityOperationRepository)
    {
        $this->entityOperationRepository = $entityOperationRepository;
    }

    public function showAction()
    {
        $page = $this->params('page', 1);
        $count = $this->params('count', 10);
        $sort = $this->params('sort', 'asc');
        $order = $this->params('order', 'id');
        $action = $this->params('actionName', null);
        $name = $this->params('name', null);

        $list = $this->entityOperationRepository->getList($page, $count, $sort, $order, $action, $name);

        $items = new ArrayCollection;
        if ($list->count() >= $page) {
            /** @var EntityOperation $item */
            foreach ($list as $item) {
                $objectData = $item->getObjectData();
                $items->add(new MainEntityDTO(
                    $objectData['id'],
                    $objectData['dateadd'],
                    $objectData['dateedit'],
                    $objectData['datedelete'],
                    $item->getAction(),
                    $item->getName()
                ));
            }
        }

        $result = '';
        /** @var MainEntityDTO $item */
        foreach ($items as $item) {
            $result .= $item->toConsole();
        }

        return $result;
    }
}
