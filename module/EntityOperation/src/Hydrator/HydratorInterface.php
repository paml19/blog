<?php

namespace paml\EntityOperation\Hydrator;

interface HydratorInterface
{
    public function hydrate(array $data, $object);

    public function extract($object);
}
