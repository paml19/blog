<?php

namespace paml\EntityOperation\Listener;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Event\LifecycleEventArgs;
use paml\EntityOperation\Entity\EntityOperation;
use paml\EntityOperation\Repository\EntityOperationRepository;

class EntityListener
{
    private $entityOperationRepository;

    private $mainModule;

    private $entities;

    public function __construct(
        EntityOperationRepository $entityOperationRepository,
        string $mainModule,
        array $entities
    ) {
        $this->entityOperationRepository = $entityOperationRepository;
        $this->mainModule = $mainModule;
        $this->entities = $entities;
    }

    public function preRemove(LifecycleEventArgs $event)
    {
        $this->save($event, 'preRemove');
    }

    public function postUpdate(LifecycleEventArgs $event)
    {
        $this->save($event, 'postUpdate');
    }

    public function postPersist(LifecycleEventArgs $event)
    {
        $this->save($event, 'postPersist');
    }

    private function save($event, string $actionName): void
    {
        $entity = $event->getEntity();
        if (in_array(get_class($entity), $this->entities)) {
            $object = $event->getObject();
            $objectMethods = get_class_methods($entity);
            $objectGetters = [];

            foreach ($objectMethods as $objectMethod) {
                if (strstr($objectMethod, 'get')) {
                    $objectGetters[] = $objectMethod;
                }
            }

            $dataToSave = [];

            foreach ($objectGetters as $objectGetter) {
                $dataName = strtolower(substr($objectGetter, 3));
                if (is_object($object->$objectGetter())) {
                    if ($object->$objectGetter() instanceof \DateTime) {
                        $dataToSave[$dataName] = $object->$objectGetter()->format('d.m.Y H:i:s');
                    } elseif ($object->$objectGetter() instanceof Collection) {
                        $collection = null;
                        foreach ($object->$objectGetter()->toArray() as $item) {
                            $collection['id'] = $item->getId();
                        }
                        $dataToSave[$dataName] = $collection;
                    } else {
                        $dataToSave[$dataName] = $object->$objectGetter()->getId();
                    }
                } else {
                    $dataToSave[$dataName] = $object->$objectGetter();
                }
            }

            $entityOperation = (new EntityOperation)
                ->setName(get_class($entity))
                ->setAction($actionName)
                ->setObjectData($dataToSave);

            $this->entityOperationRepository->save($entityOperation);
        }
    }
}
