<?php

namespace paml\EntityOperation\DTO;

use paml\EntityOperation\Entity\MainEntityInterface;

class MainEntityDTO implements MainEntityInterface
{
    private $id;

    private $dateAdd;

    private $dateEdit;

    private $dateDelete;

    private $action;

    private $name;

    /**
     * MainEntityDTO constructor.
     * @param $id
     * @param $dateAdd
     * @param $dateEdit
     * @param $dateDelete
     * @param $action
     * @param $name
     */
    public function __construct($id, $dateAdd, $dateEdit, $dateDelete, $action, $name)
    {
        $this->id = $id;
        $this->dateAdd = $dateAdd;
        $this->dateEdit = $dateEdit;
        $this->dateDelete = $dateDelete;
        $this->action = $action;
        $this->name = $name;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getDateAdd(): ?\DateTime
    {
        return $this->dateAdd;
    }

    public function setDateAdd(\DateTime $dateAdd): self
    {
        $this->dateAdd = $dateAdd;
        return $this;
    }

    public function getDateEdit(): ?\DateTime
    {
        return $this->dateEdit;
    }

    public function setDateEdit(\DateTime $dateEdit): self
    {
        $this->dateEdit = $dateEdit;
        return $this;
    }

    public function getDateDelete(): ?\DateTime
    {
        return $this->dateDelete;
    }

    public function setDateDelete(\DateTime $dateDelete): self
    {
        $this->dateDelete = $dateDelete;
        return $this;
    }

    public function toArray(): array
    {
    }

    public function toConsole(): string
    {
        return $this->id . ' | ' . $this->dateAdd . ' | ' . $this->dateEdit . ' | ' . $this->dateDelete . ' | ' .
            $this->action . ' | ' . $this->name . ' | ' . PHP_EOL;
    }
}
