<?php

namespace paml\EntityView\Controller;

interface EntityViewControllerInterface
{
    public function indexAction();

    public function addAction();

    public function deleteAction();
}
