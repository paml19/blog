<?php

namespace paml\EntityView\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use paml\EntityOperation\Repository\AbstractEntityRepository;
use paml\EntityView\Module;
use Zend\Form\FormInterface;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

abstract class AbstractViewController extends AbstractActionController implements EntityViewControllerInterface
{
    protected $entityRepository;

    protected $defaults;

    protected $form;

    private $mainRoute;

    public function __construct(
        AbstractEntityRepository $entityRepository,
        array $defaults,
        array $mainRoute,
        FormInterface $form
    ) {
        $this->entityRepository = $entityRepository;
        $this->defaults = $defaults;
        $this->form = $form;
        $this->mainRoute = $mainRoute;
    }

    public function indexAction()
    {
        $page = (int) $this->params()->fromQuery('page', $this->defaults['page']);
        $count = (int) $this->params()->fromQuery('count', $this->defaults['count']);
        $sort = (string) $this->params()->fromQuery('sort', $this->defaults['sort']);
        $order = (string) $this->params()->fromQuery('order', $this->defaults['order']);

        $list = $this->entityRepository->getList($page, $count, $sort, $order);

        $items = new ArrayCollection();
        if ($list->count() >= $page) {
            foreach ($list as $item) {
                $items->add($item);
            }
        }

        $this->getEventManager()->trigger(Module::EVENT_GET_LIST, $this, ['list' => $items]);

        return new ViewModel([
            'items' => $items
        ]);
    }

    public function addAction()
    {
        $id = $this->params()->fromRoute('id', null);
        $request = $this->getRequest();

        $object = null;

        if ($id) {
            $object = $this->entityRepository->get($id);

            if (! $object) {
                throw new \Exception('Not found object with ' . $id);
            }

            $this->form->bind($object);
        }

        if ($request->isPost()) {
            $postData = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );

            if ($id) {
                $this->getEventManager()->trigger(Module::EVENT_UPDATE, $this, ['data' => $postData]);
            } else {
                $this->getEventManager()->trigger(Module::EVENT_CREATE, $this, ['data' => $postData]);
            }

            $this->form->setData($postData);
            if ($this->form->isValid()) {
                $object = $this->form->getData();

                if ($id) {
                    $this->entityRepository->merge($object);
                    $this->getEventManager()->trigger(Module::EVENT_UPDATED, $this, ['object' => $object]);
                } else {
                    $this->entityRepository->save($object);
                    $this->getEventManager()->trigger(Module::EVENT_CREATED, $this, ['object' => $object]);
                }

                return $this->redirectToHome();
            }
        } else {
            if ($id) {
                $this->getEventManager()->trigger(Module::EVENT_GET, $this, ['object' => $object]);
            }
        }

        return new ViewModel([
            'form' => $this->form,
            'object' => $object,
        ]);
    }

    public function deleteAction()
    {
        $id = $this->params()->fromRoute('id');

        $object = $this->entityRepository->get($id);

        if (! $object) {
            throw new \Exception('Not found object with ' . $id);
        }

        $this->getEventManager()->trigger(Module::EVENT_DELETE, $this, ['object' => $object]);

        $this->entityRepository->delete($object);

        $this->getEventManager()->trigger(Module::EVENT_DELETED, $this, ['object' => $object]);

        return $this->redirectToHome();
    }

    private function redirectToHome()
    {
        $class = get_class($this);
        $class = explode('\\', $class);
        $class = $class[count($class) - 1];
        $class = strtolower(str_replace('Controller', '', $class));

        return $this->redirect()->toUrl('/' . $this->mainRoute['name'] . '/' . $class);
    }
}
