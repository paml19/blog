<?php

namespace paml\EntityView\Factory;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class AbstractViewControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $mainModule = $container->get('Config')['entity_view']['main_module'];
        $entityName = explode('\\', $requestedName);
        $entityName = str_replace('Controller', '', $entityName[count($entityName) - 1]);

        return new $requestedName(
            $container->get(EntityManager::class)->getRepository($mainModule . '\\Entity\\' . $entityName),
            $container->get('Config')['entity_view']['defaults'],
            $container->get('Config')['entity_view']['main_route'],
            $container->get('FormElementManager')->get($mainModule . '\\Form\\View\\' . $entityName . 'Form')
        );
    }
}
