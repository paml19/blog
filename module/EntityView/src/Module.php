<?php

namespace paml\EntityView;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;

class Module
{
    const EVENT_GET = 'eventGet';

    const EVENT_GET_LIST = 'eventGetList';

    const EVENT_CREATE = 'eventCreate';

    const EVENT_CREATED = 'eventCreated';

    const EVENT_UPDATE = 'eventUpdate';

    const EVENT_UPDATED = 'eventUpdated';

    const EVENT_DELETE = 'eventDelete';

    const EVENT_DELETED = 'eventDeleted';

    public function getConfig()
    {
        if (! file_exists(__DIR__ . '/../../../config/autoload/entityView.global.php')) {
            throw new \Exception('No file entityView.global.php provided, copy dist from module config to ./config/autoload/');
        }

        $mainConfig = include __DIR__ . '/../../../config/autoload/entityView.global.php';
        $fileConfig = include __DIR__ . '/../config/module.config.php';

        $controllersAliases = [
            'controllers' => [
                'aliases' => [],
            ]
        ];

        $homeRouter = [];
        if ($mainConfig['entity_view']['main_route']['name'] != 'home') {
            $homeRouter = [
                'router' => [
                    'routes' => [
                        'home' => [
                            'type' => Literal::class,
                            'options' => [
                                'route' => '/',
                                'defaults' => [
                                    'controller' => Controller\DefaultController::class,
                                    'action' => 'index',
                                ],
                            ],
                        ],
                    ],
                ],
            ];
        }

        $router = [
            'router' => [
                'routes' => [
                    $mainConfig['entity_view']['main_route']['name'] => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/' . $mainConfig['entity_view']['main_route']['route'],
                            'defaults' => [
                                'controller' => Controller\DefaultController::class,
                                'action' => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'front' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/:controller[/:action[/:id]]',
                                    'defaults' => [
                                        'action' => 'index',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];

        $entitiesFiles = scandir(__DIR__ . '/../../' . $mainConfig['entity_view']['main_module'] . '/src/Entity');
        $entitiesFiles = array_slice($entitiesFiles, 2, count($entitiesFiles) - 2);

        foreach ($entitiesFiles as $entityFile) {
            $entity = explode('.', $entityFile)[0];

            $controllersAliases['controllers']['aliases'][mb_strtolower($entity)] =
            $mainConfig['entity_view']['main_module'] . '\\Controller\\' . ucfirst($mainConfig['entity_view']['main_route']['name']) . '\\' .
                $entity . 'Controller';
        }

        $config = array_merge_recursive($fileConfig, $controllersAliases, $homeRouter, $router);

        return $config;
    }
}
