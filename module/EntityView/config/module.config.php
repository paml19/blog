<?php
namespace paml\EntityView;

use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'controllers' => [
        'factories' => [
            Controller\DefaultController::class => InvokableFactory::class,
        ],
        'aliases' => [
            'defaults' => Controller\DefaultController::class,
        ],
    ],
    'view_manager' => [
        'template_map' => [
            'paml/entity-view/default/index' => __DIR__ . '/../view/entity-view/default/index.phtml',
            'paml/entity-view/default/add' => __DIR__ . '/../view/entity-view/default/add.phtml',
            'paml/entity-view/default/delete' => __DIR__ . '/../view/entity-view/default/delete.phtml',
        ],
    ],
];
