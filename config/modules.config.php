<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

/**
 * List of enabled modules for this application.
 *
 * This should be an array of module namespaces used in the application.
 */
return [
    'Zend\I18n',
    'Zend\Filter',
    'Zend\Navigation',
    'Zend\Serializer',
    'Zend\Mail',
    'Zend\Paginator',
    'Zend\ServiceManager\Di',
    'Zend\Session',
    'Zend\Mvc\Plugin\Prg',
    'Zend\Mvc\Plugin\Identity',
    'Zend\Mvc\Plugin\FlashMessenger',
    'Zend\Mvc\Plugin\FilePrg',
    'Zend\Mvc\I18n',
    'Zend\Mvc\Console',
    'Zend\Hydrator',
    'Zend\InputFilter',
    'Zend\Log',
    'Zend\Form',
    'Zend\Cache',
    'Zend\Router',
    'Zend\Validator',
    'DoctrineModule',
    'DoctrineORMModule',
    'Jhu\ZdtLoggerModule',
    'AssetManager',
    'paml\Log',
    'paml\Notification\Mail',
    'paml\EntityOperation',
    'paml\EntityView',
    'paml\EntityRest',
    'paml\Notification',
    'paml\File',
    'paml\File\Library',
    'paml\Auth',
    'paml\Auth\Rest',
    'paml\SessionTranslator',
    'Application',
    'paml\Auth\Operation',
    'paml\Layouts\ColorAdmin',
    'paml\File\Image\Cropper',
];
