<?php

return [
    'entity_view' => [
        'main_module' => 'Application',
        'main_route' => [
            'name' => 'admin',
            'route' => 'admin',
        ],
        'defaults' => [
            'page' => 1,
            'count' => 100,
            'sort' => 'asc',
            'order' => 'id'
        ],
    ],
];
