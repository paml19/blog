<?php

use Application\Entity\Comment;

return [
    'entity_operation' => [
        'main_module' => 'Application',
        'events' => [
            \Doctrine\ORM\Events::preRemove,
            \Doctrine\ORM\Events::postUpdate,
            \Doctrine\ORM\Events::postPersist,
        ],
        'entities' => [
            \Application\Entity\Post::class,
            \Application\Entity\Category::class,
            \Application\Entity\Comment::class,
            \paml\Auth\Entity\User::class,
            \paml\Auth\Entity\History::class,
            \paml\Auth\Entity\Role::class,
            \paml\Auth\Rest\Entity\User::class,
            \paml\Auth\Rest\Entity\AccessToken::class,
            \paml\File\Entity\File::class,
        ],
    ],
];