<?php

use Zend\Http\Request;

return [
    'entity_rest' => [
        'allowed_methods' => [
            Request::METHOD_GET,
            Request::METHOD_PUT,
            Request::METHOD_POST,
            Request::METHOD_DELETE,
            Request::METHOD_OPTIONS,
        ],
        'defaults' => [
            'page' => 1,
            'count' => 5,
            'sort' => 'desc',
            'order' => 'dateAdd'
        ],
        'main_module' => 'Application'
    ],
];
