<?php

return [
    'file' => [
        'file_size' => 3 * 1024 * 1024,
        'public_path' => './data'
    ],
];