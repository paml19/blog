<?php

use Doctrine\DBAL\Driver\PDOMySql\Driver;
use Zend\I18n\Translator\TranslatorServiceFactory;

return [
    'doctrine' => [
        'connection' => [
            'orm_default' => [
                'driverClass' => Driver::class,
                'params' => [
                    'driverOptions' => [
                        \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\' COLLATE \'utf8_general_ci\'',
                    ],
                ],
            ],
        ],
        'configuration' => [
            'orm_default' => [
//                'metadata_cache' => 'rediscache',
                'query_cache' => 'rediscache',
                'result_cache' => 'rediscache',
                'hydration_cache' => 'rediscache',
            ],
        ],
        'eventmanager' => [
            'orm_default' => [
                'subscribers' => [
                    \Gedmo\Timestampable\TimestampableListener::class,
                    \Gedmo\SoftDeleteable\SoftDeleteableListener::class,
                    \Gedmo\Sluggable\SluggableListener::class,
                ],
            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            'translator' => TranslatorServiceFactory::class,
            'doctrine.cache.rediscache' => \Application\Factory\CacheFactory::class,
            \Zend\Cache\Storage\Adapter\Redis::class => \Zend\ServiceManager\Factory\InvokableFactory::class,
        ],
    ],
    'caches' => [
        'RedisCache' => [
            'adapter' => [
                'name' => Redis::class,
                'options' => [
                    'server' => [
                        'host' => '10.1.0.4',
                    ],
                ],
            ],
            'plugins' => [
                'serializer',
            ],
        ],
    ],
    'translator' => [
        'locale' => 'pl',
        'translation_file_patterns' => [
            [
                'type'     => 'phparray',
                'base_dir' => getcwd() .  '/data/languages',
                'pattern'  => '%s.php',
            ],
        ],
    ],
    'languages_list' => [
        ['min' => 'pl', 'name' => 'Polish'],
        ['min' => 'gb', 'name' => 'English']
    ],
    'notification' => [
        'items_count' => 5,
        'app_name' => 'app_name',
    ],
];
