<?php

use AssetManager\Cache\FilePathCache;

return [
    'layouts' => [
        'color-admin' => [
            'use-template' => false,
            'navigation-name' => 'admin'
        ],
        'user-entity' => null,
        'extra-entity' => null,
        'extra-params' => [
            'en.viewed' => 0,
        ],
        'events' => [],
        'role-based' => false,
    ],
    'asset_manager' => [
        'caching' => [
            'default' => [
                'cache' => FilePathCache::class,
                'options' => [
                    'dir' => './public'
                ],
            ],
        ],
    ],
];
