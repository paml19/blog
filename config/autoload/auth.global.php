<?php

return [
    'acl' => [
        'privileges' => [
////            ALLOW ALL, BUT DENY :role TO OPEN :action IN :controller
//            ':role' => ['*' => [
//                ':controller' => [
//                    ':action',
//                ],
//            ]],
////            ALLOW ALL, BUT DENY :controller
//            ':role' => ['*' => [
//                ':controller' => '*',
//            ]],
////            ALLOW :role TO :action IN :controller
//            ':role' => [
//                ':controller' => [
//                    ':action',
//                ],
//            ],
////            ALLOW :role TO :controller
//            ':role' => [
//                ':controller' => '*',
//            ],
////            ALLOW :role TO ALL
//            ':role' => '*'
            \Application\Module::ROLE_GUEST => [
                Application\Controller\BlogController::class => '*',
            ],
            \Application\Module::ROLE_ADMIN => [
                'category' => '*',
                'post' => '*',
                'dashboard' => '*',
            ],
            \Application\Module::ROLE_POST_MODERATOR => [
                'post' => '*',
                'dashboard' => '*',
                'category' => [
                    'index',
                    'asyncList'
                ],
            ],
            \Application\Module::ROLE_CATEGORY_MODERATOR => [
                'category' => '*',
                'dashboard' => '*',
                'post' => [
                    'index',
                    'asyncList'
                ]
            ],
            \paml\Auth\Module::AUTH_ROLE_SUPER_ADMIN => '*',
        ],
        'resources' => [
            'controllers_from_factories' => true, //false
            'controllers' => [],
            'excluded' => [
                Application\Controller\BlogController::class,
            ],
        ],
    ],
    'roles' => [
        [
            'name' => 'Guest',
            'slug' => \Application\Module::ROLE_GUEST,
        ],
        [
            'name' => 'Admin',
            'slug' => \Application\Module::ROLE_ADMIN,
        ],
        [
            'name' => 'Category moderator',
            'slug' => \Application\Module::ROLE_CATEGORY_MODERATOR,
        ],
        [
            'name' => 'Post moderator',
            'slug' => \Application\Module::ROLE_POST_MODERATOR
        ]
    ],
    'auth' => [
        'layouts' => [
            'main_layout' => 'layout/color-admin-auth',
            'templates' => [
                'login' => null,
                'register' => null,
            ],
        ],
        'admin_password' => 'zaq1@WSX',
        'after_login_redirect' => [
            'route' => 'admin/front',
            'options' => [
                'controller' => 'dashboard',
            ],
        ],
        'inheritance_roles' => false,
    ],
];
